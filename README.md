# browser-sync-ws

A Clojure library designed to synchronize user actions from 1 to many browsers with websockets.

## Usage

Run the server with `browser-sync-ws.server/server-start`.  
The web backend is listening on port 8080 _(see message in console/repl)_.

The web client needs the client js library at: `/js/client.js`.

A sample page is also provided and supplies that library for simple testings: `/sample.html`.

During development, for clojurescript, a wacther is available with the command:

    lein client-watch

Making a JAR:    

    lein client-compile
    lein uberjar

and run with:

    java -jar target/browser-sync-ws-0.1.0-SNAPSHOT-standalone.jar

Optional argument: port number, default: `8080` (as well as with the JAR)

    lein run 8099

To stop the server: `ctrl c`.

Here is a Tampermonkey script to connect any site to websocket:  
(just change the address set in `browserSyncAddress` and it should work.

```js
// ==UserScript==
// @name         ced/browser-sync-ws
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  synchronize user actions across mmultiple browsers
// @author       Cedric Simon
// @match        http://example.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    const browserSyncAddress = "192.168.3.6:8080";

    var newScript1 = document.createElement("script");
    newScript1.src = "http://"+browserSyncAddress+"/js/element-xpath.js";
    document.body.appendChild(newScript1);

    var newScript2 = document.createElement("script");
    newScript2.src = "http://"+browserSyncAddress+"/js/client.js";
    document.body.appendChild(newScript2);

    var counter = 0;
    function browserSyncStart() {
        ++counter;
        console.log("browser-sunc: attempt ", counter);
        try {
            window.browserSyncInit("ws://"+browserSyncAddress+"/mimic");
        }
        catch(e) {
            console.error("browser-sync: can't connect!");
            if(counter<10) setTimeout(browserSyncStart, 100);
        };
    }
    browserSyncStart();
})();
```


## License

Copyright © 2022 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
