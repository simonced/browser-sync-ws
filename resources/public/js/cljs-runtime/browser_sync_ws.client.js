goog.provide('browser_sync_ws.client');
if((typeof browser_sync_ws !== 'undefined') && (typeof browser_sync_ws.client !== 'undefined') && (typeof browser_sync_ws.client.socket !== 'undefined')){
} else {
browser_sync_ws.client.socket = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
browser_sync_ws.client.action_sender = (function browser_sync_ws$client$action_sender(e){

var target = e.target;
var type = e.type;
var xpath = window.getElementXpath(target);
var my_json = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"xpath","xpath",-1822766293),xpath,new cljs.core.Keyword(null,"event","event",301435442),type], null);
if(cljs.core.truth_(e.isTrusted)){
var G__41214 = type;
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("click",G__41214)){
return cljs.core.deref(browser_sync_ws.client.socket).send(my_json);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("keyup",G__41214)){
return cljs.core.deref(browser_sync_ws.client.socket).send(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(my_json,new cljs.core.Keyword(null,"value","value",305978217),target.value));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"_","_",-1201019570,null),G__41214)){
return null;
} else {
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__41214)].join('')));

}
}
}
} else {
return null;
}
});
browser_sync_ws.client.socket_open = (function browser_sync_ws$client$socket_open(_e){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["connection opened"], 0));

var doc = document;
var inputs = doc.querySelectorAll("input[type=text]");
doc.addEventListener("click",browser_sync_ws.client.action_sender);

var seq__41215 = cljs.core.seq(cljs.core.range.cljs$core$IFn$_invoke$arity$1(inputs.length));
var chunk__41217 = null;
var count__41218 = (0);
var i__41219 = (0);
while(true){
if((i__41219 < count__41218)){
var x = chunk__41217.cljs$core$IIndexed$_nth$arity$2(null,i__41219);
var input_41223 = (inputs[x]);
input_41223.addEventListener("keyup",browser_sync_ws.client.action_sender);


var G__41224 = seq__41215;
var G__41225 = chunk__41217;
var G__41226 = count__41218;
var G__41227 = (i__41219 + (1));
seq__41215 = G__41224;
chunk__41217 = G__41225;
count__41218 = G__41226;
i__41219 = G__41227;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__41215);
if(temp__5804__auto__){
var seq__41215__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__41215__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__41215__$1);
var G__41228 = cljs.core.chunk_rest(seq__41215__$1);
var G__41229 = c__4679__auto__;
var G__41230 = cljs.core.count(c__4679__auto__);
var G__41231 = (0);
seq__41215 = G__41228;
chunk__41217 = G__41229;
count__41218 = G__41230;
i__41219 = G__41231;
continue;
} else {
var x = cljs.core.first(seq__41215__$1);
var input_41232 = (inputs[x]);
input_41232.addEventListener("keyup",browser_sync_ws.client.action_sender);


var G__41233 = cljs.core.next(seq__41215__$1);
var G__41234 = null;
var G__41235 = (0);
var G__41236 = (0);
seq__41215 = G__41233;
chunk__41217 = G__41234;
count__41218 = G__41235;
i__41219 = G__41236;
continue;
}
} else {
return null;
}
}
break;
}
});
browser_sync_ws.client.socket_close = (function browser_sync_ws$client$socket_close(e){
if(cljs.core.truth_(e.wasClean())){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["closed cleanly"], 0));
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["connection died"], 0));
}
});
browser_sync_ws.client.socket_error = (function browser_sync_ws$client$socket_error(e){
return console.error("Error:",e.message);
});
browser_sync_ws.client.socket_message = (function browser_sync_ws$client$socket_message(e){

var data = e.data;
var map__41221 = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(data);
var map__41221__$1 = cljs.core.__destructure_map(map__41221);
var xpath = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__41221__$1,new cljs.core.Keyword(null,"xpath","xpath",-1822766293));
var event = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__41221__$1,new cljs.core.Keyword(null,"event","event",301435442));
var val = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__41221__$1,new cljs.core.Keyword(null,"value","value",305978217));
var target_node = document.evaluate(xpath,document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null).singleNodeValue;
var G__41222 = event;
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("click",G__41222)){
return target_node.click();
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("keyup",G__41222)){
return (target_node.value = val);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"_","_",-1201019570,null),G__41222)){
return null;
} else {
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__41222)].join('')));

}
}
}
});
browser_sync_ws.client.init = (function browser_sync_ws$client$init(socket_access_point){
var new_socket = (new window.WebSocket(socket_access_point));
cljs.core.reset_BANG_(browser_sync_ws.client.socket,new_socket);

(new_socket.onopen = browser_sync_ws.client.socket_open);

(new_socket.close = browser_sync_ws.client.socket_close);

(new_socket.onmessage = browser_sync_ws.client.socket_message);

return (new_socket.onerror = browser_sync_ws.client.socket_error);
});
(window.browserSyncInit = browser_sync_ws.client.init);

//# sourceMappingURL=browser_sync_ws.client.js.map
