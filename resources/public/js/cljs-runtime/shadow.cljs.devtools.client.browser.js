goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4870__auto__ = [];
var len__4864__auto___47643 = arguments.length;
var i__4865__auto___47644 = (0);
while(true){
if((i__4865__auto___47644 < len__4864__auto___47643)){
args__4870__auto__.push((arguments[i__4865__auto___47644]));

var G__47645 = (i__4865__auto___47644 + (1));
i__4865__auto___47644 = G__47645;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((1) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4871__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq47361){
var G__47362 = cljs.core.first(seq47361);
var seq47361__$1 = cljs.core.next(seq47361);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__47362,seq47361__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__47365 = cljs.core.seq(sources);
var chunk__47366 = null;
var count__47367 = (0);
var i__47368 = (0);
while(true){
if((i__47368 < count__47367)){
var map__47373 = chunk__47366.cljs$core$IIndexed$_nth$arity$2(null,i__47368);
var map__47373__$1 = cljs.core.__destructure_map(map__47373);
var src = map__47373__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47373__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47373__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47373__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47373__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e47374){var e_47646 = e47374;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_47646);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_47646.message)].join('')));
}

var G__47647 = seq__47365;
var G__47648 = chunk__47366;
var G__47649 = count__47367;
var G__47650 = (i__47368 + (1));
seq__47365 = G__47647;
chunk__47366 = G__47648;
count__47367 = G__47649;
i__47368 = G__47650;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__47365);
if(temp__5804__auto__){
var seq__47365__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__47365__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__47365__$1);
var G__47651 = cljs.core.chunk_rest(seq__47365__$1);
var G__47652 = c__4679__auto__;
var G__47653 = cljs.core.count(c__4679__auto__);
var G__47654 = (0);
seq__47365 = G__47651;
chunk__47366 = G__47652;
count__47367 = G__47653;
i__47368 = G__47654;
continue;
} else {
var map__47375 = cljs.core.first(seq__47365__$1);
var map__47375__$1 = cljs.core.__destructure_map(map__47375);
var src = map__47375__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47375__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47375__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47375__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47375__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e47376){var e_47655 = e47376;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_47655);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_47655.message)].join('')));
}

var G__47656 = cljs.core.next(seq__47365__$1);
var G__47657 = null;
var G__47658 = (0);
var G__47659 = (0);
seq__47365 = G__47656;
chunk__47366 = G__47657;
count__47367 = G__47658;
i__47368 = G__47659;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__47377 = cljs.core.seq(js_requires);
var chunk__47378 = null;
var count__47379 = (0);
var i__47380 = (0);
while(true){
if((i__47380 < count__47379)){
var js_ns = chunk__47378.cljs$core$IIndexed$_nth$arity$2(null,i__47380);
var require_str_47660 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_47660);


var G__47661 = seq__47377;
var G__47662 = chunk__47378;
var G__47663 = count__47379;
var G__47664 = (i__47380 + (1));
seq__47377 = G__47661;
chunk__47378 = G__47662;
count__47379 = G__47663;
i__47380 = G__47664;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__47377);
if(temp__5804__auto__){
var seq__47377__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__47377__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__47377__$1);
var G__47665 = cljs.core.chunk_rest(seq__47377__$1);
var G__47666 = c__4679__auto__;
var G__47667 = cljs.core.count(c__4679__auto__);
var G__47668 = (0);
seq__47377 = G__47665;
chunk__47378 = G__47666;
count__47379 = G__47667;
i__47380 = G__47668;
continue;
} else {
var js_ns = cljs.core.first(seq__47377__$1);
var require_str_47669 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_47669);


var G__47670 = cljs.core.next(seq__47377__$1);
var G__47671 = null;
var G__47672 = (0);
var G__47673 = (0);
seq__47377 = G__47670;
chunk__47378 = G__47671;
count__47379 = G__47672;
i__47380 = G__47673;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__47382){
var map__47383 = p__47382;
var map__47383__$1 = cljs.core.__destructure_map(map__47383);
var msg = map__47383__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47383__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47383__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4652__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__47384(s__47385){
return (new cljs.core.LazySeq(null,(function (){
var s__47385__$1 = s__47385;
while(true){
var temp__5804__auto__ = cljs.core.seq(s__47385__$1);
if(temp__5804__auto__){
var xs__6360__auto__ = temp__5804__auto__;
var map__47390 = cljs.core.first(xs__6360__auto__);
var map__47390__$1 = cljs.core.__destructure_map(map__47390);
var src = map__47390__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47390__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47390__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4648__auto__ = ((function (s__47385__$1,map__47390,map__47390__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__47383,map__47383__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__47384_$_iter__47386(s__47387){
return (new cljs.core.LazySeq(null,((function (s__47385__$1,map__47390,map__47390__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__47383,map__47383__$1,msg,info,reload_info){
return (function (){
var s__47387__$1 = s__47387;
while(true){
var temp__5804__auto____$1 = cljs.core.seq(s__47387__$1);
if(temp__5804__auto____$1){
var s__47387__$2 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__47387__$2)){
var c__4650__auto__ = cljs.core.chunk_first(s__47387__$2);
var size__4651__auto__ = cljs.core.count(c__4650__auto__);
var b__47389 = cljs.core.chunk_buffer(size__4651__auto__);
if((function (){var i__47388 = (0);
while(true){
if((i__47388 < size__4651__auto__)){
var warning = cljs.core._nth(c__4650__auto__,i__47388);
cljs.core.chunk_append(b__47389,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__47674 = (i__47388 + (1));
i__47388 = G__47674;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__47389),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__47384_$_iter__47386(cljs.core.chunk_rest(s__47387__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__47389),null);
}
} else {
var warning = cljs.core.first(s__47387__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__47384_$_iter__47386(cljs.core.rest(s__47387__$2)));
}
} else {
return null;
}
break;
}
});})(s__47385__$1,map__47390,map__47390__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__47383,map__47383__$1,msg,info,reload_info))
,null,null));
});})(s__47385__$1,map__47390,map__47390__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__47383,map__47383__$1,msg,info,reload_info))
;
var fs__4649__auto__ = cljs.core.seq(iterys__4648__auto__(warnings));
if(fs__4649__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4649__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__47384(cljs.core.rest(s__47385__$1)));
} else {
var G__47675 = cljs.core.rest(s__47385__$1);
s__47385__$1 = G__47675;
continue;
}
} else {
var G__47676 = cljs.core.rest(s__47385__$1);
s__47385__$1 = G__47676;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4652__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__47391_47677 = cljs.core.seq(warnings);
var chunk__47392_47678 = null;
var count__47393_47679 = (0);
var i__47394_47680 = (0);
while(true){
if((i__47394_47680 < count__47393_47679)){
var map__47397_47681 = chunk__47392_47678.cljs$core$IIndexed$_nth$arity$2(null,i__47394_47680);
var map__47397_47682__$1 = cljs.core.__destructure_map(map__47397_47681);
var w_47683 = map__47397_47682__$1;
var msg_47684__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47397_47682__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_47685 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47397_47682__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_47686 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47397_47682__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_47687 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47397_47682__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_47687)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_47685),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_47686),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_47684__$1)].join(''));


var G__47688 = seq__47391_47677;
var G__47689 = chunk__47392_47678;
var G__47690 = count__47393_47679;
var G__47691 = (i__47394_47680 + (1));
seq__47391_47677 = G__47688;
chunk__47392_47678 = G__47689;
count__47393_47679 = G__47690;
i__47394_47680 = G__47691;
continue;
} else {
var temp__5804__auto___47692 = cljs.core.seq(seq__47391_47677);
if(temp__5804__auto___47692){
var seq__47391_47693__$1 = temp__5804__auto___47692;
if(cljs.core.chunked_seq_QMARK_(seq__47391_47693__$1)){
var c__4679__auto___47694 = cljs.core.chunk_first(seq__47391_47693__$1);
var G__47695 = cljs.core.chunk_rest(seq__47391_47693__$1);
var G__47696 = c__4679__auto___47694;
var G__47697 = cljs.core.count(c__4679__auto___47694);
var G__47698 = (0);
seq__47391_47677 = G__47695;
chunk__47392_47678 = G__47696;
count__47393_47679 = G__47697;
i__47394_47680 = G__47698;
continue;
} else {
var map__47398_47699 = cljs.core.first(seq__47391_47693__$1);
var map__47398_47700__$1 = cljs.core.__destructure_map(map__47398_47699);
var w_47701 = map__47398_47700__$1;
var msg_47702__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47398_47700__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_47703 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47398_47700__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_47704 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47398_47700__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_47705 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47398_47700__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_47705)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_47703),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_47704),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_47702__$1)].join(''));


var G__47706 = cljs.core.next(seq__47391_47693__$1);
var G__47707 = null;
var G__47708 = (0);
var G__47709 = (0);
seq__47391_47677 = G__47706;
chunk__47392_47678 = G__47707;
count__47393_47679 = G__47708;
i__47394_47680 = G__47709;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__47381_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__47381_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__4251__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__4251__auto__){
var and__4251__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__4251__auto____$1){
return new$;
} else {
return and__4251__auto____$1;
}
} else {
return and__4251__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__47399){
var map__47400 = p__47399;
var map__47400__$1 = cljs.core.__destructure_map(map__47400);
var msg = map__47400__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47400__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47400__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var seq__47401 = cljs.core.seq(updates);
var chunk__47403 = null;
var count__47404 = (0);
var i__47405 = (0);
while(true){
if((i__47405 < count__47404)){
var path = chunk__47403.cljs$core$IIndexed$_nth$arity$2(null,i__47405);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__47515_47710 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__47519_47711 = null;
var count__47520_47712 = (0);
var i__47521_47713 = (0);
while(true){
if((i__47521_47713 < count__47520_47712)){
var node_47714 = chunk__47519_47711.cljs$core$IIndexed$_nth$arity$2(null,i__47521_47713);
if(cljs.core.not(node_47714.shadow$old)){
var path_match_47715 = shadow.cljs.devtools.client.browser.match_paths(node_47714.getAttribute("href"),path);
if(cljs.core.truth_(path_match_47715)){
var new_link_47716 = (function (){var G__47547 = node_47714.cloneNode(true);
G__47547.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_47715),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__47547;
})();
(node_47714.shadow$old = true);

(new_link_47716.onload = ((function (seq__47515_47710,chunk__47519_47711,count__47520_47712,i__47521_47713,seq__47401,chunk__47403,count__47404,i__47405,new_link_47716,path_match_47715,node_47714,path,map__47400,map__47400__$1,msg,updates,reload_info){
return (function (e){
var seq__47548_47717 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__47550_47718 = null;
var count__47551_47719 = (0);
var i__47552_47720 = (0);
while(true){
if((i__47552_47720 < count__47551_47719)){
var map__47556_47721 = chunk__47550_47718.cljs$core$IIndexed$_nth$arity$2(null,i__47552_47720);
var map__47556_47722__$1 = cljs.core.__destructure_map(map__47556_47721);
var task_47723 = map__47556_47722__$1;
var fn_str_47724 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47556_47722__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47725 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47556_47722__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47726 = goog.getObjectByName(fn_str_47724,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47725)].join(''));

(fn_obj_47726.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47726.cljs$core$IFn$_invoke$arity$2(path,new_link_47716) : fn_obj_47726.call(null,path,new_link_47716));


var G__47727 = seq__47548_47717;
var G__47728 = chunk__47550_47718;
var G__47729 = count__47551_47719;
var G__47730 = (i__47552_47720 + (1));
seq__47548_47717 = G__47727;
chunk__47550_47718 = G__47728;
count__47551_47719 = G__47729;
i__47552_47720 = G__47730;
continue;
} else {
var temp__5804__auto___47731 = cljs.core.seq(seq__47548_47717);
if(temp__5804__auto___47731){
var seq__47548_47732__$1 = temp__5804__auto___47731;
if(cljs.core.chunked_seq_QMARK_(seq__47548_47732__$1)){
var c__4679__auto___47733 = cljs.core.chunk_first(seq__47548_47732__$1);
var G__47734 = cljs.core.chunk_rest(seq__47548_47732__$1);
var G__47735 = c__4679__auto___47733;
var G__47736 = cljs.core.count(c__4679__auto___47733);
var G__47737 = (0);
seq__47548_47717 = G__47734;
chunk__47550_47718 = G__47735;
count__47551_47719 = G__47736;
i__47552_47720 = G__47737;
continue;
} else {
var map__47557_47738 = cljs.core.first(seq__47548_47732__$1);
var map__47557_47739__$1 = cljs.core.__destructure_map(map__47557_47738);
var task_47740 = map__47557_47739__$1;
var fn_str_47741 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47557_47739__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47742 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47557_47739__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47743 = goog.getObjectByName(fn_str_47741,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47742)].join(''));

(fn_obj_47743.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47743.cljs$core$IFn$_invoke$arity$2(path,new_link_47716) : fn_obj_47743.call(null,path,new_link_47716));


var G__47744 = cljs.core.next(seq__47548_47732__$1);
var G__47745 = null;
var G__47746 = (0);
var G__47747 = (0);
seq__47548_47717 = G__47744;
chunk__47550_47718 = G__47745;
count__47551_47719 = G__47746;
i__47552_47720 = G__47747;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_47714);
});})(seq__47515_47710,chunk__47519_47711,count__47520_47712,i__47521_47713,seq__47401,chunk__47403,count__47404,i__47405,new_link_47716,path_match_47715,node_47714,path,map__47400,map__47400__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_47715], 0));

goog.dom.insertSiblingAfter(new_link_47716,node_47714);


var G__47748 = seq__47515_47710;
var G__47749 = chunk__47519_47711;
var G__47750 = count__47520_47712;
var G__47751 = (i__47521_47713 + (1));
seq__47515_47710 = G__47748;
chunk__47519_47711 = G__47749;
count__47520_47712 = G__47750;
i__47521_47713 = G__47751;
continue;
} else {
var G__47752 = seq__47515_47710;
var G__47753 = chunk__47519_47711;
var G__47754 = count__47520_47712;
var G__47755 = (i__47521_47713 + (1));
seq__47515_47710 = G__47752;
chunk__47519_47711 = G__47753;
count__47520_47712 = G__47754;
i__47521_47713 = G__47755;
continue;
}
} else {
var G__47756 = seq__47515_47710;
var G__47757 = chunk__47519_47711;
var G__47758 = count__47520_47712;
var G__47759 = (i__47521_47713 + (1));
seq__47515_47710 = G__47756;
chunk__47519_47711 = G__47757;
count__47520_47712 = G__47758;
i__47521_47713 = G__47759;
continue;
}
} else {
var temp__5804__auto___47760 = cljs.core.seq(seq__47515_47710);
if(temp__5804__auto___47760){
var seq__47515_47761__$1 = temp__5804__auto___47760;
if(cljs.core.chunked_seq_QMARK_(seq__47515_47761__$1)){
var c__4679__auto___47762 = cljs.core.chunk_first(seq__47515_47761__$1);
var G__47763 = cljs.core.chunk_rest(seq__47515_47761__$1);
var G__47764 = c__4679__auto___47762;
var G__47765 = cljs.core.count(c__4679__auto___47762);
var G__47766 = (0);
seq__47515_47710 = G__47763;
chunk__47519_47711 = G__47764;
count__47520_47712 = G__47765;
i__47521_47713 = G__47766;
continue;
} else {
var node_47767 = cljs.core.first(seq__47515_47761__$1);
if(cljs.core.not(node_47767.shadow$old)){
var path_match_47768 = shadow.cljs.devtools.client.browser.match_paths(node_47767.getAttribute("href"),path);
if(cljs.core.truth_(path_match_47768)){
var new_link_47769 = (function (){var G__47558 = node_47767.cloneNode(true);
G__47558.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_47768),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__47558;
})();
(node_47767.shadow$old = true);

(new_link_47769.onload = ((function (seq__47515_47710,chunk__47519_47711,count__47520_47712,i__47521_47713,seq__47401,chunk__47403,count__47404,i__47405,new_link_47769,path_match_47768,node_47767,seq__47515_47761__$1,temp__5804__auto___47760,path,map__47400,map__47400__$1,msg,updates,reload_info){
return (function (e){
var seq__47559_47770 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__47561_47771 = null;
var count__47562_47772 = (0);
var i__47563_47773 = (0);
while(true){
if((i__47563_47773 < count__47562_47772)){
var map__47567_47774 = chunk__47561_47771.cljs$core$IIndexed$_nth$arity$2(null,i__47563_47773);
var map__47567_47775__$1 = cljs.core.__destructure_map(map__47567_47774);
var task_47776 = map__47567_47775__$1;
var fn_str_47777 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47567_47775__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47778 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47567_47775__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47779 = goog.getObjectByName(fn_str_47777,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47778)].join(''));

(fn_obj_47779.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47779.cljs$core$IFn$_invoke$arity$2(path,new_link_47769) : fn_obj_47779.call(null,path,new_link_47769));


var G__47780 = seq__47559_47770;
var G__47781 = chunk__47561_47771;
var G__47782 = count__47562_47772;
var G__47783 = (i__47563_47773 + (1));
seq__47559_47770 = G__47780;
chunk__47561_47771 = G__47781;
count__47562_47772 = G__47782;
i__47563_47773 = G__47783;
continue;
} else {
var temp__5804__auto___47784__$1 = cljs.core.seq(seq__47559_47770);
if(temp__5804__auto___47784__$1){
var seq__47559_47785__$1 = temp__5804__auto___47784__$1;
if(cljs.core.chunked_seq_QMARK_(seq__47559_47785__$1)){
var c__4679__auto___47786 = cljs.core.chunk_first(seq__47559_47785__$1);
var G__47787 = cljs.core.chunk_rest(seq__47559_47785__$1);
var G__47788 = c__4679__auto___47786;
var G__47789 = cljs.core.count(c__4679__auto___47786);
var G__47790 = (0);
seq__47559_47770 = G__47787;
chunk__47561_47771 = G__47788;
count__47562_47772 = G__47789;
i__47563_47773 = G__47790;
continue;
} else {
var map__47568_47791 = cljs.core.first(seq__47559_47785__$1);
var map__47568_47792__$1 = cljs.core.__destructure_map(map__47568_47791);
var task_47793 = map__47568_47792__$1;
var fn_str_47794 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47568_47792__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47795 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47568_47792__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47796 = goog.getObjectByName(fn_str_47794,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47795)].join(''));

(fn_obj_47796.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47796.cljs$core$IFn$_invoke$arity$2(path,new_link_47769) : fn_obj_47796.call(null,path,new_link_47769));


var G__47797 = cljs.core.next(seq__47559_47785__$1);
var G__47798 = null;
var G__47799 = (0);
var G__47800 = (0);
seq__47559_47770 = G__47797;
chunk__47561_47771 = G__47798;
count__47562_47772 = G__47799;
i__47563_47773 = G__47800;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_47767);
});})(seq__47515_47710,chunk__47519_47711,count__47520_47712,i__47521_47713,seq__47401,chunk__47403,count__47404,i__47405,new_link_47769,path_match_47768,node_47767,seq__47515_47761__$1,temp__5804__auto___47760,path,map__47400,map__47400__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_47768], 0));

goog.dom.insertSiblingAfter(new_link_47769,node_47767);


var G__47801 = cljs.core.next(seq__47515_47761__$1);
var G__47802 = null;
var G__47803 = (0);
var G__47804 = (0);
seq__47515_47710 = G__47801;
chunk__47519_47711 = G__47802;
count__47520_47712 = G__47803;
i__47521_47713 = G__47804;
continue;
} else {
var G__47805 = cljs.core.next(seq__47515_47761__$1);
var G__47806 = null;
var G__47807 = (0);
var G__47808 = (0);
seq__47515_47710 = G__47805;
chunk__47519_47711 = G__47806;
count__47520_47712 = G__47807;
i__47521_47713 = G__47808;
continue;
}
} else {
var G__47809 = cljs.core.next(seq__47515_47761__$1);
var G__47810 = null;
var G__47811 = (0);
var G__47812 = (0);
seq__47515_47710 = G__47809;
chunk__47519_47711 = G__47810;
count__47520_47712 = G__47811;
i__47521_47713 = G__47812;
continue;
}
}
} else {
}
}
break;
}


var G__47813 = seq__47401;
var G__47814 = chunk__47403;
var G__47815 = count__47404;
var G__47816 = (i__47405 + (1));
seq__47401 = G__47813;
chunk__47403 = G__47814;
count__47404 = G__47815;
i__47405 = G__47816;
continue;
} else {
var G__47817 = seq__47401;
var G__47818 = chunk__47403;
var G__47819 = count__47404;
var G__47820 = (i__47405 + (1));
seq__47401 = G__47817;
chunk__47403 = G__47818;
count__47404 = G__47819;
i__47405 = G__47820;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__47401);
if(temp__5804__auto__){
var seq__47401__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__47401__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__47401__$1);
var G__47821 = cljs.core.chunk_rest(seq__47401__$1);
var G__47822 = c__4679__auto__;
var G__47823 = cljs.core.count(c__4679__auto__);
var G__47824 = (0);
seq__47401 = G__47821;
chunk__47403 = G__47822;
count__47404 = G__47823;
i__47405 = G__47824;
continue;
} else {
var path = cljs.core.first(seq__47401__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__47569_47825 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__47573_47826 = null;
var count__47574_47827 = (0);
var i__47575_47828 = (0);
while(true){
if((i__47575_47828 < count__47574_47827)){
var node_47829 = chunk__47573_47826.cljs$core$IIndexed$_nth$arity$2(null,i__47575_47828);
if(cljs.core.not(node_47829.shadow$old)){
var path_match_47830 = shadow.cljs.devtools.client.browser.match_paths(node_47829.getAttribute("href"),path);
if(cljs.core.truth_(path_match_47830)){
var new_link_47831 = (function (){var G__47601 = node_47829.cloneNode(true);
G__47601.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_47830),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__47601;
})();
(node_47829.shadow$old = true);

(new_link_47831.onload = ((function (seq__47569_47825,chunk__47573_47826,count__47574_47827,i__47575_47828,seq__47401,chunk__47403,count__47404,i__47405,new_link_47831,path_match_47830,node_47829,path,seq__47401__$1,temp__5804__auto__,map__47400,map__47400__$1,msg,updates,reload_info){
return (function (e){
var seq__47602_47832 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__47604_47833 = null;
var count__47605_47834 = (0);
var i__47606_47835 = (0);
while(true){
if((i__47606_47835 < count__47605_47834)){
var map__47610_47836 = chunk__47604_47833.cljs$core$IIndexed$_nth$arity$2(null,i__47606_47835);
var map__47610_47837__$1 = cljs.core.__destructure_map(map__47610_47836);
var task_47838 = map__47610_47837__$1;
var fn_str_47839 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47610_47837__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47840 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47610_47837__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47841 = goog.getObjectByName(fn_str_47839,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47840)].join(''));

(fn_obj_47841.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47841.cljs$core$IFn$_invoke$arity$2(path,new_link_47831) : fn_obj_47841.call(null,path,new_link_47831));


var G__47842 = seq__47602_47832;
var G__47843 = chunk__47604_47833;
var G__47844 = count__47605_47834;
var G__47845 = (i__47606_47835 + (1));
seq__47602_47832 = G__47842;
chunk__47604_47833 = G__47843;
count__47605_47834 = G__47844;
i__47606_47835 = G__47845;
continue;
} else {
var temp__5804__auto___47846__$1 = cljs.core.seq(seq__47602_47832);
if(temp__5804__auto___47846__$1){
var seq__47602_47847__$1 = temp__5804__auto___47846__$1;
if(cljs.core.chunked_seq_QMARK_(seq__47602_47847__$1)){
var c__4679__auto___47848 = cljs.core.chunk_first(seq__47602_47847__$1);
var G__47849 = cljs.core.chunk_rest(seq__47602_47847__$1);
var G__47850 = c__4679__auto___47848;
var G__47851 = cljs.core.count(c__4679__auto___47848);
var G__47852 = (0);
seq__47602_47832 = G__47849;
chunk__47604_47833 = G__47850;
count__47605_47834 = G__47851;
i__47606_47835 = G__47852;
continue;
} else {
var map__47611_47853 = cljs.core.first(seq__47602_47847__$1);
var map__47611_47854__$1 = cljs.core.__destructure_map(map__47611_47853);
var task_47855 = map__47611_47854__$1;
var fn_str_47856 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47611_47854__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47857 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47611_47854__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47858 = goog.getObjectByName(fn_str_47856,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47857)].join(''));

(fn_obj_47858.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47858.cljs$core$IFn$_invoke$arity$2(path,new_link_47831) : fn_obj_47858.call(null,path,new_link_47831));


var G__47859 = cljs.core.next(seq__47602_47847__$1);
var G__47860 = null;
var G__47861 = (0);
var G__47862 = (0);
seq__47602_47832 = G__47859;
chunk__47604_47833 = G__47860;
count__47605_47834 = G__47861;
i__47606_47835 = G__47862;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_47829);
});})(seq__47569_47825,chunk__47573_47826,count__47574_47827,i__47575_47828,seq__47401,chunk__47403,count__47404,i__47405,new_link_47831,path_match_47830,node_47829,path,seq__47401__$1,temp__5804__auto__,map__47400,map__47400__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_47830], 0));

goog.dom.insertSiblingAfter(new_link_47831,node_47829);


var G__47863 = seq__47569_47825;
var G__47864 = chunk__47573_47826;
var G__47865 = count__47574_47827;
var G__47866 = (i__47575_47828 + (1));
seq__47569_47825 = G__47863;
chunk__47573_47826 = G__47864;
count__47574_47827 = G__47865;
i__47575_47828 = G__47866;
continue;
} else {
var G__47867 = seq__47569_47825;
var G__47868 = chunk__47573_47826;
var G__47869 = count__47574_47827;
var G__47870 = (i__47575_47828 + (1));
seq__47569_47825 = G__47867;
chunk__47573_47826 = G__47868;
count__47574_47827 = G__47869;
i__47575_47828 = G__47870;
continue;
}
} else {
var G__47871 = seq__47569_47825;
var G__47872 = chunk__47573_47826;
var G__47873 = count__47574_47827;
var G__47874 = (i__47575_47828 + (1));
seq__47569_47825 = G__47871;
chunk__47573_47826 = G__47872;
count__47574_47827 = G__47873;
i__47575_47828 = G__47874;
continue;
}
} else {
var temp__5804__auto___47875__$1 = cljs.core.seq(seq__47569_47825);
if(temp__5804__auto___47875__$1){
var seq__47569_47876__$1 = temp__5804__auto___47875__$1;
if(cljs.core.chunked_seq_QMARK_(seq__47569_47876__$1)){
var c__4679__auto___47877 = cljs.core.chunk_first(seq__47569_47876__$1);
var G__47878 = cljs.core.chunk_rest(seq__47569_47876__$1);
var G__47879 = c__4679__auto___47877;
var G__47880 = cljs.core.count(c__4679__auto___47877);
var G__47881 = (0);
seq__47569_47825 = G__47878;
chunk__47573_47826 = G__47879;
count__47574_47827 = G__47880;
i__47575_47828 = G__47881;
continue;
} else {
var node_47882 = cljs.core.first(seq__47569_47876__$1);
if(cljs.core.not(node_47882.shadow$old)){
var path_match_47883 = shadow.cljs.devtools.client.browser.match_paths(node_47882.getAttribute("href"),path);
if(cljs.core.truth_(path_match_47883)){
var new_link_47884 = (function (){var G__47612 = node_47882.cloneNode(true);
G__47612.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_47883),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__47612;
})();
(node_47882.shadow$old = true);

(new_link_47884.onload = ((function (seq__47569_47825,chunk__47573_47826,count__47574_47827,i__47575_47828,seq__47401,chunk__47403,count__47404,i__47405,new_link_47884,path_match_47883,node_47882,seq__47569_47876__$1,temp__5804__auto___47875__$1,path,seq__47401__$1,temp__5804__auto__,map__47400,map__47400__$1,msg,updates,reload_info){
return (function (e){
var seq__47613_47885 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__47615_47886 = null;
var count__47616_47887 = (0);
var i__47617_47888 = (0);
while(true){
if((i__47617_47888 < count__47616_47887)){
var map__47621_47889 = chunk__47615_47886.cljs$core$IIndexed$_nth$arity$2(null,i__47617_47888);
var map__47621_47890__$1 = cljs.core.__destructure_map(map__47621_47889);
var task_47891 = map__47621_47890__$1;
var fn_str_47892 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47621_47890__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47893 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47621_47890__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47894 = goog.getObjectByName(fn_str_47892,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47893)].join(''));

(fn_obj_47894.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47894.cljs$core$IFn$_invoke$arity$2(path,new_link_47884) : fn_obj_47894.call(null,path,new_link_47884));


var G__47895 = seq__47613_47885;
var G__47896 = chunk__47615_47886;
var G__47897 = count__47616_47887;
var G__47898 = (i__47617_47888 + (1));
seq__47613_47885 = G__47895;
chunk__47615_47886 = G__47896;
count__47616_47887 = G__47897;
i__47617_47888 = G__47898;
continue;
} else {
var temp__5804__auto___47899__$2 = cljs.core.seq(seq__47613_47885);
if(temp__5804__auto___47899__$2){
var seq__47613_47900__$1 = temp__5804__auto___47899__$2;
if(cljs.core.chunked_seq_QMARK_(seq__47613_47900__$1)){
var c__4679__auto___47901 = cljs.core.chunk_first(seq__47613_47900__$1);
var G__47902 = cljs.core.chunk_rest(seq__47613_47900__$1);
var G__47903 = c__4679__auto___47901;
var G__47904 = cljs.core.count(c__4679__auto___47901);
var G__47905 = (0);
seq__47613_47885 = G__47902;
chunk__47615_47886 = G__47903;
count__47616_47887 = G__47904;
i__47617_47888 = G__47905;
continue;
} else {
var map__47622_47906 = cljs.core.first(seq__47613_47900__$1);
var map__47622_47907__$1 = cljs.core.__destructure_map(map__47622_47906);
var task_47908 = map__47622_47907__$1;
var fn_str_47909 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47622_47907__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_47910 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47622_47907__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_47911 = goog.getObjectByName(fn_str_47909,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_47910)].join(''));

(fn_obj_47911.cljs$core$IFn$_invoke$arity$2 ? fn_obj_47911.cljs$core$IFn$_invoke$arity$2(path,new_link_47884) : fn_obj_47911.call(null,path,new_link_47884));


var G__47912 = cljs.core.next(seq__47613_47900__$1);
var G__47913 = null;
var G__47914 = (0);
var G__47915 = (0);
seq__47613_47885 = G__47912;
chunk__47615_47886 = G__47913;
count__47616_47887 = G__47914;
i__47617_47888 = G__47915;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_47882);
});})(seq__47569_47825,chunk__47573_47826,count__47574_47827,i__47575_47828,seq__47401,chunk__47403,count__47404,i__47405,new_link_47884,path_match_47883,node_47882,seq__47569_47876__$1,temp__5804__auto___47875__$1,path,seq__47401__$1,temp__5804__auto__,map__47400,map__47400__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_47883], 0));

goog.dom.insertSiblingAfter(new_link_47884,node_47882);


var G__47916 = cljs.core.next(seq__47569_47876__$1);
var G__47917 = null;
var G__47918 = (0);
var G__47919 = (0);
seq__47569_47825 = G__47916;
chunk__47573_47826 = G__47917;
count__47574_47827 = G__47918;
i__47575_47828 = G__47919;
continue;
} else {
var G__47920 = cljs.core.next(seq__47569_47876__$1);
var G__47921 = null;
var G__47922 = (0);
var G__47923 = (0);
seq__47569_47825 = G__47920;
chunk__47573_47826 = G__47921;
count__47574_47827 = G__47922;
i__47575_47828 = G__47923;
continue;
}
} else {
var G__47924 = cljs.core.next(seq__47569_47876__$1);
var G__47925 = null;
var G__47926 = (0);
var G__47927 = (0);
seq__47569_47825 = G__47924;
chunk__47573_47826 = G__47925;
count__47574_47827 = G__47926;
i__47575_47828 = G__47927;
continue;
}
}
} else {
}
}
break;
}


var G__47928 = cljs.core.next(seq__47401__$1);
var G__47929 = null;
var G__47930 = (0);
var G__47931 = (0);
seq__47401 = G__47928;
chunk__47403 = G__47929;
count__47404 = G__47930;
i__47405 = G__47931;
continue;
} else {
var G__47932 = cljs.core.next(seq__47401__$1);
var G__47933 = null;
var G__47934 = (0);
var G__47935 = (0);
seq__47401 = G__47932;
chunk__47403 = G__47933;
count__47404 = G__47934;
i__47405 = G__47935;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__47623){
var map__47624 = p__47623;
var map__47624__$1 = cljs.core.__destructure_map(map__47624);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47624__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.runtime_info = (((typeof SHADOW_CONFIG !== 'undefined'))?shadow.json.to_clj.cljs$core$IFn$_invoke$arity$1(SHADOW_CONFIG):null);
shadow.cljs.devtools.client.browser.client_info = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([shadow.cljs.devtools.client.browser.runtime_info,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null)], 0));
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__47625){
var map__47626 = p__47625;
var map__47626__$1 = cljs.core.__destructure_map(map__47626);
var _ = map__47626__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47626__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__47627,done,error){
var map__47628 = p__47627;
var map__47628__$1 = cljs.core.__destructure_map(map__47628);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47628__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__47629,done,error){
var map__47630 = p__47629;
var map__47630__$1 = cljs.core.__destructure_map(map__47630);
var msg = map__47630__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47630__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47630__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47630__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__47631){
var map__47632 = p__47631;
var map__47632__$1 = cljs.core.__destructure_map(map__47632);
var src = map__47632__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47632__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4251__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4251__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4251__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__47633 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__47633) : done.call(null,G__47633));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__47634){
var map__47635 = p__47634;
var map__47635__$1 = cljs.core.__destructure_map(map__47635);
var msg__$1 = map__47635__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47635__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e47636){var ex = e47636;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__47637){
var map__47638 = p__47637;
var map__47638__$1 = cljs.core.__destructure_map(map__47638);
var env = map__47638__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47638__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (msg){
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__47639){
var map__47640 = p__47639;
var map__47640__$1 = cljs.core.__destructure_map(map__47640);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47640__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47640__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__47641){
var map__47642 = p__47641;
var map__47642__$1 = cljs.core.__destructure_map(map__47642);
var svc = map__47642__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__47642__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
