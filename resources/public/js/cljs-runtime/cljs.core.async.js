goog.provide('cljs.core.async');
goog.scope(function(){
  cljs.core.async.goog$module$goog$array = goog.module.get('goog.array');
});
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__41633 = arguments.length;
switch (G__41633) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async41634 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41634 = (function (f,blockable,meta41635){
this.f = f;
this.blockable = blockable;
this.meta41635 = meta41635;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async41634.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41636,meta41635__$1){
var self__ = this;
var _41636__$1 = this;
return (new cljs.core.async.t_cljs$core$async41634(self__.f,self__.blockable,meta41635__$1));
}));

(cljs.core.async.t_cljs$core$async41634.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41636){
var self__ = this;
var _41636__$1 = this;
return self__.meta41635;
}));

(cljs.core.async.t_cljs$core$async41634.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async41634.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async41634.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async41634.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async41634.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta41635","meta41635",-1189799236,null)], null);
}));

(cljs.core.async.t_cljs$core$async41634.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async41634.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41634");

(cljs.core.async.t_cljs$core$async41634.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async41634");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async41634.
 */
cljs.core.async.__GT_t_cljs$core$async41634 = (function cljs$core$async$__GT_t_cljs$core$async41634(f__$1,blockable__$1,meta41635){
return (new cljs.core.async.t_cljs$core$async41634(f__$1,blockable__$1,meta41635));
});

}

return (new cljs.core.async.t_cljs$core$async41634(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__41655 = arguments.length;
switch (G__41655) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__41657 = arguments.length;
switch (G__41657) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__41679 = arguments.length;
switch (G__41679) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_44261 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_44261) : fn1.call(null,val_44261));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_44261) : fn1.call(null,val_44261));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__41681 = arguments.length;
switch (G__41681) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5802__auto__)){
var ret = temp__5802__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5802__auto__)){
var retb = temp__5802__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4741__auto___44276 = n;
var x_44277 = (0);
while(true){
if((x_44277 < n__4741__auto___44276)){
(a[x_44277] = x_44277);

var G__44278 = (x_44277 + (1));
x_44277 = G__44278;
continue;
} else {
}
break;
}

cljs.core.async.goog$module$goog$array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async41696 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41696 = (function (flag,meta41697){
this.flag = flag;
this.meta41697 = meta41697;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async41696.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41698,meta41697__$1){
var self__ = this;
var _41698__$1 = this;
return (new cljs.core.async.t_cljs$core$async41696(self__.flag,meta41697__$1));
}));

(cljs.core.async.t_cljs$core$async41696.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41698){
var self__ = this;
var _41698__$1 = this;
return self__.meta41697;
}));

(cljs.core.async.t_cljs$core$async41696.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async41696.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async41696.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async41696.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async41696.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta41697","meta41697",-1093370989,null)], null);
}));

(cljs.core.async.t_cljs$core$async41696.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async41696.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41696");

(cljs.core.async.t_cljs$core$async41696.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async41696");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async41696.
 */
cljs.core.async.__GT_t_cljs$core$async41696 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async41696(flag__$1,meta41697){
return (new cljs.core.async.t_cljs$core$async41696(flag__$1,meta41697));
});

}

return (new cljs.core.async.t_cljs$core$async41696(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async41712 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41712 = (function (flag,cb,meta41713){
this.flag = flag;
this.cb = cb;
this.meta41713 = meta41713;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async41712.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41714,meta41713__$1){
var self__ = this;
var _41714__$1 = this;
return (new cljs.core.async.t_cljs$core$async41712(self__.flag,self__.cb,meta41713__$1));
}));

(cljs.core.async.t_cljs$core$async41712.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41714){
var self__ = this;
var _41714__$1 = this;
return self__.meta41713;
}));

(cljs.core.async.t_cljs$core$async41712.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async41712.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async41712.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async41712.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async41712.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta41713","meta41713",-1534486964,null)], null);
}));

(cljs.core.async.t_cljs$core$async41712.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async41712.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41712");

(cljs.core.async.t_cljs$core$async41712.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async41712");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async41712.
 */
cljs.core.async.__GT_t_cljs$core$async41712 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async41712(flag__$1,cb__$1,meta41713){
return (new cljs.core.async.t_cljs$core$async41712(flag__$1,cb__$1,meta41713));
});

}

return (new cljs.core.async.t_cljs$core$async41712(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__41720_SHARP_){
var G__41726 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__41720_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__41726) : fret.call(null,G__41726));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__41721_SHARP_){
var G__41727 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__41721_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__41727) : fret.call(null,G__41727));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4253__auto__ = wport;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return port;
}
})()], null));
} else {
var G__44318 = (i + (1));
i = G__44318;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4253__auto__ = ret;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5804__auto__ = (function (){var and__4251__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4251__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4251__auto__;
}
})();
if(cljs.core.truth_(temp__5804__auto__)){
var got = temp__5804__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4870__auto__ = [];
var len__4864__auto___44326 = arguments.length;
var i__4865__auto___44327 = (0);
while(true){
if((i__4865__auto___44327 < len__4864__auto___44326)){
args__4870__auto__.push((arguments[i__4865__auto___44327]));

var G__44329 = (i__4865__auto___44327 + (1));
i__4865__auto___44327 = G__44329;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((1) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4871__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__41753){
var map__41754 = p__41753;
var map__41754__$1 = cljs.core.__destructure_map(map__41754);
var opts = map__41754__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq41739){
var G__41740 = cljs.core.first(seq41739);
var seq41739__$1 = cljs.core.next(seq41739);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__41740,seq41739__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__41759 = arguments.length;
switch (G__41759) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__41555__auto___44341 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_41794){
var state_val_41795 = (state_41794[(1)]);
if((state_val_41795 === (7))){
var inst_41790 = (state_41794[(2)]);
var state_41794__$1 = state_41794;
var statearr_41799_44342 = state_41794__$1;
(statearr_41799_44342[(2)] = inst_41790);

(statearr_41799_44342[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (1))){
var state_41794__$1 = state_41794;
var statearr_41800_44346 = state_41794__$1;
(statearr_41800_44346[(2)] = null);

(statearr_41800_44346[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (4))){
var inst_41773 = (state_41794[(7)]);
var inst_41773__$1 = (state_41794[(2)]);
var inst_41774 = (inst_41773__$1 == null);
var state_41794__$1 = (function (){var statearr_41801 = state_41794;
(statearr_41801[(7)] = inst_41773__$1);

return statearr_41801;
})();
if(cljs.core.truth_(inst_41774)){
var statearr_41802_44348 = state_41794__$1;
(statearr_41802_44348[(1)] = (5));

} else {
var statearr_41803_44349 = state_41794__$1;
(statearr_41803_44349[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (13))){
var state_41794__$1 = state_41794;
var statearr_41804_44350 = state_41794__$1;
(statearr_41804_44350[(2)] = null);

(statearr_41804_44350[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (6))){
var inst_41773 = (state_41794[(7)]);
var state_41794__$1 = state_41794;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_41794__$1,(11),to,inst_41773);
} else {
if((state_val_41795 === (3))){
var inst_41792 = (state_41794[(2)]);
var state_41794__$1 = state_41794;
return cljs.core.async.impl.ioc_helpers.return_chan(state_41794__$1,inst_41792);
} else {
if((state_val_41795 === (12))){
var state_41794__$1 = state_41794;
var statearr_41805_44353 = state_41794__$1;
(statearr_41805_44353[(2)] = null);

(statearr_41805_44353[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (2))){
var state_41794__$1 = state_41794;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_41794__$1,(4),from);
} else {
if((state_val_41795 === (11))){
var inst_41783 = (state_41794[(2)]);
var state_41794__$1 = state_41794;
if(cljs.core.truth_(inst_41783)){
var statearr_41809_44354 = state_41794__$1;
(statearr_41809_44354[(1)] = (12));

} else {
var statearr_41811_44355 = state_41794__$1;
(statearr_41811_44355[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (9))){
var state_41794__$1 = state_41794;
var statearr_41812_44356 = state_41794__$1;
(statearr_41812_44356[(2)] = null);

(statearr_41812_44356[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (5))){
var state_41794__$1 = state_41794;
if(cljs.core.truth_(close_QMARK_)){
var statearr_41813_44361 = state_41794__$1;
(statearr_41813_44361[(1)] = (8));

} else {
var statearr_41814_44362 = state_41794__$1;
(statearr_41814_44362[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (14))){
var inst_41788 = (state_41794[(2)]);
var state_41794__$1 = state_41794;
var statearr_41819_44364 = state_41794__$1;
(statearr_41819_44364[(2)] = inst_41788);

(statearr_41819_44364[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (10))){
var inst_41780 = (state_41794[(2)]);
var state_41794__$1 = state_41794;
var statearr_41820_44365 = state_41794__$1;
(statearr_41820_44365[(2)] = inst_41780);

(statearr_41820_44365[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41795 === (8))){
var inst_41777 = cljs.core.async.close_BANG_(to);
var state_41794__$1 = state_41794;
var statearr_41821_44367 = state_41794__$1;
(statearr_41821_44367[(2)] = inst_41777);

(statearr_41821_44367[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_41825 = [null,null,null,null,null,null,null,null];
(statearr_41825[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_41825[(1)] = (1));

return statearr_41825;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_41794){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_41794);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e41826){var ex__41226__auto__ = e41826;
var statearr_41827_44368 = state_41794;
(statearr_41827_44368[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_41794[(4)]))){
var statearr_41828_44369 = state_41794;
(statearr_41828_44369[(1)] = cljs.core.first((state_41794[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44370 = state_41794;
state_41794 = G__44370;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_41794){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_41794);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_41829 = f__41556__auto__();
(statearr_41829[(6)] = c__41555__auto___44341);

return statearr_41829;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__41836){
var vec__41840 = p__41836;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41840,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41840,(1),null);
var job = vec__41840;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__41555__auto___44373 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_41849){
var state_val_41850 = (state_41849[(1)]);
if((state_val_41850 === (1))){
var state_41849__$1 = state_41849;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_41849__$1,(2),res,v);
} else {
if((state_val_41850 === (2))){
var inst_41846 = (state_41849[(2)]);
var inst_41847 = cljs.core.async.close_BANG_(res);
var state_41849__$1 = (function (){var statearr_41851 = state_41849;
(statearr_41851[(7)] = inst_41846);

return statearr_41851;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_41849__$1,inst_41847);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0 = (function (){
var statearr_41852 = [null,null,null,null,null,null,null,null];
(statearr_41852[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__);

(statearr_41852[(1)] = (1));

return statearr_41852;
});
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1 = (function (state_41849){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_41849);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e41853){var ex__41226__auto__ = e41853;
var statearr_41854_44374 = state_41849;
(statearr_41854_44374[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_41849[(4)]))){
var statearr_41855_44375 = state_41849;
(statearr_41855_44375[(1)] = cljs.core.first((state_41849[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44376 = state_41849;
state_41849 = G__44376;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = function(state_41849){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1.call(this,state_41849);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_41868 = f__41556__auto__();
(statearr_41868[(6)] = c__41555__auto___44373);

return statearr_41868;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__41881){
var vec__41887 = p__41881;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41887,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41887,(1),null);
var job = vec__41887;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4741__auto___44378 = n;
var __44379 = (0);
while(true){
if((__44379 < n__4741__auto___44378)){
var G__41896_44380 = type;
var G__41896_44381__$1 = (((G__41896_44380 instanceof cljs.core.Keyword))?G__41896_44380.fqn:null);
switch (G__41896_44381__$1) {
case "compute":
var c__41555__auto___44383 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__44379,c__41555__auto___44383,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async){
return (function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = ((function (__44379,c__41555__auto___44383,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async){
return (function (state_41909){
var state_val_41910 = (state_41909[(1)]);
if((state_val_41910 === (1))){
var state_41909__$1 = state_41909;
var statearr_41912_44384 = state_41909__$1;
(statearr_41912_44384[(2)] = null);

(statearr_41912_44384[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41910 === (2))){
var state_41909__$1 = state_41909;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_41909__$1,(4),jobs);
} else {
if((state_val_41910 === (3))){
var inst_41907 = (state_41909[(2)]);
var state_41909__$1 = state_41909;
return cljs.core.async.impl.ioc_helpers.return_chan(state_41909__$1,inst_41907);
} else {
if((state_val_41910 === (4))){
var inst_41899 = (state_41909[(2)]);
var inst_41900 = process(inst_41899);
var state_41909__$1 = state_41909;
if(cljs.core.truth_(inst_41900)){
var statearr_41916_44392 = state_41909__$1;
(statearr_41916_44392[(1)] = (5));

} else {
var statearr_41917_44393 = state_41909__$1;
(statearr_41917_44393[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41910 === (5))){
var state_41909__$1 = state_41909;
var statearr_41918_44394 = state_41909__$1;
(statearr_41918_44394[(2)] = null);

(statearr_41918_44394[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41910 === (6))){
var state_41909__$1 = state_41909;
var statearr_41919_44395 = state_41909__$1;
(statearr_41919_44395[(2)] = null);

(statearr_41919_44395[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41910 === (7))){
var inst_41905 = (state_41909[(2)]);
var state_41909__$1 = state_41909;
var statearr_41924_44396 = state_41909__$1;
(statearr_41924_44396[(2)] = inst_41905);

(statearr_41924_44396[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__44379,c__41555__auto___44383,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async))
;
return ((function (__44379,switch__41222__auto__,c__41555__auto___44383,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0 = (function (){
var statearr_41942 = [null,null,null,null,null,null,null];
(statearr_41942[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__);

(statearr_41942[(1)] = (1));

return statearr_41942;
});
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1 = (function (state_41909){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_41909);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e41943){var ex__41226__auto__ = e41943;
var statearr_41944_44405 = state_41909;
(statearr_41944_44405[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_41909[(4)]))){
var statearr_41945_44426 = state_41909;
(statearr_41945_44426[(1)] = cljs.core.first((state_41909[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44428 = state_41909;
state_41909 = G__44428;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = function(state_41909){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1.call(this,state_41909);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__;
})()
;})(__44379,switch__41222__auto__,c__41555__auto___44383,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async))
})();
var state__41557__auto__ = (function (){var statearr_41956 = f__41556__auto__();
(statearr_41956[(6)] = c__41555__auto___44383);

return statearr_41956;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
});})(__44379,c__41555__auto___44383,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async))
);


break;
case "async":
var c__41555__auto___44430 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__44379,c__41555__auto___44430,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async){
return (function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = ((function (__44379,c__41555__auto___44430,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async){
return (function (state_41984){
var state_val_41985 = (state_41984[(1)]);
if((state_val_41985 === (1))){
var state_41984__$1 = state_41984;
var statearr_42003_44431 = state_41984__$1;
(statearr_42003_44431[(2)] = null);

(statearr_42003_44431[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41985 === (2))){
var state_41984__$1 = state_41984;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_41984__$1,(4),jobs);
} else {
if((state_val_41985 === (3))){
var inst_41978 = (state_41984[(2)]);
var state_41984__$1 = state_41984;
return cljs.core.async.impl.ioc_helpers.return_chan(state_41984__$1,inst_41978);
} else {
if((state_val_41985 === (4))){
var inst_41970 = (state_41984[(2)]);
var inst_41971 = async(inst_41970);
var state_41984__$1 = state_41984;
if(cljs.core.truth_(inst_41971)){
var statearr_42007_44432 = state_41984__$1;
(statearr_42007_44432[(1)] = (5));

} else {
var statearr_42008_44433 = state_41984__$1;
(statearr_42008_44433[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41985 === (5))){
var state_41984__$1 = state_41984;
var statearr_42010_44434 = state_41984__$1;
(statearr_42010_44434[(2)] = null);

(statearr_42010_44434[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41985 === (6))){
var state_41984__$1 = state_41984;
var statearr_42011_44436 = state_41984__$1;
(statearr_42011_44436[(2)] = null);

(statearr_42011_44436[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41985 === (7))){
var inst_41976 = (state_41984[(2)]);
var state_41984__$1 = state_41984;
var statearr_42015_44443 = state_41984__$1;
(statearr_42015_44443[(2)] = inst_41976);

(statearr_42015_44443[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__44379,c__41555__auto___44430,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async))
;
return ((function (__44379,switch__41222__auto__,c__41555__auto___44430,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0 = (function (){
var statearr_42016 = [null,null,null,null,null,null,null];
(statearr_42016[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__);

(statearr_42016[(1)] = (1));

return statearr_42016;
});
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1 = (function (state_41984){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_41984);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42017){var ex__41226__auto__ = e42017;
var statearr_42022_44446 = state_41984;
(statearr_42022_44446[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_41984[(4)]))){
var statearr_42027_44447 = state_41984;
(statearr_42027_44447[(1)] = cljs.core.first((state_41984[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44448 = state_41984;
state_41984 = G__44448;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = function(state_41984){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1.call(this,state_41984);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__;
})()
;})(__44379,switch__41222__auto__,c__41555__auto___44430,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async))
})();
var state__41557__auto__ = (function (){var statearr_42032 = f__41556__auto__();
(statearr_42032[(6)] = c__41555__auto___44430);

return statearr_42032;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
});})(__44379,c__41555__auto___44430,G__41896_44380,G__41896_44381__$1,n__4741__auto___44378,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__41896_44381__$1)].join('')));

}

var G__44450 = (__44379 + (1));
__44379 = G__44450;
continue;
} else {
}
break;
}

var c__41555__auto___44451 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42058){
var state_val_42059 = (state_42058[(1)]);
if((state_val_42059 === (7))){
var inst_42054 = (state_42058[(2)]);
var state_42058__$1 = state_42058;
var statearr_42063_44453 = state_42058__$1;
(statearr_42063_44453[(2)] = inst_42054);

(statearr_42063_44453[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42059 === (1))){
var state_42058__$1 = state_42058;
var statearr_42064_44454 = state_42058__$1;
(statearr_42064_44454[(2)] = null);

(statearr_42064_44454[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42059 === (4))){
var inst_42039 = (state_42058[(7)]);
var inst_42039__$1 = (state_42058[(2)]);
var inst_42040 = (inst_42039__$1 == null);
var state_42058__$1 = (function (){var statearr_42065 = state_42058;
(statearr_42065[(7)] = inst_42039__$1);

return statearr_42065;
})();
if(cljs.core.truth_(inst_42040)){
var statearr_42066_44457 = state_42058__$1;
(statearr_42066_44457[(1)] = (5));

} else {
var statearr_42067_44458 = state_42058__$1;
(statearr_42067_44458[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42059 === (6))){
var inst_42044 = (state_42058[(8)]);
var inst_42039 = (state_42058[(7)]);
var inst_42044__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_42045 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_42046 = [inst_42039,inst_42044__$1];
var inst_42047 = (new cljs.core.PersistentVector(null,2,(5),inst_42045,inst_42046,null));
var state_42058__$1 = (function (){var statearr_42068 = state_42058;
(statearr_42068[(8)] = inst_42044__$1);

return statearr_42068;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_42058__$1,(8),jobs,inst_42047);
} else {
if((state_val_42059 === (3))){
var inst_42056 = (state_42058[(2)]);
var state_42058__$1 = state_42058;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42058__$1,inst_42056);
} else {
if((state_val_42059 === (2))){
var state_42058__$1 = state_42058;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42058__$1,(4),from);
} else {
if((state_val_42059 === (9))){
var inst_42051 = (state_42058[(2)]);
var state_42058__$1 = (function (){var statearr_42069 = state_42058;
(statearr_42069[(9)] = inst_42051);

return statearr_42069;
})();
var statearr_42070_44460 = state_42058__$1;
(statearr_42070_44460[(2)] = null);

(statearr_42070_44460[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42059 === (5))){
var inst_42042 = cljs.core.async.close_BANG_(jobs);
var state_42058__$1 = state_42058;
var statearr_42072_44461 = state_42058__$1;
(statearr_42072_44461[(2)] = inst_42042);

(statearr_42072_44461[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42059 === (8))){
var inst_42044 = (state_42058[(8)]);
var inst_42049 = (state_42058[(2)]);
var state_42058__$1 = (function (){var statearr_42073 = state_42058;
(statearr_42073[(10)] = inst_42049);

return statearr_42073;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_42058__$1,(9),results,inst_42044);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0 = (function (){
var statearr_42074 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_42074[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__);

(statearr_42074[(1)] = (1));

return statearr_42074;
});
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1 = (function (state_42058){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42058);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42075){var ex__41226__auto__ = e42075;
var statearr_42076_44464 = state_42058;
(statearr_42076_44464[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42058[(4)]))){
var statearr_42077_44466 = state_42058;
(statearr_42077_44466[(1)] = cljs.core.first((state_42058[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44467 = state_42058;
state_42058 = G__44467;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = function(state_42058){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1.call(this,state_42058);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42081 = f__41556__auto__();
(statearr_42081[(6)] = c__41555__auto___44451);

return statearr_42081;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


var c__41555__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42122){
var state_val_42123 = (state_42122[(1)]);
if((state_val_42123 === (7))){
var inst_42115 = (state_42122[(2)]);
var state_42122__$1 = state_42122;
var statearr_42124_44469 = state_42122__$1;
(statearr_42124_44469[(2)] = inst_42115);

(statearr_42124_44469[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (20))){
var state_42122__$1 = state_42122;
var statearr_42125_44470 = state_42122__$1;
(statearr_42125_44470[(2)] = null);

(statearr_42125_44470[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (1))){
var state_42122__$1 = state_42122;
var statearr_42126_44471 = state_42122__$1;
(statearr_42126_44471[(2)] = null);

(statearr_42126_44471[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (4))){
var inst_42084 = (state_42122[(7)]);
var inst_42084__$1 = (state_42122[(2)]);
var inst_42085 = (inst_42084__$1 == null);
var state_42122__$1 = (function (){var statearr_42127 = state_42122;
(statearr_42127[(7)] = inst_42084__$1);

return statearr_42127;
})();
if(cljs.core.truth_(inst_42085)){
var statearr_42128_44472 = state_42122__$1;
(statearr_42128_44472[(1)] = (5));

} else {
var statearr_42129_44473 = state_42122__$1;
(statearr_42129_44473[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (15))){
var inst_42097 = (state_42122[(8)]);
var state_42122__$1 = state_42122;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_42122__$1,(18),to,inst_42097);
} else {
if((state_val_42123 === (21))){
var inst_42110 = (state_42122[(2)]);
var state_42122__$1 = state_42122;
var statearr_42130_44474 = state_42122__$1;
(statearr_42130_44474[(2)] = inst_42110);

(statearr_42130_44474[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (13))){
var inst_42112 = (state_42122[(2)]);
var state_42122__$1 = (function (){var statearr_42131 = state_42122;
(statearr_42131[(9)] = inst_42112);

return statearr_42131;
})();
var statearr_42132_44475 = state_42122__$1;
(statearr_42132_44475[(2)] = null);

(statearr_42132_44475[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (6))){
var inst_42084 = (state_42122[(7)]);
var state_42122__$1 = state_42122;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42122__$1,(11),inst_42084);
} else {
if((state_val_42123 === (17))){
var inst_42105 = (state_42122[(2)]);
var state_42122__$1 = state_42122;
if(cljs.core.truth_(inst_42105)){
var statearr_42136_44476 = state_42122__$1;
(statearr_42136_44476[(1)] = (19));

} else {
var statearr_42137_44477 = state_42122__$1;
(statearr_42137_44477[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (3))){
var inst_42117 = (state_42122[(2)]);
var state_42122__$1 = state_42122;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42122__$1,inst_42117);
} else {
if((state_val_42123 === (12))){
var inst_42094 = (state_42122[(10)]);
var state_42122__$1 = state_42122;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42122__$1,(14),inst_42094);
} else {
if((state_val_42123 === (2))){
var state_42122__$1 = state_42122;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42122__$1,(4),results);
} else {
if((state_val_42123 === (19))){
var state_42122__$1 = state_42122;
var statearr_42138_44478 = state_42122__$1;
(statearr_42138_44478[(2)] = null);

(statearr_42138_44478[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (11))){
var inst_42094 = (state_42122[(2)]);
var state_42122__$1 = (function (){var statearr_42142 = state_42122;
(statearr_42142[(10)] = inst_42094);

return statearr_42142;
})();
var statearr_42143_44483 = state_42122__$1;
(statearr_42143_44483[(2)] = null);

(statearr_42143_44483[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (9))){
var state_42122__$1 = state_42122;
var statearr_42144_44485 = state_42122__$1;
(statearr_42144_44485[(2)] = null);

(statearr_42144_44485[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (5))){
var state_42122__$1 = state_42122;
if(cljs.core.truth_(close_QMARK_)){
var statearr_42145_44488 = state_42122__$1;
(statearr_42145_44488[(1)] = (8));

} else {
var statearr_42146_44489 = state_42122__$1;
(statearr_42146_44489[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (14))){
var inst_42097 = (state_42122[(8)]);
var inst_42099 = (state_42122[(11)]);
var inst_42097__$1 = (state_42122[(2)]);
var inst_42098 = (inst_42097__$1 == null);
var inst_42099__$1 = cljs.core.not(inst_42098);
var state_42122__$1 = (function (){var statearr_42147 = state_42122;
(statearr_42147[(8)] = inst_42097__$1);

(statearr_42147[(11)] = inst_42099__$1);

return statearr_42147;
})();
if(inst_42099__$1){
var statearr_42148_44490 = state_42122__$1;
(statearr_42148_44490[(1)] = (15));

} else {
var statearr_42149_44493 = state_42122__$1;
(statearr_42149_44493[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (16))){
var inst_42099 = (state_42122[(11)]);
var state_42122__$1 = state_42122;
var statearr_42150_44494 = state_42122__$1;
(statearr_42150_44494[(2)] = inst_42099);

(statearr_42150_44494[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (10))){
var inst_42091 = (state_42122[(2)]);
var state_42122__$1 = state_42122;
var statearr_42151_44495 = state_42122__$1;
(statearr_42151_44495[(2)] = inst_42091);

(statearr_42151_44495[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (18))){
var inst_42102 = (state_42122[(2)]);
var state_42122__$1 = state_42122;
var statearr_42152_44496 = state_42122__$1;
(statearr_42152_44496[(2)] = inst_42102);

(statearr_42152_44496[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42123 === (8))){
var inst_42088 = cljs.core.async.close_BANG_(to);
var state_42122__$1 = state_42122;
var statearr_42153_44497 = state_42122__$1;
(statearr_42153_44497[(2)] = inst_42088);

(statearr_42153_44497[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0 = (function (){
var statearr_42154 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42154[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__);

(statearr_42154[(1)] = (1));

return statearr_42154;
});
var cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1 = (function (state_42122){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42122);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42157){var ex__41226__auto__ = e42157;
var statearr_42158_44505 = state_42122;
(statearr_42158_44505[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42122[(4)]))){
var statearr_42159_44506 = state_42122;
(statearr_42159_44506[(1)] = cljs.core.first((state_42122[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44508 = state_42122;
state_42122 = G__44508;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__ = function(state_42122){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1.call(this,state_42122);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__41223__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42160 = f__41556__auto__();
(statearr_42160[(6)] = c__41555__auto__);

return statearr_42160;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

return c__41555__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). The
 *   presumption is that af will return immediately, having launched some
 *   asynchronous operation whose completion/callback will put results on
 *   the channel, then close! it. Outputs will be returned in order
 *   relative to the inputs. By default, the to channel will be closed
 *   when the from channel closes, but can be determined by the close?
 *   parameter. Will stop consuming the from channel if the to channel
 *   closes. See also pipeline, pipeline-blocking.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__42163 = arguments.length;
switch (G__42163) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__42165 = arguments.length;
switch (G__42165) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__42171 = arguments.length;
switch (G__42171) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__41555__auto___44522 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42201){
var state_val_42202 = (state_42201[(1)]);
if((state_val_42202 === (7))){
var inst_42197 = (state_42201[(2)]);
var state_42201__$1 = state_42201;
var statearr_42203_44523 = state_42201__$1;
(statearr_42203_44523[(2)] = inst_42197);

(statearr_42203_44523[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (1))){
var state_42201__$1 = state_42201;
var statearr_42212_44524 = state_42201__$1;
(statearr_42212_44524[(2)] = null);

(statearr_42212_44524[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (4))){
var inst_42178 = (state_42201[(7)]);
var inst_42178__$1 = (state_42201[(2)]);
var inst_42179 = (inst_42178__$1 == null);
var state_42201__$1 = (function (){var statearr_42223 = state_42201;
(statearr_42223[(7)] = inst_42178__$1);

return statearr_42223;
})();
if(cljs.core.truth_(inst_42179)){
var statearr_42234_44525 = state_42201__$1;
(statearr_42234_44525[(1)] = (5));

} else {
var statearr_42240_44529 = state_42201__$1;
(statearr_42240_44529[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (13))){
var state_42201__$1 = state_42201;
var statearr_42241_44530 = state_42201__$1;
(statearr_42241_44530[(2)] = null);

(statearr_42241_44530[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (6))){
var inst_42178 = (state_42201[(7)]);
var inst_42184 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_42178) : p.call(null,inst_42178));
var state_42201__$1 = state_42201;
if(cljs.core.truth_(inst_42184)){
var statearr_42242_44531 = state_42201__$1;
(statearr_42242_44531[(1)] = (9));

} else {
var statearr_42243_44532 = state_42201__$1;
(statearr_42243_44532[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (3))){
var inst_42199 = (state_42201[(2)]);
var state_42201__$1 = state_42201;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42201__$1,inst_42199);
} else {
if((state_val_42202 === (12))){
var state_42201__$1 = state_42201;
var statearr_42250_44533 = state_42201__$1;
(statearr_42250_44533[(2)] = null);

(statearr_42250_44533[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (2))){
var state_42201__$1 = state_42201;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42201__$1,(4),ch);
} else {
if((state_val_42202 === (11))){
var inst_42178 = (state_42201[(7)]);
var inst_42188 = (state_42201[(2)]);
var state_42201__$1 = state_42201;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_42201__$1,(8),inst_42188,inst_42178);
} else {
if((state_val_42202 === (9))){
var state_42201__$1 = state_42201;
var statearr_42254_44538 = state_42201__$1;
(statearr_42254_44538[(2)] = tc);

(statearr_42254_44538[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (5))){
var inst_42181 = cljs.core.async.close_BANG_(tc);
var inst_42182 = cljs.core.async.close_BANG_(fc);
var state_42201__$1 = (function (){var statearr_42255 = state_42201;
(statearr_42255[(8)] = inst_42181);

return statearr_42255;
})();
var statearr_42256_44539 = state_42201__$1;
(statearr_42256_44539[(2)] = inst_42182);

(statearr_42256_44539[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (14))){
var inst_42195 = (state_42201[(2)]);
var state_42201__$1 = state_42201;
var statearr_42258_44540 = state_42201__$1;
(statearr_42258_44540[(2)] = inst_42195);

(statearr_42258_44540[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (10))){
var state_42201__$1 = state_42201;
var statearr_42261_44541 = state_42201__$1;
(statearr_42261_44541[(2)] = fc);

(statearr_42261_44541[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42202 === (8))){
var inst_42190 = (state_42201[(2)]);
var state_42201__$1 = state_42201;
if(cljs.core.truth_(inst_42190)){
var statearr_42264_44542 = state_42201__$1;
(statearr_42264_44542[(1)] = (12));

} else {
var statearr_42265_44543 = state_42201__$1;
(statearr_42265_44543[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_42267 = [null,null,null,null,null,null,null,null,null];
(statearr_42267[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_42267[(1)] = (1));

return statearr_42267;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_42201){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42201);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42269){var ex__41226__auto__ = e42269;
var statearr_42270_44548 = state_42201;
(statearr_42270_44548[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42201[(4)]))){
var statearr_42271_44556 = state_42201;
(statearr_42271_44556[(1)] = cljs.core.first((state_42201[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44557 = state_42201;
state_42201 = G__44557;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_42201){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_42201);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42272 = f__41556__auto__();
(statearr_42272[(6)] = c__41555__auto___44522);

return statearr_42272;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__41555__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42298){
var state_val_42299 = (state_42298[(1)]);
if((state_val_42299 === (7))){
var inst_42294 = (state_42298[(2)]);
var state_42298__$1 = state_42298;
var statearr_42300_44574 = state_42298__$1;
(statearr_42300_44574[(2)] = inst_42294);

(statearr_42300_44574[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (1))){
var inst_42273 = init;
var inst_42274 = inst_42273;
var state_42298__$1 = (function (){var statearr_42301 = state_42298;
(statearr_42301[(7)] = inst_42274);

return statearr_42301;
})();
var statearr_42306_44575 = state_42298__$1;
(statearr_42306_44575[(2)] = null);

(statearr_42306_44575[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (4))){
var inst_42277 = (state_42298[(8)]);
var inst_42277__$1 = (state_42298[(2)]);
var inst_42278 = (inst_42277__$1 == null);
var state_42298__$1 = (function (){var statearr_42307 = state_42298;
(statearr_42307[(8)] = inst_42277__$1);

return statearr_42307;
})();
if(cljs.core.truth_(inst_42278)){
var statearr_42308_44576 = state_42298__$1;
(statearr_42308_44576[(1)] = (5));

} else {
var statearr_42309_44577 = state_42298__$1;
(statearr_42309_44577[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (6))){
var inst_42274 = (state_42298[(7)]);
var inst_42277 = (state_42298[(8)]);
var inst_42282 = (state_42298[(9)]);
var inst_42282__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_42274,inst_42277) : f.call(null,inst_42274,inst_42277));
var inst_42286 = cljs.core.reduced_QMARK_(inst_42282__$1);
var state_42298__$1 = (function (){var statearr_42313 = state_42298;
(statearr_42313[(9)] = inst_42282__$1);

return statearr_42313;
})();
if(inst_42286){
var statearr_42314_44578 = state_42298__$1;
(statearr_42314_44578[(1)] = (8));

} else {
var statearr_42315_44579 = state_42298__$1;
(statearr_42315_44579[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (3))){
var inst_42296 = (state_42298[(2)]);
var state_42298__$1 = state_42298;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42298__$1,inst_42296);
} else {
if((state_val_42299 === (2))){
var state_42298__$1 = state_42298;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42298__$1,(4),ch);
} else {
if((state_val_42299 === (9))){
var inst_42282 = (state_42298[(9)]);
var inst_42274 = inst_42282;
var state_42298__$1 = (function (){var statearr_42316 = state_42298;
(statearr_42316[(7)] = inst_42274);

return statearr_42316;
})();
var statearr_42317_44584 = state_42298__$1;
(statearr_42317_44584[(2)] = null);

(statearr_42317_44584[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (5))){
var inst_42274 = (state_42298[(7)]);
var state_42298__$1 = state_42298;
var statearr_42318_44589 = state_42298__$1;
(statearr_42318_44589[(2)] = inst_42274);

(statearr_42318_44589[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (10))){
var inst_42292 = (state_42298[(2)]);
var state_42298__$1 = state_42298;
var statearr_42319_44591 = state_42298__$1;
(statearr_42319_44591[(2)] = inst_42292);

(statearr_42319_44591[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42299 === (8))){
var inst_42282 = (state_42298[(9)]);
var inst_42288 = cljs.core.deref(inst_42282);
var state_42298__$1 = state_42298;
var statearr_42320_44595 = state_42298__$1;
(statearr_42320_44595[(2)] = inst_42288);

(statearr_42320_44595[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__41223__auto__ = null;
var cljs$core$async$reduce_$_state_machine__41223__auto____0 = (function (){
var statearr_42321 = [null,null,null,null,null,null,null,null,null,null];
(statearr_42321[(0)] = cljs$core$async$reduce_$_state_machine__41223__auto__);

(statearr_42321[(1)] = (1));

return statearr_42321;
});
var cljs$core$async$reduce_$_state_machine__41223__auto____1 = (function (state_42298){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42298);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42322){var ex__41226__auto__ = e42322;
var statearr_42323_44596 = state_42298;
(statearr_42323_44596[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42298[(4)]))){
var statearr_42324_44597 = state_42298;
(statearr_42324_44597[(1)] = cljs.core.first((state_42298[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44601 = state_42298;
state_42298 = G__44601;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__41223__auto__ = function(state_42298){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__41223__auto____1.call(this,state_42298);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__41223__auto____0;
cljs$core$async$reduce_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__41223__auto____1;
return cljs$core$async$reduce_$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42325 = f__41556__auto__();
(statearr_42325[(6)] = c__41555__auto__);

return statearr_42325;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

return c__41555__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__41555__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42331){
var state_val_42332 = (state_42331[(1)]);
if((state_val_42332 === (1))){
var inst_42326 = cljs.core.async.reduce(f__$1,init,ch);
var state_42331__$1 = state_42331;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42331__$1,(2),inst_42326);
} else {
if((state_val_42332 === (2))){
var inst_42328 = (state_42331[(2)]);
var inst_42329 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_42328) : f__$1.call(null,inst_42328));
var state_42331__$1 = state_42331;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42331__$1,inst_42329);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__41223__auto__ = null;
var cljs$core$async$transduce_$_state_machine__41223__auto____0 = (function (){
var statearr_42333 = [null,null,null,null,null,null,null];
(statearr_42333[(0)] = cljs$core$async$transduce_$_state_machine__41223__auto__);

(statearr_42333[(1)] = (1));

return statearr_42333;
});
var cljs$core$async$transduce_$_state_machine__41223__auto____1 = (function (state_42331){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42331);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42334){var ex__41226__auto__ = e42334;
var statearr_42335_44602 = state_42331;
(statearr_42335_44602[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42331[(4)]))){
var statearr_42336_44603 = state_42331;
(statearr_42336_44603[(1)] = cljs.core.first((state_42331[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44604 = state_42331;
state_42331 = G__44604;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__41223__auto__ = function(state_42331){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__41223__auto____1.call(this,state_42331);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__41223__auto____0;
cljs$core$async$transduce_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__41223__auto____1;
return cljs$core$async$transduce_$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42337 = f__41556__auto__();
(statearr_42337[(6)] = c__41555__auto__);

return statearr_42337;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

return c__41555__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__42339 = arguments.length;
switch (G__42339) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__41555__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42373){
var state_val_42374 = (state_42373[(1)]);
if((state_val_42374 === (7))){
var inst_42355 = (state_42373[(2)]);
var state_42373__$1 = state_42373;
var statearr_42375_44614 = state_42373__$1;
(statearr_42375_44614[(2)] = inst_42355);

(statearr_42375_44614[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (1))){
var inst_42349 = cljs.core.seq(coll);
var inst_42350 = inst_42349;
var state_42373__$1 = (function (){var statearr_42376 = state_42373;
(statearr_42376[(7)] = inst_42350);

return statearr_42376;
})();
var statearr_42377_44618 = state_42373__$1;
(statearr_42377_44618[(2)] = null);

(statearr_42377_44618[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (4))){
var inst_42350 = (state_42373[(7)]);
var inst_42353 = cljs.core.first(inst_42350);
var state_42373__$1 = state_42373;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_42373__$1,(7),ch,inst_42353);
} else {
if((state_val_42374 === (13))){
var inst_42367 = (state_42373[(2)]);
var state_42373__$1 = state_42373;
var statearr_42378_44622 = state_42373__$1;
(statearr_42378_44622[(2)] = inst_42367);

(statearr_42378_44622[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (6))){
var inst_42358 = (state_42373[(2)]);
var state_42373__$1 = state_42373;
if(cljs.core.truth_(inst_42358)){
var statearr_42379_44623 = state_42373__$1;
(statearr_42379_44623[(1)] = (8));

} else {
var statearr_42380_44624 = state_42373__$1;
(statearr_42380_44624[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (3))){
var inst_42371 = (state_42373[(2)]);
var state_42373__$1 = state_42373;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42373__$1,inst_42371);
} else {
if((state_val_42374 === (12))){
var state_42373__$1 = state_42373;
var statearr_42381_44628 = state_42373__$1;
(statearr_42381_44628[(2)] = null);

(statearr_42381_44628[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (2))){
var inst_42350 = (state_42373[(7)]);
var state_42373__$1 = state_42373;
if(cljs.core.truth_(inst_42350)){
var statearr_42382_44629 = state_42373__$1;
(statearr_42382_44629[(1)] = (4));

} else {
var statearr_42383_44630 = state_42373__$1;
(statearr_42383_44630[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (11))){
var inst_42364 = cljs.core.async.close_BANG_(ch);
var state_42373__$1 = state_42373;
var statearr_42384_44631 = state_42373__$1;
(statearr_42384_44631[(2)] = inst_42364);

(statearr_42384_44631[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (9))){
var state_42373__$1 = state_42373;
if(cljs.core.truth_(close_QMARK_)){
var statearr_42385_44632 = state_42373__$1;
(statearr_42385_44632[(1)] = (11));

} else {
var statearr_42386_44633 = state_42373__$1;
(statearr_42386_44633[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (5))){
var inst_42350 = (state_42373[(7)]);
var state_42373__$1 = state_42373;
var statearr_42387_44637 = state_42373__$1;
(statearr_42387_44637[(2)] = inst_42350);

(statearr_42387_44637[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (10))){
var inst_42369 = (state_42373[(2)]);
var state_42373__$1 = state_42373;
var statearr_42388_44638 = state_42373__$1;
(statearr_42388_44638[(2)] = inst_42369);

(statearr_42388_44638[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42374 === (8))){
var inst_42350 = (state_42373[(7)]);
var inst_42360 = cljs.core.next(inst_42350);
var inst_42350__$1 = inst_42360;
var state_42373__$1 = (function (){var statearr_42389 = state_42373;
(statearr_42389[(7)] = inst_42350__$1);

return statearr_42389;
})();
var statearr_42390_44639 = state_42373__$1;
(statearr_42390_44639[(2)] = null);

(statearr_42390_44639[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_42391 = [null,null,null,null,null,null,null,null];
(statearr_42391[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_42391[(1)] = (1));

return statearr_42391;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_42373){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42373);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42392){var ex__41226__auto__ = e42392;
var statearr_42393_44644 = state_42373;
(statearr_42393_44644[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42373[(4)]))){
var statearr_42394_44645 = state_42373;
(statearr_42394_44645[(1)] = cljs.core.first((state_42373[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44650 = state_42373;
state_42373 = G__44650;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_42373){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_42373);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42398 = f__41556__auto__();
(statearr_42398[(6)] = c__41555__auto__);

return statearr_42398;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

return c__41555__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__42403 = arguments.length;
switch (G__42403) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_44660 = (function (_){
var x__4550__auto__ = (((_ == null))?null:_);
var m__4551__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4551__auto__.call(null,_));
} else {
var m__4549__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4549__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_44660(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_44665 = (function (m,ch,close_QMARK_){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4551__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4549__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4549__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_44665(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_44671 = (function (m,ch){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4551__auto__.call(null,m,ch));
} else {
var m__4549__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4549__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_44671(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_44680 = (function (m){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4551__auto__.call(null,m));
} else {
var m__4549__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4549__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_44680(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async42418 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42418 = (function (ch,cs,meta42419){
this.ch = ch;
this.cs = cs;
this.meta42419 = meta42419;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_42420,meta42419__$1){
var self__ = this;
var _42420__$1 = this;
return (new cljs.core.async.t_cljs$core$async42418(self__.ch,self__.cs,meta42419__$1));
}));

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_42420){
var self__ = this;
var _42420__$1 = this;
return self__.meta42419;
}));

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async42418.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async42418.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta42419","meta42419",-1774523715,null)], null);
}));

(cljs.core.async.t_cljs$core$async42418.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async42418.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42418");

(cljs.core.async.t_cljs$core$async42418.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async42418");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async42418.
 */
cljs.core.async.__GT_t_cljs$core$async42418 = (function cljs$core$async$mult_$___GT_t_cljs$core$async42418(ch__$1,cs__$1,meta42419){
return (new cljs.core.async.t_cljs$core$async42418(ch__$1,cs__$1,meta42419));
});

}

return (new cljs.core.async.t_cljs$core$async42418(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__41555__auto___44686 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42579){
var state_val_42580 = (state_42579[(1)]);
if((state_val_42580 === (7))){
var inst_42575 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42582_44687 = state_42579__$1;
(statearr_42582_44687[(2)] = inst_42575);

(statearr_42582_44687[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (20))){
var inst_42463 = (state_42579[(7)]);
var inst_42479 = cljs.core.first(inst_42463);
var inst_42480 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_42479,(0),null);
var inst_42481 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_42479,(1),null);
var state_42579__$1 = (function (){var statearr_42583 = state_42579;
(statearr_42583[(8)] = inst_42480);

return statearr_42583;
})();
if(cljs.core.truth_(inst_42481)){
var statearr_42584_44688 = state_42579__$1;
(statearr_42584_44688[(1)] = (22));

} else {
var statearr_42585_44689 = state_42579__$1;
(statearr_42585_44689[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (27))){
var inst_42510 = (state_42579[(9)]);
var inst_42512 = (state_42579[(10)]);
var inst_42431 = (state_42579[(11)]);
var inst_42517 = (state_42579[(12)]);
var inst_42517__$1 = cljs.core._nth(inst_42510,inst_42512);
var inst_42518 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_42517__$1,inst_42431,done);
var state_42579__$1 = (function (){var statearr_42598 = state_42579;
(statearr_42598[(12)] = inst_42517__$1);

return statearr_42598;
})();
if(cljs.core.truth_(inst_42518)){
var statearr_42599_44691 = state_42579__$1;
(statearr_42599_44691[(1)] = (30));

} else {
var statearr_42600_44692 = state_42579__$1;
(statearr_42600_44692[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (1))){
var state_42579__$1 = state_42579;
var statearr_42601_44693 = state_42579__$1;
(statearr_42601_44693[(2)] = null);

(statearr_42601_44693[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (24))){
var inst_42463 = (state_42579[(7)]);
var inst_42486 = (state_42579[(2)]);
var inst_42487 = cljs.core.next(inst_42463);
var inst_42441 = inst_42487;
var inst_42442 = null;
var inst_42443 = (0);
var inst_42444 = (0);
var state_42579__$1 = (function (){var statearr_42602 = state_42579;
(statearr_42602[(13)] = inst_42441);

(statearr_42602[(14)] = inst_42442);

(statearr_42602[(15)] = inst_42443);

(statearr_42602[(16)] = inst_42444);

(statearr_42602[(17)] = inst_42486);

return statearr_42602;
})();
var statearr_42603_44695 = state_42579__$1;
(statearr_42603_44695[(2)] = null);

(statearr_42603_44695[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (39))){
var state_42579__$1 = state_42579;
var statearr_42609_44696 = state_42579__$1;
(statearr_42609_44696[(2)] = null);

(statearr_42609_44696[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (4))){
var inst_42431 = (state_42579[(11)]);
var inst_42431__$1 = (state_42579[(2)]);
var inst_42432 = (inst_42431__$1 == null);
var state_42579__$1 = (function (){var statearr_42610 = state_42579;
(statearr_42610[(11)] = inst_42431__$1);

return statearr_42610;
})();
if(cljs.core.truth_(inst_42432)){
var statearr_42611_44697 = state_42579__$1;
(statearr_42611_44697[(1)] = (5));

} else {
var statearr_42612_44698 = state_42579__$1;
(statearr_42612_44698[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (15))){
var inst_42441 = (state_42579[(13)]);
var inst_42442 = (state_42579[(14)]);
var inst_42443 = (state_42579[(15)]);
var inst_42444 = (state_42579[(16)]);
var inst_42459 = (state_42579[(2)]);
var inst_42460 = (inst_42444 + (1));
var tmp42605 = inst_42441;
var tmp42606 = inst_42442;
var tmp42607 = inst_42443;
var inst_42441__$1 = tmp42605;
var inst_42442__$1 = tmp42606;
var inst_42443__$1 = tmp42607;
var inst_42444__$1 = inst_42460;
var state_42579__$1 = (function (){var statearr_42613 = state_42579;
(statearr_42613[(13)] = inst_42441__$1);

(statearr_42613[(14)] = inst_42442__$1);

(statearr_42613[(15)] = inst_42443__$1);

(statearr_42613[(16)] = inst_42444__$1);

(statearr_42613[(18)] = inst_42459);

return statearr_42613;
})();
var statearr_42614_44701 = state_42579__$1;
(statearr_42614_44701[(2)] = null);

(statearr_42614_44701[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (21))){
var inst_42490 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42618_44705 = state_42579__$1;
(statearr_42618_44705[(2)] = inst_42490);

(statearr_42618_44705[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (31))){
var inst_42517 = (state_42579[(12)]);
var inst_42521 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_42517);
var state_42579__$1 = state_42579;
var statearr_42619_44706 = state_42579__$1;
(statearr_42619_44706[(2)] = inst_42521);

(statearr_42619_44706[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (32))){
var inst_42509 = (state_42579[(19)]);
var inst_42510 = (state_42579[(9)]);
var inst_42512 = (state_42579[(10)]);
var inst_42511 = (state_42579[(20)]);
var inst_42529 = (state_42579[(2)]);
var inst_42530 = (inst_42512 + (1));
var tmp42615 = inst_42509;
var tmp42616 = inst_42510;
var tmp42617 = inst_42511;
var inst_42509__$1 = tmp42615;
var inst_42510__$1 = tmp42616;
var inst_42511__$1 = tmp42617;
var inst_42512__$1 = inst_42530;
var state_42579__$1 = (function (){var statearr_42620 = state_42579;
(statearr_42620[(19)] = inst_42509__$1);

(statearr_42620[(9)] = inst_42510__$1);

(statearr_42620[(10)] = inst_42512__$1);

(statearr_42620[(21)] = inst_42529);

(statearr_42620[(20)] = inst_42511__$1);

return statearr_42620;
})();
var statearr_42621_44707 = state_42579__$1;
(statearr_42621_44707[(2)] = null);

(statearr_42621_44707[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (40))){
var inst_42542 = (state_42579[(22)]);
var inst_42549 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_42542);
var state_42579__$1 = state_42579;
var statearr_42623_44708 = state_42579__$1;
(statearr_42623_44708[(2)] = inst_42549);

(statearr_42623_44708[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (33))){
var inst_42533 = (state_42579[(23)]);
var inst_42535 = cljs.core.chunked_seq_QMARK_(inst_42533);
var state_42579__$1 = state_42579;
if(inst_42535){
var statearr_42624_44709 = state_42579__$1;
(statearr_42624_44709[(1)] = (36));

} else {
var statearr_42625_44710 = state_42579__$1;
(statearr_42625_44710[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (13))){
var inst_42453 = (state_42579[(24)]);
var inst_42456 = cljs.core.async.close_BANG_(inst_42453);
var state_42579__$1 = state_42579;
var statearr_42626_44711 = state_42579__$1;
(statearr_42626_44711[(2)] = inst_42456);

(statearr_42626_44711[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (22))){
var inst_42480 = (state_42579[(8)]);
var inst_42483 = cljs.core.async.close_BANG_(inst_42480);
var state_42579__$1 = state_42579;
var statearr_42627_44712 = state_42579__$1;
(statearr_42627_44712[(2)] = inst_42483);

(statearr_42627_44712[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (36))){
var inst_42533 = (state_42579[(23)]);
var inst_42537 = cljs.core.chunk_first(inst_42533);
var inst_42538 = cljs.core.chunk_rest(inst_42533);
var inst_42539 = cljs.core.count(inst_42537);
var inst_42509 = inst_42538;
var inst_42510 = inst_42537;
var inst_42511 = inst_42539;
var inst_42512 = (0);
var state_42579__$1 = (function (){var statearr_42628 = state_42579;
(statearr_42628[(19)] = inst_42509);

(statearr_42628[(9)] = inst_42510);

(statearr_42628[(10)] = inst_42512);

(statearr_42628[(20)] = inst_42511);

return statearr_42628;
})();
var statearr_42632_44715 = state_42579__$1;
(statearr_42632_44715[(2)] = null);

(statearr_42632_44715[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (41))){
var inst_42533 = (state_42579[(23)]);
var inst_42554 = (state_42579[(2)]);
var inst_42555 = cljs.core.next(inst_42533);
var inst_42509 = inst_42555;
var inst_42510 = null;
var inst_42511 = (0);
var inst_42512 = (0);
var state_42579__$1 = (function (){var statearr_42633 = state_42579;
(statearr_42633[(25)] = inst_42554);

(statearr_42633[(19)] = inst_42509);

(statearr_42633[(9)] = inst_42510);

(statearr_42633[(10)] = inst_42512);

(statearr_42633[(20)] = inst_42511);

return statearr_42633;
})();
var statearr_42634_44719 = state_42579__$1;
(statearr_42634_44719[(2)] = null);

(statearr_42634_44719[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (43))){
var state_42579__$1 = state_42579;
var statearr_42635_44721 = state_42579__$1;
(statearr_42635_44721[(2)] = null);

(statearr_42635_44721[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (29))){
var inst_42563 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42636_44723 = state_42579__$1;
(statearr_42636_44723[(2)] = inst_42563);

(statearr_42636_44723[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (44))){
var inst_42572 = (state_42579[(2)]);
var state_42579__$1 = (function (){var statearr_42638 = state_42579;
(statearr_42638[(26)] = inst_42572);

return statearr_42638;
})();
var statearr_42639_44726 = state_42579__$1;
(statearr_42639_44726[(2)] = null);

(statearr_42639_44726[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (6))){
var inst_42501 = (state_42579[(27)]);
var inst_42500 = cljs.core.deref(cs);
var inst_42501__$1 = cljs.core.keys(inst_42500);
var inst_42502 = cljs.core.count(inst_42501__$1);
var inst_42503 = cljs.core.reset_BANG_(dctr,inst_42502);
var inst_42508 = cljs.core.seq(inst_42501__$1);
var inst_42509 = inst_42508;
var inst_42510 = null;
var inst_42511 = (0);
var inst_42512 = (0);
var state_42579__$1 = (function (){var statearr_42640 = state_42579;
(statearr_42640[(19)] = inst_42509);

(statearr_42640[(9)] = inst_42510);

(statearr_42640[(10)] = inst_42512);

(statearr_42640[(27)] = inst_42501__$1);

(statearr_42640[(28)] = inst_42503);

(statearr_42640[(20)] = inst_42511);

return statearr_42640;
})();
var statearr_42641_44733 = state_42579__$1;
(statearr_42641_44733[(2)] = null);

(statearr_42641_44733[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (28))){
var inst_42533 = (state_42579[(23)]);
var inst_42509 = (state_42579[(19)]);
var inst_42533__$1 = cljs.core.seq(inst_42509);
var state_42579__$1 = (function (){var statearr_42642 = state_42579;
(statearr_42642[(23)] = inst_42533__$1);

return statearr_42642;
})();
if(inst_42533__$1){
var statearr_42643_44736 = state_42579__$1;
(statearr_42643_44736[(1)] = (33));

} else {
var statearr_42644_44738 = state_42579__$1;
(statearr_42644_44738[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (25))){
var inst_42512 = (state_42579[(10)]);
var inst_42511 = (state_42579[(20)]);
var inst_42514 = (inst_42512 < inst_42511);
var inst_42515 = inst_42514;
var state_42579__$1 = state_42579;
if(cljs.core.truth_(inst_42515)){
var statearr_42645_44739 = state_42579__$1;
(statearr_42645_44739[(1)] = (27));

} else {
var statearr_42646_44740 = state_42579__$1;
(statearr_42646_44740[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (34))){
var state_42579__$1 = state_42579;
var statearr_42647_44741 = state_42579__$1;
(statearr_42647_44741[(2)] = null);

(statearr_42647_44741[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (17))){
var state_42579__$1 = state_42579;
var statearr_42648_44742 = state_42579__$1;
(statearr_42648_44742[(2)] = null);

(statearr_42648_44742[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (3))){
var inst_42577 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42579__$1,inst_42577);
} else {
if((state_val_42580 === (12))){
var inst_42496 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42649_44743 = state_42579__$1;
(statearr_42649_44743[(2)] = inst_42496);

(statearr_42649_44743[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (2))){
var state_42579__$1 = state_42579;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42579__$1,(4),ch);
} else {
if((state_val_42580 === (23))){
var state_42579__$1 = state_42579;
var statearr_42655_44744 = state_42579__$1;
(statearr_42655_44744[(2)] = null);

(statearr_42655_44744[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (35))){
var inst_42561 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42662_44745 = state_42579__$1;
(statearr_42662_44745[(2)] = inst_42561);

(statearr_42662_44745[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (19))){
var inst_42463 = (state_42579[(7)]);
var inst_42471 = cljs.core.chunk_first(inst_42463);
var inst_42472 = cljs.core.chunk_rest(inst_42463);
var inst_42473 = cljs.core.count(inst_42471);
var inst_42441 = inst_42472;
var inst_42442 = inst_42471;
var inst_42443 = inst_42473;
var inst_42444 = (0);
var state_42579__$1 = (function (){var statearr_42663 = state_42579;
(statearr_42663[(13)] = inst_42441);

(statearr_42663[(14)] = inst_42442);

(statearr_42663[(15)] = inst_42443);

(statearr_42663[(16)] = inst_42444);

return statearr_42663;
})();
var statearr_42664_44747 = state_42579__$1;
(statearr_42664_44747[(2)] = null);

(statearr_42664_44747[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (11))){
var inst_42441 = (state_42579[(13)]);
var inst_42463 = (state_42579[(7)]);
var inst_42463__$1 = cljs.core.seq(inst_42441);
var state_42579__$1 = (function (){var statearr_42665 = state_42579;
(statearr_42665[(7)] = inst_42463__$1);

return statearr_42665;
})();
if(inst_42463__$1){
var statearr_42666_44755 = state_42579__$1;
(statearr_42666_44755[(1)] = (16));

} else {
var statearr_42667_44756 = state_42579__$1;
(statearr_42667_44756[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (9))){
var inst_42498 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42668_44757 = state_42579__$1;
(statearr_42668_44757[(2)] = inst_42498);

(statearr_42668_44757[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (5))){
var inst_42438 = cljs.core.deref(cs);
var inst_42439 = cljs.core.seq(inst_42438);
var inst_42441 = inst_42439;
var inst_42442 = null;
var inst_42443 = (0);
var inst_42444 = (0);
var state_42579__$1 = (function (){var statearr_42669 = state_42579;
(statearr_42669[(13)] = inst_42441);

(statearr_42669[(14)] = inst_42442);

(statearr_42669[(15)] = inst_42443);

(statearr_42669[(16)] = inst_42444);

return statearr_42669;
})();
var statearr_42670_44761 = state_42579__$1;
(statearr_42670_44761[(2)] = null);

(statearr_42670_44761[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (14))){
var state_42579__$1 = state_42579;
var statearr_42672_44764 = state_42579__$1;
(statearr_42672_44764[(2)] = null);

(statearr_42672_44764[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (45))){
var inst_42569 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42673_44766 = state_42579__$1;
(statearr_42673_44766[(2)] = inst_42569);

(statearr_42673_44766[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (26))){
var inst_42501 = (state_42579[(27)]);
var inst_42565 = (state_42579[(2)]);
var inst_42566 = cljs.core.seq(inst_42501);
var state_42579__$1 = (function (){var statearr_42674 = state_42579;
(statearr_42674[(29)] = inst_42565);

return statearr_42674;
})();
if(inst_42566){
var statearr_42675_44768 = state_42579__$1;
(statearr_42675_44768[(1)] = (42));

} else {
var statearr_42676_44769 = state_42579__$1;
(statearr_42676_44769[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (16))){
var inst_42463 = (state_42579[(7)]);
var inst_42465 = cljs.core.chunked_seq_QMARK_(inst_42463);
var state_42579__$1 = state_42579;
if(inst_42465){
var statearr_42677_44773 = state_42579__$1;
(statearr_42677_44773[(1)] = (19));

} else {
var statearr_42678_44774 = state_42579__$1;
(statearr_42678_44774[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (38))){
var inst_42558 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42682_44779 = state_42579__$1;
(statearr_42682_44779[(2)] = inst_42558);

(statearr_42682_44779[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (30))){
var state_42579__$1 = state_42579;
var statearr_42684_44780 = state_42579__$1;
(statearr_42684_44780[(2)] = null);

(statearr_42684_44780[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (10))){
var inst_42442 = (state_42579[(14)]);
var inst_42444 = (state_42579[(16)]);
var inst_42452 = cljs.core._nth(inst_42442,inst_42444);
var inst_42453 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_42452,(0),null);
var inst_42454 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_42452,(1),null);
var state_42579__$1 = (function (){var statearr_42688 = state_42579;
(statearr_42688[(24)] = inst_42453);

return statearr_42688;
})();
if(cljs.core.truth_(inst_42454)){
var statearr_42689_44787 = state_42579__$1;
(statearr_42689_44787[(1)] = (13));

} else {
var statearr_42690_44788 = state_42579__$1;
(statearr_42690_44788[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (18))){
var inst_42493 = (state_42579[(2)]);
var state_42579__$1 = state_42579;
var statearr_42692_44789 = state_42579__$1;
(statearr_42692_44789[(2)] = inst_42493);

(statearr_42692_44789[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (42))){
var state_42579__$1 = state_42579;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42579__$1,(45),dchan);
} else {
if((state_val_42580 === (37))){
var inst_42533 = (state_42579[(23)]);
var inst_42542 = (state_42579[(22)]);
var inst_42431 = (state_42579[(11)]);
var inst_42542__$1 = cljs.core.first(inst_42533);
var inst_42543 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_42542__$1,inst_42431,done);
var state_42579__$1 = (function (){var statearr_42693 = state_42579;
(statearr_42693[(22)] = inst_42542__$1);

return statearr_42693;
})();
if(cljs.core.truth_(inst_42543)){
var statearr_42694_44792 = state_42579__$1;
(statearr_42694_44792[(1)] = (39));

} else {
var statearr_42695_44793 = state_42579__$1;
(statearr_42695_44793[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42580 === (8))){
var inst_42443 = (state_42579[(15)]);
var inst_42444 = (state_42579[(16)]);
var inst_42446 = (inst_42444 < inst_42443);
var inst_42447 = inst_42446;
var state_42579__$1 = state_42579;
if(cljs.core.truth_(inst_42447)){
var statearr_42696_44794 = state_42579__$1;
(statearr_42696_44794[(1)] = (10));

} else {
var statearr_42697_44798 = state_42579__$1;
(statearr_42697_44798[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__41223__auto__ = null;
var cljs$core$async$mult_$_state_machine__41223__auto____0 = (function (){
var statearr_42699 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42699[(0)] = cljs$core$async$mult_$_state_machine__41223__auto__);

(statearr_42699[(1)] = (1));

return statearr_42699;
});
var cljs$core$async$mult_$_state_machine__41223__auto____1 = (function (state_42579){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42579);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42702){var ex__41226__auto__ = e42702;
var statearr_42703_44799 = state_42579;
(statearr_42703_44799[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42579[(4)]))){
var statearr_42704_44800 = state_42579;
(statearr_42704_44800[(1)] = cljs.core.first((state_42579[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44801 = state_42579;
state_42579 = G__44801;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__41223__auto__ = function(state_42579){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__41223__auto____1.call(this,state_42579);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__41223__auto____0;
cljs$core$async$mult_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__41223__auto____1;
return cljs$core$async$mult_$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42705 = f__41556__auto__();
(statearr_42705[(6)] = c__41555__auto___44686);

return statearr_42705;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__42709 = arguments.length;
switch (G__42709) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_44803 = (function (m,ch){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4551__auto__.call(null,m,ch));
} else {
var m__4549__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4549__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_44803(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_44810 = (function (m,ch){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4551__auto__.call(null,m,ch));
} else {
var m__4549__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4549__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_44810(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_44811 = (function (m){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4551__auto__.call(null,m));
} else {
var m__4549__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4549__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_44811(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_44812 = (function (m,state_map){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4551__auto__.call(null,m,state_map));
} else {
var m__4549__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4549__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_44812(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_44813 = (function (m,mode){
var x__4550__auto__ = (((m == null))?null:m);
var m__4551__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4551__auto__.call(null,m,mode));
} else {
var m__4549__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4549__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_44813(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4870__auto__ = [];
var len__4864__auto___44818 = arguments.length;
var i__4865__auto___44819 = (0);
while(true){
if((i__4865__auto___44819 < len__4864__auto___44818)){
args__4870__auto__.push((arguments[i__4865__auto___44819]));

var G__44820 = (i__4865__auto___44819 + (1));
i__4865__auto___44819 = G__44820;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((3) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4871__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__42739){
var map__42740 = p__42739;
var map__42740__$1 = cljs.core.__destructure_map(map__42740);
var opts = map__42740__$1;
var statearr_42741_44827 = state;
(statearr_42741_44827[(1)] = cont_block);


var temp__5804__auto__ = cljs.core.async.do_alts((function (val){
var statearr_42742_44828 = state;
(statearr_42742_44828[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5804__auto__)){
var cb = temp__5804__auto__;
var statearr_42744_44829 = state;
(statearr_42744_44829[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq42731){
var G__42732 = cljs.core.first(seq42731);
var seq42731__$1 = cljs.core.next(seq42731);
var G__42733 = cljs.core.first(seq42731__$1);
var seq42731__$2 = cljs.core.next(seq42731__$1);
var G__42734 = cljs.core.first(seq42731__$2);
var seq42731__$3 = cljs.core.next(seq42731__$2);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__42732,G__42733,G__42734,seq42731__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async42755 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42755 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta42756){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta42756 = meta42756;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_42757,meta42756__$1){
var self__ = this;
var _42757__$1 = this;
return (new cljs.core.async.t_cljs$core$async42755(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta42756__$1));
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_42757){
var self__ = this;
var _42757__$1 = this;
return self__.meta42756;
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async42755.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async42755.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta42756","meta42756",-1782107478,null)], null);
}));

(cljs.core.async.t_cljs$core$async42755.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async42755.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42755");

(cljs.core.async.t_cljs$core$async42755.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async42755");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async42755.
 */
cljs.core.async.__GT_t_cljs$core$async42755 = (function cljs$core$async$mix_$___GT_t_cljs$core$async42755(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta42756){
return (new cljs.core.async.t_cljs$core$async42755(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta42756));
});

}

return (new cljs.core.async.t_cljs$core$async42755(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__41555__auto___44843 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_42833){
var state_val_42834 = (state_42833[(1)]);
if((state_val_42834 === (7))){
var inst_42793 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
if(cljs.core.truth_(inst_42793)){
var statearr_42837_44847 = state_42833__$1;
(statearr_42837_44847[(1)] = (8));

} else {
var statearr_42838_44848 = state_42833__$1;
(statearr_42838_44848[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (20))){
var inst_42786 = (state_42833[(7)]);
var state_42833__$1 = state_42833;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_42833__$1,(23),out,inst_42786);
} else {
if((state_val_42834 === (1))){
var inst_42768 = calc_state();
var inst_42770 = cljs.core.__destructure_map(inst_42768);
var inst_42771 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_42770,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_42772 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_42770,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_42773 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_42770,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_42774 = inst_42768;
var state_42833__$1 = (function (){var statearr_42839 = state_42833;
(statearr_42839[(8)] = inst_42773);

(statearr_42839[(9)] = inst_42772);

(statearr_42839[(10)] = inst_42771);

(statearr_42839[(11)] = inst_42774);

return statearr_42839;
})();
var statearr_42846_44849 = state_42833__$1;
(statearr_42846_44849[(2)] = null);

(statearr_42846_44849[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (24))){
var inst_42777 = (state_42833[(12)]);
var inst_42774 = inst_42777;
var state_42833__$1 = (function (){var statearr_42851 = state_42833;
(statearr_42851[(11)] = inst_42774);

return statearr_42851;
})();
var statearr_42852_44850 = state_42833__$1;
(statearr_42852_44850[(2)] = null);

(statearr_42852_44850[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (4))){
var inst_42786 = (state_42833[(7)]);
var inst_42788 = (state_42833[(13)]);
var inst_42785 = (state_42833[(2)]);
var inst_42786__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_42785,(0),null);
var inst_42787 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_42785,(1),null);
var inst_42788__$1 = (inst_42786__$1 == null);
var state_42833__$1 = (function (){var statearr_42854 = state_42833;
(statearr_42854[(7)] = inst_42786__$1);

(statearr_42854[(14)] = inst_42787);

(statearr_42854[(13)] = inst_42788__$1);

return statearr_42854;
})();
if(cljs.core.truth_(inst_42788__$1)){
var statearr_42855_44851 = state_42833__$1;
(statearr_42855_44851[(1)] = (5));

} else {
var statearr_42856_44852 = state_42833__$1;
(statearr_42856_44852[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (15))){
var inst_42807 = (state_42833[(15)]);
var inst_42778 = (state_42833[(16)]);
var inst_42807__$1 = cljs.core.empty_QMARK_(inst_42778);
var state_42833__$1 = (function (){var statearr_42857 = state_42833;
(statearr_42857[(15)] = inst_42807__$1);

return statearr_42857;
})();
if(inst_42807__$1){
var statearr_42858_44853 = state_42833__$1;
(statearr_42858_44853[(1)] = (17));

} else {
var statearr_42859_44854 = state_42833__$1;
(statearr_42859_44854[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (21))){
var inst_42777 = (state_42833[(12)]);
var inst_42774 = inst_42777;
var state_42833__$1 = (function (){var statearr_42860 = state_42833;
(statearr_42860[(11)] = inst_42774);

return statearr_42860;
})();
var statearr_42864_44855 = state_42833__$1;
(statearr_42864_44855[(2)] = null);

(statearr_42864_44855[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (13))){
var inst_42800 = (state_42833[(2)]);
var inst_42801 = calc_state();
var inst_42774 = inst_42801;
var state_42833__$1 = (function (){var statearr_42865 = state_42833;
(statearr_42865[(11)] = inst_42774);

(statearr_42865[(17)] = inst_42800);

return statearr_42865;
})();
var statearr_42870_44858 = state_42833__$1;
(statearr_42870_44858[(2)] = null);

(statearr_42870_44858[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (22))){
var inst_42827 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
var statearr_42871_44859 = state_42833__$1;
(statearr_42871_44859[(2)] = inst_42827);

(statearr_42871_44859[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (6))){
var inst_42787 = (state_42833[(14)]);
var inst_42791 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_42787,change);
var state_42833__$1 = state_42833;
var statearr_42872_44860 = state_42833__$1;
(statearr_42872_44860[(2)] = inst_42791);

(statearr_42872_44860[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (25))){
var state_42833__$1 = state_42833;
var statearr_42873_44861 = state_42833__$1;
(statearr_42873_44861[(2)] = null);

(statearr_42873_44861[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (17))){
var inst_42787 = (state_42833[(14)]);
var inst_42779 = (state_42833[(18)]);
var inst_42809 = (inst_42779.cljs$core$IFn$_invoke$arity$1 ? inst_42779.cljs$core$IFn$_invoke$arity$1(inst_42787) : inst_42779.call(null,inst_42787));
var inst_42810 = cljs.core.not(inst_42809);
var state_42833__$1 = state_42833;
var statearr_42877_44862 = state_42833__$1;
(statearr_42877_44862[(2)] = inst_42810);

(statearr_42877_44862[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (3))){
var inst_42831 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
return cljs.core.async.impl.ioc_helpers.return_chan(state_42833__$1,inst_42831);
} else {
if((state_val_42834 === (12))){
var state_42833__$1 = state_42833;
var statearr_42879_44863 = state_42833__$1;
(statearr_42879_44863[(2)] = null);

(statearr_42879_44863[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (2))){
var inst_42777 = (state_42833[(12)]);
var inst_42774 = (state_42833[(11)]);
var inst_42777__$1 = cljs.core.__destructure_map(inst_42774);
var inst_42778 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_42777__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_42779 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_42777__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_42780 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_42777__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_42833__$1 = (function (){var statearr_42880 = state_42833;
(statearr_42880[(18)] = inst_42779);

(statearr_42880[(12)] = inst_42777__$1);

(statearr_42880[(16)] = inst_42778);

return statearr_42880;
})();
return cljs.core.async.ioc_alts_BANG_(state_42833__$1,(4),inst_42780);
} else {
if((state_val_42834 === (23))){
var inst_42818 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
if(cljs.core.truth_(inst_42818)){
var statearr_42881_44865 = state_42833__$1;
(statearr_42881_44865[(1)] = (24));

} else {
var statearr_42882_44866 = state_42833__$1;
(statearr_42882_44866[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (19))){
var inst_42813 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
var statearr_42883_44871 = state_42833__$1;
(statearr_42883_44871[(2)] = inst_42813);

(statearr_42883_44871[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (11))){
var inst_42787 = (state_42833[(14)]);
var inst_42797 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_42787);
var state_42833__$1 = state_42833;
var statearr_42884_44872 = state_42833__$1;
(statearr_42884_44872[(2)] = inst_42797);

(statearr_42884_44872[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (9))){
var inst_42787 = (state_42833[(14)]);
var inst_42804 = (state_42833[(19)]);
var inst_42778 = (state_42833[(16)]);
var inst_42804__$1 = (inst_42778.cljs$core$IFn$_invoke$arity$1 ? inst_42778.cljs$core$IFn$_invoke$arity$1(inst_42787) : inst_42778.call(null,inst_42787));
var state_42833__$1 = (function (){var statearr_42885 = state_42833;
(statearr_42885[(19)] = inst_42804__$1);

return statearr_42885;
})();
if(cljs.core.truth_(inst_42804__$1)){
var statearr_42886_44877 = state_42833__$1;
(statearr_42886_44877[(1)] = (14));

} else {
var statearr_42887_44880 = state_42833__$1;
(statearr_42887_44880[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (5))){
var inst_42788 = (state_42833[(13)]);
var state_42833__$1 = state_42833;
var statearr_42889_44881 = state_42833__$1;
(statearr_42889_44881[(2)] = inst_42788);

(statearr_42889_44881[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (14))){
var inst_42804 = (state_42833[(19)]);
var state_42833__$1 = state_42833;
var statearr_42890_44884 = state_42833__$1;
(statearr_42890_44884[(2)] = inst_42804);

(statearr_42890_44884[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (26))){
var inst_42823 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
var statearr_42891_44885 = state_42833__$1;
(statearr_42891_44885[(2)] = inst_42823);

(statearr_42891_44885[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (16))){
var inst_42815 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
if(cljs.core.truth_(inst_42815)){
var statearr_42892_44886 = state_42833__$1;
(statearr_42892_44886[(1)] = (20));

} else {
var statearr_42893_44891 = state_42833__$1;
(statearr_42893_44891[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (10))){
var inst_42829 = (state_42833[(2)]);
var state_42833__$1 = state_42833;
var statearr_42894_44892 = state_42833__$1;
(statearr_42894_44892[(2)] = inst_42829);

(statearr_42894_44892[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (18))){
var inst_42807 = (state_42833[(15)]);
var state_42833__$1 = state_42833;
var statearr_42895_44895 = state_42833__$1;
(statearr_42895_44895[(2)] = inst_42807);

(statearr_42895_44895[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42834 === (8))){
var inst_42786 = (state_42833[(7)]);
var inst_42795 = (inst_42786 == null);
var state_42833__$1 = state_42833;
if(cljs.core.truth_(inst_42795)){
var statearr_42896_44898 = state_42833__$1;
(statearr_42896_44898[(1)] = (11));

} else {
var statearr_42897_44899 = state_42833__$1;
(statearr_42897_44899[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__41223__auto__ = null;
var cljs$core$async$mix_$_state_machine__41223__auto____0 = (function (){
var statearr_42907 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42907[(0)] = cljs$core$async$mix_$_state_machine__41223__auto__);

(statearr_42907[(1)] = (1));

return statearr_42907;
});
var cljs$core$async$mix_$_state_machine__41223__auto____1 = (function (state_42833){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_42833);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e42908){var ex__41226__auto__ = e42908;
var statearr_42909_44903 = state_42833;
(statearr_42909_44903[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_42833[(4)]))){
var statearr_42910_44904 = state_42833;
(statearr_42910_44904[(1)] = cljs.core.first((state_42833[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44906 = state_42833;
state_42833 = G__44906;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__41223__auto__ = function(state_42833){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__41223__auto____1.call(this,state_42833);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__41223__auto____0;
cljs$core$async$mix_$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__41223__auto____1;
return cljs$core$async$mix_$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_42916 = f__41556__auto__();
(statearr_42916[(6)] = c__41555__auto___44843);

return statearr_42916;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_44912 = (function (p,v,ch,close_QMARK_){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4551__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4549__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4549__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_44912(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_44920 = (function (p,v,ch){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4551__auto__.call(null,p,v,ch));
} else {
var m__4549__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4549__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_44920(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_44924 = (function() {
var G__44925 = null;
var G__44925__1 = (function (p){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4551__auto__.call(null,p));
} else {
var m__4549__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4549__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__44925__2 = (function (p,v){
var x__4550__auto__ = (((p == null))?null:p);
var m__4551__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4551__auto__.call(null,p,v));
} else {
var m__4549__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4549__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__44925 = function(p,v){
switch(arguments.length){
case 1:
return G__44925__1.call(this,p);
case 2:
return G__44925__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__44925.cljs$core$IFn$_invoke$arity$1 = G__44925__1;
G__44925.cljs$core$IFn$_invoke$arity$2 = G__44925__2;
return G__44925;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__42920 = arguments.length;
switch (G__42920) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_44924(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_44924(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__42924 = arguments.length;
switch (G__42924) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4253__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__42921_SHARP_){
if(cljs.core.truth_((p1__42921_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__42921_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__42921_SHARP_.call(null,topic)))){
return p1__42921_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__42921_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async42934 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42934 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta42935){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta42935 = meta42935;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_42936,meta42935__$1){
var self__ = this;
var _42936__$1 = this;
return (new cljs.core.async.t_cljs$core$async42934(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta42935__$1));
}));

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_42936){
var self__ = this;
var _42936__$1 = this;
return self__.meta42935;
}));

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5804__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5804__auto__)){
var m = temp__5804__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async42934.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async42934.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta42935","meta42935",1008613526,null)], null);
}));

(cljs.core.async.t_cljs$core$async42934.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async42934.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42934");

(cljs.core.async.t_cljs$core$async42934.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async42934");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async42934.
 */
cljs.core.async.__GT_t_cljs$core$async42934 = (function cljs$core$async$__GT_t_cljs$core$async42934(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta42935){
return (new cljs.core.async.t_cljs$core$async42934(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta42935));
});

}

return (new cljs.core.async.t_cljs$core$async42934(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__41555__auto___44944 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43027){
var state_val_43028 = (state_43027[(1)]);
if((state_val_43028 === (7))){
var inst_43023 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
var statearr_43030_44949 = state_43027__$1;
(statearr_43030_44949[(2)] = inst_43023);

(statearr_43030_44949[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (20))){
var state_43027__$1 = state_43027;
var statearr_43031_44950 = state_43027__$1;
(statearr_43031_44950[(2)] = null);

(statearr_43031_44950[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (1))){
var state_43027__$1 = state_43027;
var statearr_43036_44952 = state_43027__$1;
(statearr_43036_44952[(2)] = null);

(statearr_43036_44952[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (24))){
var inst_43006 = (state_43027[(7)]);
var inst_43015 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_43006);
var state_43027__$1 = state_43027;
var statearr_43042_44956 = state_43027__$1;
(statearr_43042_44956[(2)] = inst_43015);

(statearr_43042_44956[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (4))){
var inst_42949 = (state_43027[(8)]);
var inst_42949__$1 = (state_43027[(2)]);
var inst_42954 = (inst_42949__$1 == null);
var state_43027__$1 = (function (){var statearr_43043 = state_43027;
(statearr_43043[(8)] = inst_42949__$1);

return statearr_43043;
})();
if(cljs.core.truth_(inst_42954)){
var statearr_43045_44958 = state_43027__$1;
(statearr_43045_44958[(1)] = (5));

} else {
var statearr_43046_44960 = state_43027__$1;
(statearr_43046_44960[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (15))){
var inst_42996 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
var statearr_43047_44966 = state_43027__$1;
(statearr_43047_44966[(2)] = inst_42996);

(statearr_43047_44966[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (21))){
var inst_43020 = (state_43027[(2)]);
var state_43027__$1 = (function (){var statearr_43048 = state_43027;
(statearr_43048[(9)] = inst_43020);

return statearr_43048;
})();
var statearr_43049_44971 = state_43027__$1;
(statearr_43049_44971[(2)] = null);

(statearr_43049_44971[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (13))){
var inst_42978 = (state_43027[(10)]);
var inst_42980 = cljs.core.chunked_seq_QMARK_(inst_42978);
var state_43027__$1 = state_43027;
if(inst_42980){
var statearr_43054_44972 = state_43027__$1;
(statearr_43054_44972[(1)] = (16));

} else {
var statearr_43055_44977 = state_43027__$1;
(statearr_43055_44977[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (22))){
var inst_43012 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
if(cljs.core.truth_(inst_43012)){
var statearr_43061_44979 = state_43027__$1;
(statearr_43061_44979[(1)] = (23));

} else {
var statearr_43062_44983 = state_43027__$1;
(statearr_43062_44983[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (6))){
var inst_42949 = (state_43027[(8)]);
var inst_43008 = (state_43027[(11)]);
var inst_43006 = (state_43027[(7)]);
var inst_43006__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_42949) : topic_fn.call(null,inst_42949));
var inst_43007 = cljs.core.deref(mults);
var inst_43008__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_43007,inst_43006__$1);
var state_43027__$1 = (function (){var statearr_43064 = state_43027;
(statearr_43064[(11)] = inst_43008__$1);

(statearr_43064[(7)] = inst_43006__$1);

return statearr_43064;
})();
if(cljs.core.truth_(inst_43008__$1)){
var statearr_43065_44987 = state_43027__$1;
(statearr_43065_44987[(1)] = (19));

} else {
var statearr_43066_44989 = state_43027__$1;
(statearr_43066_44989[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (25))){
var inst_43017 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
var statearr_43067_44990 = state_43027__$1;
(statearr_43067_44990[(2)] = inst_43017);

(statearr_43067_44990[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (17))){
var inst_42978 = (state_43027[(10)]);
var inst_42987 = cljs.core.first(inst_42978);
var inst_42988 = cljs.core.async.muxch_STAR_(inst_42987);
var inst_42989 = cljs.core.async.close_BANG_(inst_42988);
var inst_42990 = cljs.core.next(inst_42978);
var inst_42963 = inst_42990;
var inst_42964 = null;
var inst_42965 = (0);
var inst_42966 = (0);
var state_43027__$1 = (function (){var statearr_43070 = state_43027;
(statearr_43070[(12)] = inst_42965);

(statearr_43070[(13)] = inst_42964);

(statearr_43070[(14)] = inst_42966);

(statearr_43070[(15)] = inst_42989);

(statearr_43070[(16)] = inst_42963);

return statearr_43070;
})();
var statearr_43071_44994 = state_43027__$1;
(statearr_43071_44994[(2)] = null);

(statearr_43071_44994[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (3))){
var inst_43025 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
return cljs.core.async.impl.ioc_helpers.return_chan(state_43027__$1,inst_43025);
} else {
if((state_val_43028 === (12))){
var inst_42998 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
var statearr_43074_44998 = state_43027__$1;
(statearr_43074_44998[(2)] = inst_42998);

(statearr_43074_44998[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (2))){
var state_43027__$1 = state_43027;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_43027__$1,(4),ch);
} else {
if((state_val_43028 === (23))){
var state_43027__$1 = state_43027;
var statearr_43076_45003 = state_43027__$1;
(statearr_43076_45003[(2)] = null);

(statearr_43076_45003[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (19))){
var inst_42949 = (state_43027[(8)]);
var inst_43008 = (state_43027[(11)]);
var inst_43010 = cljs.core.async.muxch_STAR_(inst_43008);
var state_43027__$1 = state_43027;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43027__$1,(22),inst_43010,inst_42949);
} else {
if((state_val_43028 === (11))){
var inst_42978 = (state_43027[(10)]);
var inst_42963 = (state_43027[(16)]);
var inst_42978__$1 = cljs.core.seq(inst_42963);
var state_43027__$1 = (function (){var statearr_43077 = state_43027;
(statearr_43077[(10)] = inst_42978__$1);

return statearr_43077;
})();
if(inst_42978__$1){
var statearr_43078_45007 = state_43027__$1;
(statearr_43078_45007[(1)] = (13));

} else {
var statearr_43079_45008 = state_43027__$1;
(statearr_43079_45008[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (9))){
var inst_43000 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
var statearr_43084_45010 = state_43027__$1;
(statearr_43084_45010[(2)] = inst_43000);

(statearr_43084_45010[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (5))){
var inst_42960 = cljs.core.deref(mults);
var inst_42961 = cljs.core.vals(inst_42960);
var inst_42962 = cljs.core.seq(inst_42961);
var inst_42963 = inst_42962;
var inst_42964 = null;
var inst_42965 = (0);
var inst_42966 = (0);
var state_43027__$1 = (function (){var statearr_43086 = state_43027;
(statearr_43086[(12)] = inst_42965);

(statearr_43086[(13)] = inst_42964);

(statearr_43086[(14)] = inst_42966);

(statearr_43086[(16)] = inst_42963);

return statearr_43086;
})();
var statearr_43088_45014 = state_43027__$1;
(statearr_43088_45014[(2)] = null);

(statearr_43088_45014[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (14))){
var state_43027__$1 = state_43027;
var statearr_43092_45015 = state_43027__$1;
(statearr_43092_45015[(2)] = null);

(statearr_43092_45015[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (16))){
var inst_42978 = (state_43027[(10)]);
var inst_42982 = cljs.core.chunk_first(inst_42978);
var inst_42983 = cljs.core.chunk_rest(inst_42978);
var inst_42984 = cljs.core.count(inst_42982);
var inst_42963 = inst_42983;
var inst_42964 = inst_42982;
var inst_42965 = inst_42984;
var inst_42966 = (0);
var state_43027__$1 = (function (){var statearr_43093 = state_43027;
(statearr_43093[(12)] = inst_42965);

(statearr_43093[(13)] = inst_42964);

(statearr_43093[(14)] = inst_42966);

(statearr_43093[(16)] = inst_42963);

return statearr_43093;
})();
var statearr_43098_45016 = state_43027__$1;
(statearr_43098_45016[(2)] = null);

(statearr_43098_45016[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (10))){
var inst_42965 = (state_43027[(12)]);
var inst_42964 = (state_43027[(13)]);
var inst_42966 = (state_43027[(14)]);
var inst_42963 = (state_43027[(16)]);
var inst_42972 = cljs.core._nth(inst_42964,inst_42966);
var inst_42973 = cljs.core.async.muxch_STAR_(inst_42972);
var inst_42974 = cljs.core.async.close_BANG_(inst_42973);
var inst_42975 = (inst_42966 + (1));
var tmp43089 = inst_42965;
var tmp43090 = inst_42964;
var tmp43091 = inst_42963;
var inst_42963__$1 = tmp43091;
var inst_42964__$1 = tmp43090;
var inst_42965__$1 = tmp43089;
var inst_42966__$1 = inst_42975;
var state_43027__$1 = (function (){var statearr_43099 = state_43027;
(statearr_43099[(12)] = inst_42965__$1);

(statearr_43099[(13)] = inst_42964__$1);

(statearr_43099[(17)] = inst_42974);

(statearr_43099[(14)] = inst_42966__$1);

(statearr_43099[(16)] = inst_42963__$1);

return statearr_43099;
})();
var statearr_43100_45024 = state_43027__$1;
(statearr_43100_45024[(2)] = null);

(statearr_43100_45024[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (18))){
var inst_42993 = (state_43027[(2)]);
var state_43027__$1 = state_43027;
var statearr_43101_45025 = state_43027__$1;
(statearr_43101_45025[(2)] = inst_42993);

(statearr_43101_45025[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43028 === (8))){
var inst_42965 = (state_43027[(12)]);
var inst_42966 = (state_43027[(14)]);
var inst_42969 = (inst_42966 < inst_42965);
var inst_42970 = inst_42969;
var state_43027__$1 = state_43027;
if(cljs.core.truth_(inst_42970)){
var statearr_43104_45029 = state_43027__$1;
(statearr_43104_45029[(1)] = (10));

} else {
var statearr_43105_45030 = state_43027__$1;
(statearr_43105_45030[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_43109 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43109[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_43109[(1)] = (1));

return statearr_43109;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_43027){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43027);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e43110){var ex__41226__auto__ = e43110;
var statearr_43111_45042 = state_43027;
(statearr_43111_45042[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43027[(4)]))){
var statearr_43112_45044 = state_43027;
(statearr_43112_45044[(1)] = cljs.core.first((state_43027[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45055 = state_43027;
state_43027 = G__45055;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_43027){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_43027);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_43113 = f__41556__auto__();
(statearr_43113[(6)] = c__41555__auto___44944);

return statearr_43113;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__43115 = arguments.length;
switch (G__43115) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__43119 = arguments.length;
switch (G__43119) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__43123 = arguments.length;
switch (G__43123) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
if((cnt === (0))){
cljs.core.async.close_BANG_(out);
} else {
var c__41555__auto___45113 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43172){
var state_val_43173 = (state_43172[(1)]);
if((state_val_43173 === (7))){
var state_43172__$1 = state_43172;
var statearr_43178_45118 = state_43172__$1;
(statearr_43178_45118[(2)] = null);

(statearr_43178_45118[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (1))){
var state_43172__$1 = state_43172;
var statearr_43180_45119 = state_43172__$1;
(statearr_43180_45119[(2)] = null);

(statearr_43180_45119[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (4))){
var inst_43131 = (state_43172[(7)]);
var inst_43132 = (state_43172[(8)]);
var inst_43134 = (inst_43132 < inst_43131);
var state_43172__$1 = state_43172;
if(cljs.core.truth_(inst_43134)){
var statearr_43181_45127 = state_43172__$1;
(statearr_43181_45127[(1)] = (6));

} else {
var statearr_43182_45129 = state_43172__$1;
(statearr_43182_45129[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (15))){
var inst_43158 = (state_43172[(9)]);
var inst_43163 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_43158);
var state_43172__$1 = state_43172;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43172__$1,(17),out,inst_43163);
} else {
if((state_val_43173 === (13))){
var inst_43158 = (state_43172[(9)]);
var inst_43158__$1 = (state_43172[(2)]);
var inst_43159 = cljs.core.some(cljs.core.nil_QMARK_,inst_43158__$1);
var state_43172__$1 = (function (){var statearr_43189 = state_43172;
(statearr_43189[(9)] = inst_43158__$1);

return statearr_43189;
})();
if(cljs.core.truth_(inst_43159)){
var statearr_43190_45134 = state_43172__$1;
(statearr_43190_45134[(1)] = (14));

} else {
var statearr_43191_45135 = state_43172__$1;
(statearr_43191_45135[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (6))){
var state_43172__$1 = state_43172;
var statearr_43192_45137 = state_43172__$1;
(statearr_43192_45137[(2)] = null);

(statearr_43192_45137[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (17))){
var inst_43165 = (state_43172[(2)]);
var state_43172__$1 = (function (){var statearr_43203 = state_43172;
(statearr_43203[(10)] = inst_43165);

return statearr_43203;
})();
var statearr_43207_45140 = state_43172__$1;
(statearr_43207_45140[(2)] = null);

(statearr_43207_45140[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (3))){
var inst_43170 = (state_43172[(2)]);
var state_43172__$1 = state_43172;
return cljs.core.async.impl.ioc_helpers.return_chan(state_43172__$1,inst_43170);
} else {
if((state_val_43173 === (12))){
var _ = (function (){var statearr_43208 = state_43172;
(statearr_43208[(4)] = cljs.core.rest((state_43172[(4)])));

return statearr_43208;
})();
var state_43172__$1 = state_43172;
var ex43201 = (state_43172__$1[(2)]);
var statearr_43211_45148 = state_43172__$1;
(statearr_43211_45148[(5)] = ex43201);


if((ex43201 instanceof Object)){
var statearr_43216_45149 = state_43172__$1;
(statearr_43216_45149[(1)] = (11));

(statearr_43216_45149[(5)] = null);

} else {
throw ex43201;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (2))){
var inst_43129 = cljs.core.reset_BANG_(dctr,cnt);
var inst_43131 = cnt;
var inst_43132 = (0);
var state_43172__$1 = (function (){var statearr_43217 = state_43172;
(statearr_43217[(7)] = inst_43131);

(statearr_43217[(8)] = inst_43132);

(statearr_43217[(11)] = inst_43129);

return statearr_43217;
})();
var statearr_43220_45157 = state_43172__$1;
(statearr_43220_45157[(2)] = null);

(statearr_43220_45157[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (11))){
var inst_43137 = (state_43172[(2)]);
var inst_43138 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_43172__$1 = (function (){var statearr_43221 = state_43172;
(statearr_43221[(12)] = inst_43137);

return statearr_43221;
})();
var statearr_43222_45159 = state_43172__$1;
(statearr_43222_45159[(2)] = inst_43138);

(statearr_43222_45159[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (9))){
var inst_43132 = (state_43172[(8)]);
var _ = (function (){var statearr_43223 = state_43172;
(statearr_43223[(4)] = cljs.core.cons((12),(state_43172[(4)])));

return statearr_43223;
})();
var inst_43144 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_43132) : chs__$1.call(null,inst_43132));
var inst_43145 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_43132) : done.call(null,inst_43132));
var inst_43146 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_43144,inst_43145);
var ___$1 = (function (){var statearr_43224 = state_43172;
(statearr_43224[(4)] = cljs.core.rest((state_43172[(4)])));

return statearr_43224;
})();
var state_43172__$1 = state_43172;
var statearr_43225_45162 = state_43172__$1;
(statearr_43225_45162[(2)] = inst_43146);

(statearr_43225_45162[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (5))){
var inst_43156 = (state_43172[(2)]);
var state_43172__$1 = (function (){var statearr_43226 = state_43172;
(statearr_43226[(13)] = inst_43156);

return statearr_43226;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_43172__$1,(13),dchan);
} else {
if((state_val_43173 === (14))){
var inst_43161 = cljs.core.async.close_BANG_(out);
var state_43172__$1 = state_43172;
var statearr_43227_45167 = state_43172__$1;
(statearr_43227_45167[(2)] = inst_43161);

(statearr_43227_45167[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (16))){
var inst_43168 = (state_43172[(2)]);
var state_43172__$1 = state_43172;
var statearr_43228_45169 = state_43172__$1;
(statearr_43228_45169[(2)] = inst_43168);

(statearr_43228_45169[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (10))){
var inst_43132 = (state_43172[(8)]);
var inst_43149 = (state_43172[(2)]);
var inst_43150 = (inst_43132 + (1));
var inst_43132__$1 = inst_43150;
var state_43172__$1 = (function (){var statearr_43229 = state_43172;
(statearr_43229[(14)] = inst_43149);

(statearr_43229[(8)] = inst_43132__$1);

return statearr_43229;
})();
var statearr_43231_45172 = state_43172__$1;
(statearr_43231_45172[(2)] = null);

(statearr_43231_45172[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43173 === (8))){
var inst_43154 = (state_43172[(2)]);
var state_43172__$1 = state_43172;
var statearr_43233_45175 = state_43172__$1;
(statearr_43233_45175[(2)] = inst_43154);

(statearr_43233_45175[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_43234 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43234[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_43234[(1)] = (1));

return statearr_43234;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_43172){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43172);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e43235){var ex__41226__auto__ = e43235;
var statearr_43236_45183 = state_43172;
(statearr_43236_45183[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43172[(4)]))){
var statearr_43237_45184 = state_43172;
(statearr_43237_45184[(1)] = cljs.core.first((state_43172[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45189 = state_43172;
state_43172 = G__45189;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_43172){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_43172);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_43238 = f__41556__auto__();
(statearr_43238[(6)] = c__41555__auto___45113);

return statearr_43238;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

}

return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__43244 = arguments.length;
switch (G__43244) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__41555__auto___45197 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43289){
var state_val_43290 = (state_43289[(1)]);
if((state_val_43290 === (7))){
var inst_43263 = (state_43289[(7)]);
var inst_43264 = (state_43289[(8)]);
var inst_43263__$1 = (state_43289[(2)]);
var inst_43264__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_43263__$1,(0),null);
var inst_43265 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_43263__$1,(1),null);
var inst_43266 = (inst_43264__$1 == null);
var state_43289__$1 = (function (){var statearr_43295 = state_43289;
(statearr_43295[(9)] = inst_43265);

(statearr_43295[(7)] = inst_43263__$1);

(statearr_43295[(8)] = inst_43264__$1);

return statearr_43295;
})();
if(cljs.core.truth_(inst_43266)){
var statearr_43297_45200 = state_43289__$1;
(statearr_43297_45200[(1)] = (8));

} else {
var statearr_43300_45201 = state_43289__$1;
(statearr_43300_45201[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (1))){
var inst_43250 = cljs.core.vec(chs);
var inst_43251 = inst_43250;
var state_43289__$1 = (function (){var statearr_43303 = state_43289;
(statearr_43303[(10)] = inst_43251);

return statearr_43303;
})();
var statearr_43314_45210 = state_43289__$1;
(statearr_43314_45210[(2)] = null);

(statearr_43314_45210[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (4))){
var inst_43251 = (state_43289[(10)]);
var state_43289__$1 = state_43289;
return cljs.core.async.ioc_alts_BANG_(state_43289__$1,(7),inst_43251);
} else {
if((state_val_43290 === (6))){
var inst_43285 = (state_43289[(2)]);
var state_43289__$1 = state_43289;
var statearr_43322_45213 = state_43289__$1;
(statearr_43322_45213[(2)] = inst_43285);

(statearr_43322_45213[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (3))){
var inst_43287 = (state_43289[(2)]);
var state_43289__$1 = state_43289;
return cljs.core.async.impl.ioc_helpers.return_chan(state_43289__$1,inst_43287);
} else {
if((state_val_43290 === (2))){
var inst_43251 = (state_43289[(10)]);
var inst_43255 = cljs.core.count(inst_43251);
var inst_43256 = (inst_43255 > (0));
var state_43289__$1 = state_43289;
if(cljs.core.truth_(inst_43256)){
var statearr_43329_45221 = state_43289__$1;
(statearr_43329_45221[(1)] = (4));

} else {
var statearr_43330_45222 = state_43289__$1;
(statearr_43330_45222[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (11))){
var inst_43251 = (state_43289[(10)]);
var inst_43278 = (state_43289[(2)]);
var tmp43325 = inst_43251;
var inst_43251__$1 = tmp43325;
var state_43289__$1 = (function (){var statearr_43332 = state_43289;
(statearr_43332[(10)] = inst_43251__$1);

(statearr_43332[(11)] = inst_43278);

return statearr_43332;
})();
var statearr_43333_45223 = state_43289__$1;
(statearr_43333_45223[(2)] = null);

(statearr_43333_45223[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (9))){
var inst_43264 = (state_43289[(8)]);
var state_43289__$1 = state_43289;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43289__$1,(11),out,inst_43264);
} else {
if((state_val_43290 === (5))){
var inst_43283 = cljs.core.async.close_BANG_(out);
var state_43289__$1 = state_43289;
var statearr_43351_45226 = state_43289__$1;
(statearr_43351_45226[(2)] = inst_43283);

(statearr_43351_45226[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (10))){
var inst_43281 = (state_43289[(2)]);
var state_43289__$1 = state_43289;
var statearr_43354_45227 = state_43289__$1;
(statearr_43354_45227[(2)] = inst_43281);

(statearr_43354_45227[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43290 === (8))){
var inst_43265 = (state_43289[(9)]);
var inst_43251 = (state_43289[(10)]);
var inst_43263 = (state_43289[(7)]);
var inst_43264 = (state_43289[(8)]);
var inst_43272 = (function (){var cs = inst_43251;
var vec__43259 = inst_43263;
var v = inst_43264;
var c = inst_43265;
return (function (p1__43239_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__43239_SHARP_);
});
})();
var inst_43273 = cljs.core.filterv(inst_43272,inst_43251);
var inst_43251__$1 = inst_43273;
var state_43289__$1 = (function (){var statearr_43362 = state_43289;
(statearr_43362[(10)] = inst_43251__$1);

return statearr_43362;
})();
var statearr_43367_45232 = state_43289__$1;
(statearr_43367_45232[(2)] = null);

(statearr_43367_45232[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_43373 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43373[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_43373[(1)] = (1));

return statearr_43373;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_43289){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43289);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e43377){var ex__41226__auto__ = e43377;
var statearr_43378_45234 = state_43289;
(statearr_43378_45234[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43289[(4)]))){
var statearr_43382_45235 = state_43289;
(statearr_43382_45235[(1)] = cljs.core.first((state_43289[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45236 = state_43289;
state_43289 = G__45236;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_43289){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_43289);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_43385 = f__41556__auto__();
(statearr_43385[(6)] = c__41555__auto___45197);

return statearr_43385;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__43399 = arguments.length;
switch (G__43399) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__41555__auto___45242 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43448){
var state_val_43449 = (state_43448[(1)]);
if((state_val_43449 === (7))){
var inst_43421 = (state_43448[(7)]);
var inst_43421__$1 = (state_43448[(2)]);
var inst_43422 = (inst_43421__$1 == null);
var inst_43423 = cljs.core.not(inst_43422);
var state_43448__$1 = (function (){var statearr_43459 = state_43448;
(statearr_43459[(7)] = inst_43421__$1);

return statearr_43459;
})();
if(inst_43423){
var statearr_43461_45249 = state_43448__$1;
(statearr_43461_45249[(1)] = (8));

} else {
var statearr_43462_45250 = state_43448__$1;
(statearr_43462_45250[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (1))){
var inst_43414 = (0);
var state_43448__$1 = (function (){var statearr_43465 = state_43448;
(statearr_43465[(8)] = inst_43414);

return statearr_43465;
})();
var statearr_43467_45253 = state_43448__$1;
(statearr_43467_45253[(2)] = null);

(statearr_43467_45253[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (4))){
var state_43448__$1 = state_43448;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_43448__$1,(7),ch);
} else {
if((state_val_43449 === (6))){
var inst_43440 = (state_43448[(2)]);
var state_43448__$1 = state_43448;
var statearr_43471_45258 = state_43448__$1;
(statearr_43471_45258[(2)] = inst_43440);

(statearr_43471_45258[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (3))){
var inst_43442 = (state_43448[(2)]);
var inst_43444 = cljs.core.async.close_BANG_(out);
var state_43448__$1 = (function (){var statearr_43475 = state_43448;
(statearr_43475[(9)] = inst_43442);

return statearr_43475;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_43448__$1,inst_43444);
} else {
if((state_val_43449 === (2))){
var inst_43414 = (state_43448[(8)]);
var inst_43418 = (inst_43414 < n);
var state_43448__$1 = state_43448;
if(cljs.core.truth_(inst_43418)){
var statearr_43482_45259 = state_43448__$1;
(statearr_43482_45259[(1)] = (4));

} else {
var statearr_43483_45260 = state_43448__$1;
(statearr_43483_45260[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (11))){
var inst_43414 = (state_43448[(8)]);
var inst_43426 = (state_43448[(2)]);
var inst_43433 = (inst_43414 + (1));
var inst_43414__$1 = inst_43433;
var state_43448__$1 = (function (){var statearr_43485 = state_43448;
(statearr_43485[(10)] = inst_43426);

(statearr_43485[(8)] = inst_43414__$1);

return statearr_43485;
})();
var statearr_43486_45262 = state_43448__$1;
(statearr_43486_45262[(2)] = null);

(statearr_43486_45262[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (9))){
var state_43448__$1 = state_43448;
var statearr_43490_45263 = state_43448__$1;
(statearr_43490_45263[(2)] = null);

(statearr_43490_45263[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (5))){
var state_43448__$1 = state_43448;
var statearr_43499_45264 = state_43448__$1;
(statearr_43499_45264[(2)] = null);

(statearr_43499_45264[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (10))){
var inst_43437 = (state_43448[(2)]);
var state_43448__$1 = state_43448;
var statearr_43501_45265 = state_43448__$1;
(statearr_43501_45265[(2)] = inst_43437);

(statearr_43501_45265[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43449 === (8))){
var inst_43421 = (state_43448[(7)]);
var state_43448__$1 = state_43448;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43448__$1,(11),out,inst_43421);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_43507 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_43507[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_43507[(1)] = (1));

return statearr_43507;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_43448){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43448);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e43513){var ex__41226__auto__ = e43513;
var statearr_43516_45266 = state_43448;
(statearr_43516_45266[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43448[(4)]))){
var statearr_43519_45267 = state_43448;
(statearr_43519_45267[(1)] = cljs.core.first((state_43448[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45268 = state_43448;
state_43448 = G__45268;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_43448){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_43448);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_43522 = f__41556__auto__();
(statearr_43522[(6)] = c__41555__auto___45242);

return statearr_43522;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async43531 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43531 = (function (f,ch,meta43532){
this.f = f;
this.ch = ch;
this.meta43532 = meta43532;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43533,meta43532__$1){
var self__ = this;
var _43533__$1 = this;
return (new cljs.core.async.t_cljs$core$async43531(self__.f,self__.ch,meta43532__$1));
}));

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43533){
var self__ = this;
var _43533__$1 = this;
return self__.meta43532;
}));

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async43568 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43568 = (function (f,ch,meta43532,_,fn1,meta43569){
this.f = f;
this.ch = ch;
this.meta43532 = meta43532;
this._ = _;
this.fn1 = fn1;
this.meta43569 = meta43569;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async43568.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43570,meta43569__$1){
var self__ = this;
var _43570__$1 = this;
return (new cljs.core.async.t_cljs$core$async43568(self__.f,self__.ch,self__.meta43532,self__._,self__.fn1,meta43569__$1));
}));

(cljs.core.async.t_cljs$core$async43568.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43570){
var self__ = this;
var _43570__$1 = this;
return self__.meta43569;
}));

(cljs.core.async.t_cljs$core$async43568.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43568.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async43568.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async43568.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__43527_SHARP_){
var G__43591 = (((p1__43527_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__43527_SHARP_) : self__.f.call(null,p1__43527_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__43591) : f1.call(null,G__43591));
});
}));

(cljs.core.async.t_cljs$core$async43568.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43532","meta43532",1979555739,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async43531","cljs.core.async/t_cljs$core$async43531",645807061,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta43569","meta43569",-1850802285,null)], null);
}));

(cljs.core.async.t_cljs$core$async43568.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async43568.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43568");

(cljs.core.async.t_cljs$core$async43568.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async43568");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async43568.
 */
cljs.core.async.__GT_t_cljs$core$async43568 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async43568(f__$1,ch__$1,meta43532__$1,___$2,fn1__$1,meta43569){
return (new cljs.core.async.t_cljs$core$async43568(f__$1,ch__$1,meta43532__$1,___$2,fn1__$1,meta43569));
});

}

return (new cljs.core.async.t_cljs$core$async43568(self__.f,self__.ch,self__.meta43532,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4251__auto__ = ret;
if(cljs.core.truth_(and__4251__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4251__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__43610 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__43610) : self__.f.call(null,G__43610));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43531.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async43531.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43532","meta43532",1979555739,null)], null);
}));

(cljs.core.async.t_cljs$core$async43531.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async43531.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43531");

(cljs.core.async.t_cljs$core$async43531.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async43531");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async43531.
 */
cljs.core.async.__GT_t_cljs$core$async43531 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async43531(f__$1,ch__$1,meta43532){
return (new cljs.core.async.t_cljs$core$async43531(f__$1,ch__$1,meta43532));
});

}

return (new cljs.core.async.t_cljs$core$async43531(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async43623 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43623 = (function (f,ch,meta43624){
this.f = f;
this.ch = ch;
this.meta43624 = meta43624;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43625,meta43624__$1){
var self__ = this;
var _43625__$1 = this;
return (new cljs.core.async.t_cljs$core$async43623(self__.f,self__.ch,meta43624__$1));
}));

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43625){
var self__ = this;
var _43625__$1 = this;
return self__.meta43624;
}));

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43623.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async43623.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43624","meta43624",-937848168,null)], null);
}));

(cljs.core.async.t_cljs$core$async43623.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async43623.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43623");

(cljs.core.async.t_cljs$core$async43623.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async43623");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async43623.
 */
cljs.core.async.__GT_t_cljs$core$async43623 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async43623(f__$1,ch__$1,meta43624){
return (new cljs.core.async.t_cljs$core$async43623(f__$1,ch__$1,meta43624));
});

}

return (new cljs.core.async.t_cljs$core$async43623(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async43681 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43681 = (function (p,ch,meta43682){
this.p = p;
this.ch = ch;
this.meta43682 = meta43682;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43683,meta43682__$1){
var self__ = this;
var _43683__$1 = this;
return (new cljs.core.async.t_cljs$core$async43681(self__.p,self__.ch,meta43682__$1));
}));

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43683){
var self__ = this;
var _43683__$1 = this;
return self__.meta43682;
}));

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async43681.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async43681.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43682","meta43682",439790702,null)], null);
}));

(cljs.core.async.t_cljs$core$async43681.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async43681.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43681");

(cljs.core.async.t_cljs$core$async43681.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"cljs.core.async/t_cljs$core$async43681");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async43681.
 */
cljs.core.async.__GT_t_cljs$core$async43681 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async43681(p__$1,ch__$1,meta43682){
return (new cljs.core.async.t_cljs$core$async43681(p__$1,ch__$1,meta43682));
});

}

return (new cljs.core.async.t_cljs$core$async43681(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__43718 = arguments.length;
switch (G__43718) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__41555__auto___45299 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43746){
var state_val_43747 = (state_43746[(1)]);
if((state_val_43747 === (7))){
var inst_43741 = (state_43746[(2)]);
var state_43746__$1 = state_43746;
var statearr_43752_45300 = state_43746__$1;
(statearr_43752_45300[(2)] = inst_43741);

(statearr_43752_45300[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (1))){
var state_43746__$1 = state_43746;
var statearr_43753_45301 = state_43746__$1;
(statearr_43753_45301[(2)] = null);

(statearr_43753_45301[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (4))){
var inst_43726 = (state_43746[(7)]);
var inst_43726__$1 = (state_43746[(2)]);
var inst_43727 = (inst_43726__$1 == null);
var state_43746__$1 = (function (){var statearr_43755 = state_43746;
(statearr_43755[(7)] = inst_43726__$1);

return statearr_43755;
})();
if(cljs.core.truth_(inst_43727)){
var statearr_43756_45302 = state_43746__$1;
(statearr_43756_45302[(1)] = (5));

} else {
var statearr_43757_45303 = state_43746__$1;
(statearr_43757_45303[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (6))){
var inst_43726 = (state_43746[(7)]);
var inst_43731 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_43726) : p.call(null,inst_43726));
var state_43746__$1 = state_43746;
if(cljs.core.truth_(inst_43731)){
var statearr_43758_45304 = state_43746__$1;
(statearr_43758_45304[(1)] = (8));

} else {
var statearr_43759_45305 = state_43746__$1;
(statearr_43759_45305[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (3))){
var inst_43744 = (state_43746[(2)]);
var state_43746__$1 = state_43746;
return cljs.core.async.impl.ioc_helpers.return_chan(state_43746__$1,inst_43744);
} else {
if((state_val_43747 === (2))){
var state_43746__$1 = state_43746;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_43746__$1,(4),ch);
} else {
if((state_val_43747 === (11))){
var inst_43734 = (state_43746[(2)]);
var state_43746__$1 = state_43746;
var statearr_43760_45306 = state_43746__$1;
(statearr_43760_45306[(2)] = inst_43734);

(statearr_43760_45306[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (9))){
var state_43746__$1 = state_43746;
var statearr_43761_45307 = state_43746__$1;
(statearr_43761_45307[(2)] = null);

(statearr_43761_45307[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (5))){
var inst_43729 = cljs.core.async.close_BANG_(out);
var state_43746__$1 = state_43746;
var statearr_43762_45308 = state_43746__$1;
(statearr_43762_45308[(2)] = inst_43729);

(statearr_43762_45308[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (10))){
var inst_43738 = (state_43746[(2)]);
var state_43746__$1 = (function (){var statearr_43763 = state_43746;
(statearr_43763[(8)] = inst_43738);

return statearr_43763;
})();
var statearr_43764_45309 = state_43746__$1;
(statearr_43764_45309[(2)] = null);

(statearr_43764_45309[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43747 === (8))){
var inst_43726 = (state_43746[(7)]);
var state_43746__$1 = state_43746;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43746__$1,(11),out,inst_43726);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_43766 = [null,null,null,null,null,null,null,null,null];
(statearr_43766[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_43766[(1)] = (1));

return statearr_43766;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_43746){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43746);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e43767){var ex__41226__auto__ = e43767;
var statearr_43768_45311 = state_43746;
(statearr_43768_45311[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43746[(4)]))){
var statearr_43769_45312 = state_43746;
(statearr_43769_45312[(1)] = cljs.core.first((state_43746[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45313 = state_43746;
state_43746 = G__45313;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_43746){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_43746);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_43771 = f__41556__auto__();
(statearr_43771[(6)] = c__41555__auto___45299);

return statearr_43771;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__43775 = arguments.length;
switch (G__43775) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__41555__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43844){
var state_val_43845 = (state_43844[(1)]);
if((state_val_43845 === (7))){
var inst_43836 = (state_43844[(2)]);
var state_43844__$1 = state_43844;
var statearr_43854_45317 = state_43844__$1;
(statearr_43854_45317[(2)] = inst_43836);

(statearr_43854_45317[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (20))){
var inst_43806 = (state_43844[(7)]);
var inst_43817 = (state_43844[(2)]);
var inst_43818 = cljs.core.next(inst_43806);
var inst_43791 = inst_43818;
var inst_43792 = null;
var inst_43793 = (0);
var inst_43794 = (0);
var state_43844__$1 = (function (){var statearr_43855 = state_43844;
(statearr_43855[(8)] = inst_43792);

(statearr_43855[(9)] = inst_43817);

(statearr_43855[(10)] = inst_43794);

(statearr_43855[(11)] = inst_43793);

(statearr_43855[(12)] = inst_43791);

return statearr_43855;
})();
var statearr_43857_45318 = state_43844__$1;
(statearr_43857_45318[(2)] = null);

(statearr_43857_45318[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (1))){
var state_43844__$1 = state_43844;
var statearr_43858_45319 = state_43844__$1;
(statearr_43858_45319[(2)] = null);

(statearr_43858_45319[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (4))){
var inst_43779 = (state_43844[(13)]);
var inst_43779__$1 = (state_43844[(2)]);
var inst_43781 = (inst_43779__$1 == null);
var state_43844__$1 = (function (){var statearr_43859 = state_43844;
(statearr_43859[(13)] = inst_43779__$1);

return statearr_43859;
})();
if(cljs.core.truth_(inst_43781)){
var statearr_43860_45322 = state_43844__$1;
(statearr_43860_45322[(1)] = (5));

} else {
var statearr_43861_45323 = state_43844__$1;
(statearr_43861_45323[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (15))){
var state_43844__$1 = state_43844;
var statearr_43865_45324 = state_43844__$1;
(statearr_43865_45324[(2)] = null);

(statearr_43865_45324[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (21))){
var state_43844__$1 = state_43844;
var statearr_43866_45325 = state_43844__$1;
(statearr_43866_45325[(2)] = null);

(statearr_43866_45325[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (13))){
var inst_43792 = (state_43844[(8)]);
var inst_43794 = (state_43844[(10)]);
var inst_43793 = (state_43844[(11)]);
var inst_43791 = (state_43844[(12)]);
var inst_43801 = (state_43844[(2)]);
var inst_43802 = (inst_43794 + (1));
var tmp43862 = inst_43792;
var tmp43863 = inst_43793;
var tmp43864 = inst_43791;
var inst_43791__$1 = tmp43864;
var inst_43792__$1 = tmp43862;
var inst_43793__$1 = tmp43863;
var inst_43794__$1 = inst_43802;
var state_43844__$1 = (function (){var statearr_43867 = state_43844;
(statearr_43867[(14)] = inst_43801);

(statearr_43867[(8)] = inst_43792__$1);

(statearr_43867[(10)] = inst_43794__$1);

(statearr_43867[(11)] = inst_43793__$1);

(statearr_43867[(12)] = inst_43791__$1);

return statearr_43867;
})();
var statearr_43868_45333 = state_43844__$1;
(statearr_43868_45333[(2)] = null);

(statearr_43868_45333[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (22))){
var state_43844__$1 = state_43844;
var statearr_43870_45334 = state_43844__$1;
(statearr_43870_45334[(2)] = null);

(statearr_43870_45334[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (6))){
var inst_43779 = (state_43844[(13)]);
var inst_43789 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_43779) : f.call(null,inst_43779));
var inst_43790 = cljs.core.seq(inst_43789);
var inst_43791 = inst_43790;
var inst_43792 = null;
var inst_43793 = (0);
var inst_43794 = (0);
var state_43844__$1 = (function (){var statearr_43871 = state_43844;
(statearr_43871[(8)] = inst_43792);

(statearr_43871[(10)] = inst_43794);

(statearr_43871[(11)] = inst_43793);

(statearr_43871[(12)] = inst_43791);

return statearr_43871;
})();
var statearr_43878_45335 = state_43844__$1;
(statearr_43878_45335[(2)] = null);

(statearr_43878_45335[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (17))){
var inst_43806 = (state_43844[(7)]);
var inst_43810 = cljs.core.chunk_first(inst_43806);
var inst_43811 = cljs.core.chunk_rest(inst_43806);
var inst_43812 = cljs.core.count(inst_43810);
var inst_43791 = inst_43811;
var inst_43792 = inst_43810;
var inst_43793 = inst_43812;
var inst_43794 = (0);
var state_43844__$1 = (function (){var statearr_43879 = state_43844;
(statearr_43879[(8)] = inst_43792);

(statearr_43879[(10)] = inst_43794);

(statearr_43879[(11)] = inst_43793);

(statearr_43879[(12)] = inst_43791);

return statearr_43879;
})();
var statearr_43880_45338 = state_43844__$1;
(statearr_43880_45338[(2)] = null);

(statearr_43880_45338[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (3))){
var inst_43838 = (state_43844[(2)]);
var state_43844__$1 = state_43844;
return cljs.core.async.impl.ioc_helpers.return_chan(state_43844__$1,inst_43838);
} else {
if((state_val_43845 === (12))){
var inst_43826 = (state_43844[(2)]);
var state_43844__$1 = state_43844;
var statearr_43881_45339 = state_43844__$1;
(statearr_43881_45339[(2)] = inst_43826);

(statearr_43881_45339[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (2))){
var state_43844__$1 = state_43844;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_43844__$1,(4),in$);
} else {
if((state_val_43845 === (23))){
var inst_43834 = (state_43844[(2)]);
var state_43844__$1 = state_43844;
var statearr_43882_45341 = state_43844__$1;
(statearr_43882_45341[(2)] = inst_43834);

(statearr_43882_45341[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (19))){
var inst_43821 = (state_43844[(2)]);
var state_43844__$1 = state_43844;
var statearr_43883_45342 = state_43844__$1;
(statearr_43883_45342[(2)] = inst_43821);

(statearr_43883_45342[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (11))){
var inst_43806 = (state_43844[(7)]);
var inst_43791 = (state_43844[(12)]);
var inst_43806__$1 = cljs.core.seq(inst_43791);
var state_43844__$1 = (function (){var statearr_43888 = state_43844;
(statearr_43888[(7)] = inst_43806__$1);

return statearr_43888;
})();
if(inst_43806__$1){
var statearr_43889_45343 = state_43844__$1;
(statearr_43889_45343[(1)] = (14));

} else {
var statearr_43890_45344 = state_43844__$1;
(statearr_43890_45344[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (9))){
var inst_43828 = (state_43844[(2)]);
var inst_43829 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_43844__$1 = (function (){var statearr_43893 = state_43844;
(statearr_43893[(15)] = inst_43828);

return statearr_43893;
})();
if(cljs.core.truth_(inst_43829)){
var statearr_43894_45345 = state_43844__$1;
(statearr_43894_45345[(1)] = (21));

} else {
var statearr_43895_45346 = state_43844__$1;
(statearr_43895_45346[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (5))){
var inst_43783 = cljs.core.async.close_BANG_(out);
var state_43844__$1 = state_43844;
var statearr_43896_45348 = state_43844__$1;
(statearr_43896_45348[(2)] = inst_43783);

(statearr_43896_45348[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (14))){
var inst_43806 = (state_43844[(7)]);
var inst_43808 = cljs.core.chunked_seq_QMARK_(inst_43806);
var state_43844__$1 = state_43844;
if(inst_43808){
var statearr_43897_45352 = state_43844__$1;
(statearr_43897_45352[(1)] = (17));

} else {
var statearr_43899_45353 = state_43844__$1;
(statearr_43899_45353[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (16))){
var inst_43824 = (state_43844[(2)]);
var state_43844__$1 = state_43844;
var statearr_43901_45354 = state_43844__$1;
(statearr_43901_45354[(2)] = inst_43824);

(statearr_43901_45354[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43845 === (10))){
var inst_43792 = (state_43844[(8)]);
var inst_43794 = (state_43844[(10)]);
var inst_43799 = cljs.core._nth(inst_43792,inst_43794);
var state_43844__$1 = state_43844;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43844__$1,(13),out,inst_43799);
} else {
if((state_val_43845 === (18))){
var inst_43806 = (state_43844[(7)]);
var inst_43815 = cljs.core.first(inst_43806);
var state_43844__$1 = state_43844;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43844__$1,(20),out,inst_43815);
} else {
if((state_val_43845 === (8))){
var inst_43794 = (state_43844[(10)]);
var inst_43793 = (state_43844[(11)]);
var inst_43796 = (inst_43794 < inst_43793);
var inst_43797 = inst_43796;
var state_43844__$1 = state_43844;
if(cljs.core.truth_(inst_43797)){
var statearr_43913_45355 = state_43844__$1;
(statearr_43913_45355[(1)] = (10));

} else {
var statearr_43915_45356 = state_43844__$1;
(statearr_43915_45356[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__41223__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__41223__auto____0 = (function (){
var statearr_43918 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43918[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__41223__auto__);

(statearr_43918[(1)] = (1));

return statearr_43918;
});
var cljs$core$async$mapcat_STAR__$_state_machine__41223__auto____1 = (function (state_43844){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43844);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e43923){var ex__41226__auto__ = e43923;
var statearr_43924_45357 = state_43844;
(statearr_43924_45357[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43844[(4)]))){
var statearr_43925_45358 = state_43844;
(statearr_43925_45358[(1)] = cljs.core.first((state_43844[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45359 = state_43844;
state_43844 = G__45359;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__41223__auto__ = function(state_43844){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__41223__auto____1.call(this,state_43844);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__41223__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__41223__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_43926 = f__41556__auto__();
(statearr_43926[(6)] = c__41555__auto__);

return statearr_43926;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

return c__41555__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__43928 = arguments.length;
switch (G__43928) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__43932 = arguments.length;
switch (G__43932) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__43936 = arguments.length;
switch (G__43936) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__41555__auto___45366 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_43967){
var state_val_43968 = (state_43967[(1)]);
if((state_val_43968 === (7))){
var inst_43958 = (state_43967[(2)]);
var state_43967__$1 = state_43967;
var statearr_43982_45367 = state_43967__$1;
(statearr_43982_45367[(2)] = inst_43958);

(statearr_43982_45367[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (1))){
var inst_43939 = null;
var state_43967__$1 = (function (){var statearr_43983 = state_43967;
(statearr_43983[(7)] = inst_43939);

return statearr_43983;
})();
var statearr_43988_45371 = state_43967__$1;
(statearr_43988_45371[(2)] = null);

(statearr_43988_45371[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (4))){
var inst_43942 = (state_43967[(8)]);
var inst_43942__$1 = (state_43967[(2)]);
var inst_43944 = (inst_43942__$1 == null);
var inst_43945 = cljs.core.not(inst_43944);
var state_43967__$1 = (function (){var statearr_43990 = state_43967;
(statearr_43990[(8)] = inst_43942__$1);

return statearr_43990;
})();
if(inst_43945){
var statearr_43991_45379 = state_43967__$1;
(statearr_43991_45379[(1)] = (5));

} else {
var statearr_43992_45380 = state_43967__$1;
(statearr_43992_45380[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (6))){
var state_43967__$1 = state_43967;
var statearr_43993_45382 = state_43967__$1;
(statearr_43993_45382[(2)] = null);

(statearr_43993_45382[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (3))){
var inst_43963 = (state_43967[(2)]);
var inst_43965 = cljs.core.async.close_BANG_(out);
var state_43967__$1 = (function (){var statearr_43994 = state_43967;
(statearr_43994[(9)] = inst_43963);

return statearr_43994;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_43967__$1,inst_43965);
} else {
if((state_val_43968 === (2))){
var state_43967__$1 = state_43967;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_43967__$1,(4),ch);
} else {
if((state_val_43968 === (11))){
var inst_43942 = (state_43967[(8)]);
var inst_43952 = (state_43967[(2)]);
var inst_43939 = inst_43942;
var state_43967__$1 = (function (){var statearr_43995 = state_43967;
(statearr_43995[(10)] = inst_43952);

(statearr_43995[(7)] = inst_43939);

return statearr_43995;
})();
var statearr_43996_45389 = state_43967__$1;
(statearr_43996_45389[(2)] = null);

(statearr_43996_45389[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (9))){
var inst_43942 = (state_43967[(8)]);
var state_43967__$1 = state_43967;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_43967__$1,(11),out,inst_43942);
} else {
if((state_val_43968 === (5))){
var inst_43942 = (state_43967[(8)]);
var inst_43939 = (state_43967[(7)]);
var inst_43947 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_43942,inst_43939);
var state_43967__$1 = state_43967;
if(inst_43947){
var statearr_43998_45390 = state_43967__$1;
(statearr_43998_45390[(1)] = (8));

} else {
var statearr_43999_45392 = state_43967__$1;
(statearr_43999_45392[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (10))){
var inst_43955 = (state_43967[(2)]);
var state_43967__$1 = state_43967;
var statearr_44000_45399 = state_43967__$1;
(statearr_44000_45399[(2)] = inst_43955);

(statearr_44000_45399[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43968 === (8))){
var inst_43939 = (state_43967[(7)]);
var tmp43997 = inst_43939;
var inst_43939__$1 = tmp43997;
var state_43967__$1 = (function (){var statearr_44001 = state_43967;
(statearr_44001[(7)] = inst_43939__$1);

return statearr_44001;
})();
var statearr_44002_45400 = state_43967__$1;
(statearr_44002_45400[(2)] = null);

(statearr_44002_45400[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_44005 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_44005[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_44005[(1)] = (1));

return statearr_44005;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_43967){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_43967);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e44006){var ex__41226__auto__ = e44006;
var statearr_44007_45404 = state_43967;
(statearr_44007_45404[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_43967[(4)]))){
var statearr_44008_45405 = state_43967;
(statearr_44008_45405[(1)] = cljs.core.first((state_43967[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45406 = state_43967;
state_43967 = G__45406;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_43967){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_43967);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_44012 = f__41556__auto__();
(statearr_44012[(6)] = c__41555__auto___45366);

return statearr_44012;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__44016 = arguments.length;
switch (G__44016) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__41555__auto___45421 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_44057){
var state_val_44058 = (state_44057[(1)]);
if((state_val_44058 === (7))){
var inst_44053 = (state_44057[(2)]);
var state_44057__$1 = state_44057;
var statearr_44062_45426 = state_44057__$1;
(statearr_44062_45426[(2)] = inst_44053);

(statearr_44062_45426[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (1))){
var inst_44020 = (new Array(n));
var inst_44021 = inst_44020;
var inst_44022 = (0);
var state_44057__$1 = (function (){var statearr_44063 = state_44057;
(statearr_44063[(7)] = inst_44021);

(statearr_44063[(8)] = inst_44022);

return statearr_44063;
})();
var statearr_44064_45428 = state_44057__$1;
(statearr_44064_45428[(2)] = null);

(statearr_44064_45428[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (4))){
var inst_44025 = (state_44057[(9)]);
var inst_44025__$1 = (state_44057[(2)]);
var inst_44026 = (inst_44025__$1 == null);
var inst_44027 = cljs.core.not(inst_44026);
var state_44057__$1 = (function (){var statearr_44065 = state_44057;
(statearr_44065[(9)] = inst_44025__$1);

return statearr_44065;
})();
if(inst_44027){
var statearr_44066_45431 = state_44057__$1;
(statearr_44066_45431[(1)] = (5));

} else {
var statearr_44067_45432 = state_44057__$1;
(statearr_44067_45432[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (15))){
var inst_44047 = (state_44057[(2)]);
var state_44057__$1 = state_44057;
var statearr_44069_45433 = state_44057__$1;
(statearr_44069_45433[(2)] = inst_44047);

(statearr_44069_45433[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (13))){
var state_44057__$1 = state_44057;
var statearr_44075_45434 = state_44057__$1;
(statearr_44075_45434[(2)] = null);

(statearr_44075_45434[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (6))){
var inst_44022 = (state_44057[(8)]);
var inst_44043 = (inst_44022 > (0));
var state_44057__$1 = state_44057;
if(cljs.core.truth_(inst_44043)){
var statearr_44078_45435 = state_44057__$1;
(statearr_44078_45435[(1)] = (12));

} else {
var statearr_44079_45436 = state_44057__$1;
(statearr_44079_45436[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (3))){
var inst_44055 = (state_44057[(2)]);
var state_44057__$1 = state_44057;
return cljs.core.async.impl.ioc_helpers.return_chan(state_44057__$1,inst_44055);
} else {
if((state_val_44058 === (12))){
var inst_44021 = (state_44057[(7)]);
var inst_44045 = cljs.core.vec(inst_44021);
var state_44057__$1 = state_44057;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_44057__$1,(15),out,inst_44045);
} else {
if((state_val_44058 === (2))){
var state_44057__$1 = state_44057;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_44057__$1,(4),ch);
} else {
if((state_val_44058 === (11))){
var inst_44037 = (state_44057[(2)]);
var inst_44038 = (new Array(n));
var inst_44021 = inst_44038;
var inst_44022 = (0);
var state_44057__$1 = (function (){var statearr_44081 = state_44057;
(statearr_44081[(7)] = inst_44021);

(statearr_44081[(8)] = inst_44022);

(statearr_44081[(10)] = inst_44037);

return statearr_44081;
})();
var statearr_44084_45437 = state_44057__$1;
(statearr_44084_45437[(2)] = null);

(statearr_44084_45437[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (9))){
var inst_44021 = (state_44057[(7)]);
var inst_44035 = cljs.core.vec(inst_44021);
var state_44057__$1 = state_44057;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_44057__$1,(11),out,inst_44035);
} else {
if((state_val_44058 === (5))){
var inst_44025 = (state_44057[(9)]);
var inst_44021 = (state_44057[(7)]);
var inst_44022 = (state_44057[(8)]);
var inst_44030 = (state_44057[(11)]);
var inst_44029 = (inst_44021[inst_44022] = inst_44025);
var inst_44030__$1 = (inst_44022 + (1));
var inst_44031 = (inst_44030__$1 < n);
var state_44057__$1 = (function (){var statearr_44085 = state_44057;
(statearr_44085[(12)] = inst_44029);

(statearr_44085[(11)] = inst_44030__$1);

return statearr_44085;
})();
if(cljs.core.truth_(inst_44031)){
var statearr_44086_45438 = state_44057__$1;
(statearr_44086_45438[(1)] = (8));

} else {
var statearr_44087_45439 = state_44057__$1;
(statearr_44087_45439[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (14))){
var inst_44050 = (state_44057[(2)]);
var inst_44051 = cljs.core.async.close_BANG_(out);
var state_44057__$1 = (function (){var statearr_44089 = state_44057;
(statearr_44089[(13)] = inst_44050);

return statearr_44089;
})();
var statearr_44090_45440 = state_44057__$1;
(statearr_44090_45440[(2)] = inst_44051);

(statearr_44090_45440[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (10))){
var inst_44041 = (state_44057[(2)]);
var state_44057__$1 = state_44057;
var statearr_44094_45441 = state_44057__$1;
(statearr_44094_45441[(2)] = inst_44041);

(statearr_44094_45441[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44058 === (8))){
var inst_44021 = (state_44057[(7)]);
var inst_44030 = (state_44057[(11)]);
var tmp44088 = inst_44021;
var inst_44021__$1 = tmp44088;
var inst_44022 = inst_44030;
var state_44057__$1 = (function (){var statearr_44095 = state_44057;
(statearr_44095[(7)] = inst_44021__$1);

(statearr_44095[(8)] = inst_44022);

return statearr_44095;
})();
var statearr_44096_45442 = state_44057__$1;
(statearr_44096_45442[(2)] = null);

(statearr_44096_45442[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_44097 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_44097[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_44097[(1)] = (1));

return statearr_44097;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_44057){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_44057);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e44099){var ex__41226__auto__ = e44099;
var statearr_44100_45444 = state_44057;
(statearr_44100_45444[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_44057[(4)]))){
var statearr_44101_45448 = state_44057;
(statearr_44101_45448[(1)] = cljs.core.first((state_44057[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45449 = state_44057;
state_44057 = G__45449;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_44057){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_44057);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_44102 = f__41556__auto__();
(statearr_44102[(6)] = c__41555__auto___45421);

return statearr_44102;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__44104 = arguments.length;
switch (G__44104) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__41555__auto___45455 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_44152){
var state_val_44153 = (state_44152[(1)]);
if((state_val_44153 === (7))){
var inst_44148 = (state_44152[(2)]);
var state_44152__$1 = state_44152;
var statearr_44154_45456 = state_44152__$1;
(statearr_44154_45456[(2)] = inst_44148);

(statearr_44154_45456[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (1))){
var inst_44108 = [];
var inst_44109 = inst_44108;
var inst_44110 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_44152__$1 = (function (){var statearr_44155 = state_44152;
(statearr_44155[(7)] = inst_44109);

(statearr_44155[(8)] = inst_44110);

return statearr_44155;
})();
var statearr_44156_45458 = state_44152__$1;
(statearr_44156_45458[(2)] = null);

(statearr_44156_45458[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (4))){
var inst_44113 = (state_44152[(9)]);
var inst_44113__$1 = (state_44152[(2)]);
var inst_44114 = (inst_44113__$1 == null);
var inst_44115 = cljs.core.not(inst_44114);
var state_44152__$1 = (function (){var statearr_44157 = state_44152;
(statearr_44157[(9)] = inst_44113__$1);

return statearr_44157;
})();
if(inst_44115){
var statearr_44158_45460 = state_44152__$1;
(statearr_44158_45460[(1)] = (5));

} else {
var statearr_44159_45461 = state_44152__$1;
(statearr_44159_45461[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (15))){
var inst_44109 = (state_44152[(7)]);
var inst_44140 = cljs.core.vec(inst_44109);
var state_44152__$1 = state_44152;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_44152__$1,(18),out,inst_44140);
} else {
if((state_val_44153 === (13))){
var inst_44135 = (state_44152[(2)]);
var state_44152__$1 = state_44152;
var statearr_44161_45464 = state_44152__$1;
(statearr_44161_45464[(2)] = inst_44135);

(statearr_44161_45464[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (6))){
var inst_44109 = (state_44152[(7)]);
var inst_44137 = inst_44109.length;
var inst_44138 = (inst_44137 > (0));
var state_44152__$1 = state_44152;
if(cljs.core.truth_(inst_44138)){
var statearr_44162_45465 = state_44152__$1;
(statearr_44162_45465[(1)] = (15));

} else {
var statearr_44163_45466 = state_44152__$1;
(statearr_44163_45466[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (17))){
var inst_44145 = (state_44152[(2)]);
var inst_44146 = cljs.core.async.close_BANG_(out);
var state_44152__$1 = (function (){var statearr_44164 = state_44152;
(statearr_44164[(10)] = inst_44145);

return statearr_44164;
})();
var statearr_44165_45467 = state_44152__$1;
(statearr_44165_45467[(2)] = inst_44146);

(statearr_44165_45467[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (3))){
var inst_44150 = (state_44152[(2)]);
var state_44152__$1 = state_44152;
return cljs.core.async.impl.ioc_helpers.return_chan(state_44152__$1,inst_44150);
} else {
if((state_val_44153 === (12))){
var inst_44109 = (state_44152[(7)]);
var inst_44128 = cljs.core.vec(inst_44109);
var state_44152__$1 = state_44152;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_44152__$1,(14),out,inst_44128);
} else {
if((state_val_44153 === (2))){
var state_44152__$1 = state_44152;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_44152__$1,(4),ch);
} else {
if((state_val_44153 === (11))){
var inst_44109 = (state_44152[(7)]);
var inst_44117 = (state_44152[(11)]);
var inst_44113 = (state_44152[(9)]);
var inst_44125 = inst_44109.push(inst_44113);
var tmp44166 = inst_44109;
var inst_44109__$1 = tmp44166;
var inst_44110 = inst_44117;
var state_44152__$1 = (function (){var statearr_44167 = state_44152;
(statearr_44167[(7)] = inst_44109__$1);

(statearr_44167[(12)] = inst_44125);

(statearr_44167[(8)] = inst_44110);

return statearr_44167;
})();
var statearr_44169_45479 = state_44152__$1;
(statearr_44169_45479[(2)] = null);

(statearr_44169_45479[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (9))){
var inst_44110 = (state_44152[(8)]);
var inst_44121 = cljs.core.keyword_identical_QMARK_(inst_44110,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var state_44152__$1 = state_44152;
var statearr_44170_45480 = state_44152__$1;
(statearr_44170_45480[(2)] = inst_44121);

(statearr_44170_45480[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (5))){
var inst_44118 = (state_44152[(13)]);
var inst_44117 = (state_44152[(11)]);
var inst_44110 = (state_44152[(8)]);
var inst_44113 = (state_44152[(9)]);
var inst_44117__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_44113) : f.call(null,inst_44113));
var inst_44118__$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_44117__$1,inst_44110);
var state_44152__$1 = (function (){var statearr_44171 = state_44152;
(statearr_44171[(13)] = inst_44118__$1);

(statearr_44171[(11)] = inst_44117__$1);

return statearr_44171;
})();
if(inst_44118__$1){
var statearr_44172_45483 = state_44152__$1;
(statearr_44172_45483[(1)] = (8));

} else {
var statearr_44173_45484 = state_44152__$1;
(statearr_44173_45484[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (14))){
var inst_44117 = (state_44152[(11)]);
var inst_44113 = (state_44152[(9)]);
var inst_44130 = (state_44152[(2)]);
var inst_44131 = [];
var inst_44132 = inst_44131.push(inst_44113);
var inst_44109 = inst_44131;
var inst_44110 = inst_44117;
var state_44152__$1 = (function (){var statearr_44174 = state_44152;
(statearr_44174[(7)] = inst_44109);

(statearr_44174[(14)] = inst_44130);

(statearr_44174[(8)] = inst_44110);

(statearr_44174[(15)] = inst_44132);

return statearr_44174;
})();
var statearr_44175_45485 = state_44152__$1;
(statearr_44175_45485[(2)] = null);

(statearr_44175_45485[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (16))){
var state_44152__$1 = state_44152;
var statearr_44176_45486 = state_44152__$1;
(statearr_44176_45486[(2)] = null);

(statearr_44176_45486[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (10))){
var inst_44123 = (state_44152[(2)]);
var state_44152__$1 = state_44152;
if(cljs.core.truth_(inst_44123)){
var statearr_44177_45487 = state_44152__$1;
(statearr_44177_45487[(1)] = (11));

} else {
var statearr_44180_45488 = state_44152__$1;
(statearr_44180_45488[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (18))){
var inst_44142 = (state_44152[(2)]);
var state_44152__$1 = state_44152;
var statearr_44181_45489 = state_44152__$1;
(statearr_44181_45489[(2)] = inst_44142);

(statearr_44181_45489[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44153 === (8))){
var inst_44118 = (state_44152[(13)]);
var state_44152__$1 = state_44152;
var statearr_44182_45490 = state_44152__$1;
(statearr_44182_45490[(2)] = inst_44118);

(statearr_44182_45490[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__41223__auto__ = null;
var cljs$core$async$state_machine__41223__auto____0 = (function (){
var statearr_44183 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_44183[(0)] = cljs$core$async$state_machine__41223__auto__);

(statearr_44183[(1)] = (1));

return statearr_44183;
});
var cljs$core$async$state_machine__41223__auto____1 = (function (state_44152){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_44152);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e44188){var ex__41226__auto__ = e44188;
var statearr_44189_45491 = state_44152;
(statearr_44189_45491[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_44152[(4)]))){
var statearr_44190_45492 = state_44152;
(statearr_44190_45492[(1)] = cljs.core.first((state_44152[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45493 = state_44152;
state_44152 = G__45493;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
cljs$core$async$state_machine__41223__auto__ = function(state_44152){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__41223__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__41223__auto____1.call(this,state_44152);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__41223__auto____0;
cljs$core$async$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__41223__auto____1;
return cljs$core$async$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_44196 = f__41556__auto__();
(statearr_44196[(6)] = c__41555__auto___45455);

return statearr_44196;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
