goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__46145){
var map__46146 = p__46145;
var map__46146__$1 = cljs.core.__destructure_map(map__46146);
var m = map__46146__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46146__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46146__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4253__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return [(function (){var temp__5804__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__46150_46400 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__46151_46401 = null;
var count__46152_46402 = (0);
var i__46153_46403 = (0);
while(true){
if((i__46153_46403 < count__46152_46402)){
var f_46404 = chunk__46151_46401.cljs$core$IIndexed$_nth$arity$2(null,i__46153_46403);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_46404], 0));


var G__46405 = seq__46150_46400;
var G__46406 = chunk__46151_46401;
var G__46407 = count__46152_46402;
var G__46408 = (i__46153_46403 + (1));
seq__46150_46400 = G__46405;
chunk__46151_46401 = G__46406;
count__46152_46402 = G__46407;
i__46153_46403 = G__46408;
continue;
} else {
var temp__5804__auto___46409 = cljs.core.seq(seq__46150_46400);
if(temp__5804__auto___46409){
var seq__46150_46410__$1 = temp__5804__auto___46409;
if(cljs.core.chunked_seq_QMARK_(seq__46150_46410__$1)){
var c__4679__auto___46411 = cljs.core.chunk_first(seq__46150_46410__$1);
var G__46412 = cljs.core.chunk_rest(seq__46150_46410__$1);
var G__46413 = c__4679__auto___46411;
var G__46414 = cljs.core.count(c__4679__auto___46411);
var G__46415 = (0);
seq__46150_46400 = G__46412;
chunk__46151_46401 = G__46413;
count__46152_46402 = G__46414;
i__46153_46403 = G__46415;
continue;
} else {
var f_46417 = cljs.core.first(seq__46150_46410__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_46417], 0));


var G__46418 = cljs.core.next(seq__46150_46410__$1);
var G__46419 = null;
var G__46420 = (0);
var G__46421 = (0);
seq__46150_46400 = G__46418;
chunk__46151_46401 = G__46419;
count__46152_46402 = G__46420;
i__46153_46403 = G__46421;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_46422 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4253__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_46422], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_46422)))?cljs.core.second(arglists_46422):arglists_46422)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__46168_46430 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__46169_46431 = null;
var count__46170_46432 = (0);
var i__46171_46433 = (0);
while(true){
if((i__46171_46433 < count__46170_46432)){
var vec__46186_46434 = chunk__46169_46431.cljs$core$IIndexed$_nth$arity$2(null,i__46171_46433);
var name_46435 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46186_46434,(0),null);
var map__46189_46436 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46186_46434,(1),null);
var map__46189_46437__$1 = cljs.core.__destructure_map(map__46189_46436);
var doc_46438 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46189_46437__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_46439 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46189_46437__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_46435], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_46439], 0));

if(cljs.core.truth_(doc_46438)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_46438], 0));
} else {
}


var G__46443 = seq__46168_46430;
var G__46444 = chunk__46169_46431;
var G__46445 = count__46170_46432;
var G__46446 = (i__46171_46433 + (1));
seq__46168_46430 = G__46443;
chunk__46169_46431 = G__46444;
count__46170_46432 = G__46445;
i__46171_46433 = G__46446;
continue;
} else {
var temp__5804__auto___46447 = cljs.core.seq(seq__46168_46430);
if(temp__5804__auto___46447){
var seq__46168_46448__$1 = temp__5804__auto___46447;
if(cljs.core.chunked_seq_QMARK_(seq__46168_46448__$1)){
var c__4679__auto___46449 = cljs.core.chunk_first(seq__46168_46448__$1);
var G__46450 = cljs.core.chunk_rest(seq__46168_46448__$1);
var G__46451 = c__4679__auto___46449;
var G__46452 = cljs.core.count(c__4679__auto___46449);
var G__46453 = (0);
seq__46168_46430 = G__46450;
chunk__46169_46431 = G__46451;
count__46170_46432 = G__46452;
i__46171_46433 = G__46453;
continue;
} else {
var vec__46191_46454 = cljs.core.first(seq__46168_46448__$1);
var name_46455 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46191_46454,(0),null);
var map__46194_46456 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46191_46454,(1),null);
var map__46194_46457__$1 = cljs.core.__destructure_map(map__46194_46456);
var doc_46458 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46194_46457__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_46459 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46194_46457__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_46455], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_46459], 0));

if(cljs.core.truth_(doc_46458)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_46458], 0));
} else {
}


var G__46463 = cljs.core.next(seq__46168_46448__$1);
var G__46464 = null;
var G__46465 = (0);
var G__46466 = (0);
seq__46168_46430 = G__46463;
chunk__46169_46431 = G__46464;
count__46170_46432 = G__46465;
i__46171_46433 = G__46466;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5804__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5804__auto__)){
var fnspec = temp__5804__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__46200 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__46201 = null;
var count__46202 = (0);
var i__46203 = (0);
while(true){
if((i__46203 < count__46202)){
var role = chunk__46201.cljs$core$IIndexed$_nth$arity$2(null,i__46203);
var temp__5804__auto___46468__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___46468__$1)){
var spec_46469 = temp__5804__auto___46468__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_46469)], 0));
} else {
}


var G__46473 = seq__46200;
var G__46474 = chunk__46201;
var G__46475 = count__46202;
var G__46476 = (i__46203 + (1));
seq__46200 = G__46473;
chunk__46201 = G__46474;
count__46202 = G__46475;
i__46203 = G__46476;
continue;
} else {
var temp__5804__auto____$1 = cljs.core.seq(seq__46200);
if(temp__5804__auto____$1){
var seq__46200__$1 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__46200__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__46200__$1);
var G__46477 = cljs.core.chunk_rest(seq__46200__$1);
var G__46478 = c__4679__auto__;
var G__46479 = cljs.core.count(c__4679__auto__);
var G__46480 = (0);
seq__46200 = G__46477;
chunk__46201 = G__46478;
count__46202 = G__46479;
i__46203 = G__46480;
continue;
} else {
var role = cljs.core.first(seq__46200__$1);
var temp__5804__auto___46481__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___46481__$2)){
var spec_46482 = temp__5804__auto___46481__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_46482)], 0));
} else {
}


var G__46483 = cljs.core.next(seq__46200__$1);
var G__46484 = null;
var G__46485 = (0);
var G__46486 = (0);
seq__46200 = G__46483;
chunk__46201 = G__46484;
count__46202 = G__46485;
i__46203 = G__46486;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol("cljs.core","ExceptionInfo","cljs.core/ExceptionInfo",701839050,null):(((t instanceof Error))?cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("js",t.name):null
))], null),(function (){var temp__5804__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5804__auto__)){
var msg = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5804__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5804__auto__)){
var ed = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__46490 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__46491 = cljs.core.ex_cause(t);
via = G__46490;
t = G__46491;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5804__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5804__auto__)){
var root_msg = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5804__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5804__auto__)){
var data = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5804__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5804__auto__)){
var phase = temp__5804__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__46288 = datafied_throwable;
var map__46288__$1 = cljs.core.__destructure_map(map__46288);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46288__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46288__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__46288__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__46289 = cljs.core.last(via);
var map__46289__$1 = cljs.core.__destructure_map(map__46289);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46289__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46289__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46289__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__46290 = data;
var map__46290__$1 = cljs.core.__destructure_map(map__46290);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46290__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46290__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46290__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__46291 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__46291__$1 = cljs.core.__destructure_map(map__46291);
var top_data = map__46291__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46291__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__46311 = phase;
var G__46311__$1 = (((G__46311 instanceof cljs.core.Keyword))?G__46311.fqn:null);
switch (G__46311__$1) {
case "read-source":
var map__46317 = data;
var map__46317__$1 = cljs.core.__destructure_map(map__46317);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46317__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46317__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__46318 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__46318__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46318,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__46318);
var G__46318__$2 = (cljs.core.truth_((function (){var fexpr__46320 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__46320.cljs$core$IFn$_invoke$arity$1 ? fexpr__46320.cljs$core$IFn$_invoke$arity$1(source) : fexpr__46320.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__46318__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__46318__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46318__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__46318__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__46325 = top_data;
var G__46325__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46325,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__46325);
var G__46325__$2 = (cljs.core.truth_((function (){var fexpr__46327 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__46327.cljs$core$IFn$_invoke$arity$1 ? fexpr__46327.cljs$core$IFn$_invoke$arity$1(source) : fexpr__46327.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__46325__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__46325__$1);
var G__46325__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46325__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__46325__$2);
var G__46325__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46325__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__46325__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46325__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__46325__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__46328 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46328,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46328,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46328,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46328,(3),null);
var G__46331 = top_data;
var G__46331__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46331,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__46331);
var G__46331__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46331__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__46331__$1);
var G__46331__$3 = (cljs.core.truth_((function (){var and__4251__auto__ = source__$1;
if(cljs.core.truth_(and__4251__auto__)){
return method;
} else {
return and__4251__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46331__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__46331__$2);
var G__46331__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46331__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__46331__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46331__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__46331__$4;
}

break;
case "execution":
var vec__46335 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46335,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46335,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46335,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__46335,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__46279_SHARP_){
var or__4253__auto__ = (p1__46279_SHARP_ == null);
if(or__4253__auto__){
return or__4253__auto__;
} else {
var fexpr__46338 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__46338.cljs$core$IFn$_invoke$arity$1 ? fexpr__46338.cljs$core$IFn$_invoke$arity$1(p1__46279_SHARP_) : fexpr__46338.call(null,p1__46279_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4253__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return line;
}
})();
var G__46339 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__46339__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46339,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__46339);
var G__46339__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46339__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__46339__$1);
var G__46339__$3 = (cljs.core.truth_((function (){var or__4253__auto__ = fn;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
var and__4251__auto__ = source__$1;
if(cljs.core.truth_(and__4251__auto__)){
return method;
} else {
return and__4251__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46339__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4253__auto__ = fn;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__46339__$2);
var G__46339__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46339__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__46339__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__46339__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__46339__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__46311__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__46345){
var map__46346 = p__46345;
var map__46346__$1 = cljs.core.__destructure_map(map__46346);
var triage_data = map__46346__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__46346__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4253__auto__ = source;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4253__auto__ = line;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4253__auto__ = class$;
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__46357 = phase;
var G__46357__$1 = (((G__46357 instanceof cljs.core.Keyword))?G__46357.fqn:null);
switch (G__46357__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__46358 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__46359 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__46360 = loc;
var G__46361 = (cljs.core.truth_(spec)?(function (){var sb__4795__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__46363_46522 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__46364_46523 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__46365_46524 = true;
var _STAR_print_fn_STAR__temp_val__46366_46525 = (function (x__4796__auto__){
return sb__4795__auto__.append(x__4796__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__46365_46524);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__46366_46525);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__46343_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__46343_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__46364_46523);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__46363_46522);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4795__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__46358,G__46359,G__46360,G__46361) : format.call(null,G__46358,G__46359,G__46360,G__46361));

break;
case "macroexpansion":
var G__46367 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__46368 = cause_type;
var G__46369 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__46370 = loc;
var G__46371 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__46367,G__46368,G__46369,G__46370,G__46371) : format.call(null,G__46367,G__46368,G__46369,G__46370,G__46371));

break;
case "compile-syntax-check":
var G__46372 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__46373 = cause_type;
var G__46374 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__46375 = loc;
var G__46376 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__46372,G__46373,G__46374,G__46375,G__46376) : format.call(null,G__46372,G__46373,G__46374,G__46375,G__46376));

break;
case "compilation":
var G__46377 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__46378 = cause_type;
var G__46379 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__46380 = loc;
var G__46381 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__46377,G__46378,G__46379,G__46380,G__46381) : format.call(null,G__46377,G__46378,G__46379,G__46380,G__46381));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__46382 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__46383 = symbol;
var G__46384 = loc;
var G__46385 = (function (){var sb__4795__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__46387_46533 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__46388_46534 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__46389_46535 = true;
var _STAR_print_fn_STAR__temp_val__46390_46536 = (function (x__4796__auto__){
return sb__4795__auto__.append(x__4796__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__46389_46535);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__46390_46536);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__46344_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__46344_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__46388_46534);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__46387_46533);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4795__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__46382,G__46383,G__46384,G__46385) : format.call(null,G__46382,G__46383,G__46384,G__46385));
} else {
var G__46394 = "Execution error%s at %s(%s).\n%s\n";
var G__46395 = cause_type;
var G__46396 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__46397 = loc;
var G__46398 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__46394,G__46395,G__46396,G__46397,G__46398) : format.call(null,G__46394,G__46395,G__46396,G__46397,G__46398));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__46357__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
