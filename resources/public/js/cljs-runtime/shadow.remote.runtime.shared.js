goog.provide('shadow.remote.runtime.shared');
shadow.remote.runtime.shared.init_state = (function shadow$remote$runtime$shared$init_state(client_info){
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),(0),new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.PersistentArrayMap.EMPTY], null);
});
shadow.remote.runtime.shared.now = (function shadow$remote$runtime$shared$now(){
return Date.now();
});
shadow.remote.runtime.shared.relay_msg = (function shadow$remote$runtime$shared$relay_msg(runtime,msg){
return shadow.remote.runtime.api.relay_msg(runtime,msg);
});
shadow.remote.runtime.shared.reply = (function shadow$remote$runtime$shared$reply(runtime,p__43240,res){
var map__43241 = p__43240;
var map__43241__$1 = cljs.core.__destructure_map(map__43241);
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43241__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43241__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var res__$1 = (function (){var G__43243 = res;
var G__43243__$1 = (cljs.core.truth_(call_id)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__43243,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id):G__43243);
if(cljs.core.truth_(from)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__43243__$1,new cljs.core.Keyword(null,"to","to",192099007),from);
} else {
return G__43243__$1;
}
})();
return shadow.remote.runtime.api.relay_msg(runtime,res__$1);
});
shadow.remote.runtime.shared.call = (function shadow$remote$runtime$shared$call(var_args){
var G__43249 = arguments.length;
switch (G__43249) {
case 3:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3 = (function (runtime,msg,handlers){
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4(runtime,msg,handlers,(0));
}));

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4 = (function (p__43271,msg,handlers,timeout_after_ms){
var map__43274 = p__43271;
var map__43274__$1 = cljs.core.__destructure_map(map__43274);
var runtime = map__43274__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43274__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var call_id = new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),cljs.core.inc);

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handlers","handlers",79528781),handlers,new cljs.core.Keyword(null,"called-at","called-at",607081160),shadow.remote.runtime.shared.now(),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg,new cljs.core.Keyword(null,"timeout","timeout",-318625318),timeout_after_ms], null));

return shadow.remote.runtime.api.relay_msg(runtime,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id));
}));

(shadow.remote.runtime.shared.call.cljs$lang$maxFixedArity = 4);

shadow.remote.runtime.shared.trigger_BANG_ = (function shadow$remote$runtime$shared$trigger_BANG_(var_args){
var args__4870__auto__ = [];
var len__4864__auto___43545 = arguments.length;
var i__4865__auto___43546 = (0);
while(true){
if((i__4865__auto___43546 < len__4864__auto___43545)){
args__4870__auto__.push((arguments[i__4865__auto___43546]));

var G__43548 = (i__4865__auto___43546 + (1));
i__4865__auto___43546 = G__43548;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((2) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((2)),(0),null)):null);
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4871__auto__);
});

(shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__43301,ev,args){
var map__43302 = p__43301;
var map__43302__$1 = cljs.core.__destructure_map(map__43302);
var runtime = map__43302__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43302__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var seq__43305 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__43308 = null;
var count__43309 = (0);
var i__43310 = (0);
while(true){
if((i__43310 < count__43309)){
var ext = chunk__43308.cljs$core$IIndexed$_nth$arity$2(null,i__43310);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__43552 = seq__43305;
var G__43553 = chunk__43308;
var G__43554 = count__43309;
var G__43555 = (i__43310 + (1));
seq__43305 = G__43552;
chunk__43308 = G__43553;
count__43309 = G__43554;
i__43310 = G__43555;
continue;
} else {
var G__43558 = seq__43305;
var G__43559 = chunk__43308;
var G__43560 = count__43309;
var G__43561 = (i__43310 + (1));
seq__43305 = G__43558;
chunk__43308 = G__43559;
count__43309 = G__43560;
i__43310 = G__43561;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__43305);
if(temp__5804__auto__){
var seq__43305__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__43305__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__43305__$1);
var G__43564 = cljs.core.chunk_rest(seq__43305__$1);
var G__43565 = c__4679__auto__;
var G__43566 = cljs.core.count(c__4679__auto__);
var G__43567 = (0);
seq__43305 = G__43564;
chunk__43308 = G__43565;
count__43309 = G__43566;
i__43310 = G__43567;
continue;
} else {
var ext = cljs.core.first(seq__43305__$1);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__43572 = cljs.core.next(seq__43305__$1);
var G__43573 = null;
var G__43574 = (0);
var G__43575 = (0);
seq__43305 = G__43572;
chunk__43308 = G__43573;
count__43309 = G__43574;
i__43310 = G__43575;
continue;
} else {
var G__43577 = cljs.core.next(seq__43305__$1);
var G__43578 = null;
var G__43579 = (0);
var G__43580 = (0);
seq__43305 = G__43577;
chunk__43308 = G__43578;
count__43309 = G__43579;
i__43310 = G__43580;
continue;
}
}
} else {
return null;
}
}
break;
}
}));

(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$applyTo = (function (seq43291){
var G__43292 = cljs.core.first(seq43291);
var seq43291__$1 = cljs.core.next(seq43291);
var G__43293 = cljs.core.first(seq43291__$1);
var seq43291__$2 = cljs.core.next(seq43291__$1);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__43292,G__43293,seq43291__$2);
}));

shadow.remote.runtime.shared.welcome = (function shadow$remote$runtime$shared$welcome(p__43347,p__43348){
var map__43349 = p__43347;
var map__43349__$1 = cljs.core.__destructure_map(map__43349);
var runtime = map__43349__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43349__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__43350 = p__43348;
var map__43350__$1 = cljs.core.__destructure_map(map__43350);
var msg = map__43350__$1;
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43350__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc,new cljs.core.Keyword(null,"client-id","client-id",-464622140),client_id);

var map__43353 = cljs.core.deref(state_ref);
var map__43353__$1 = cljs.core.__destructure_map(map__43353);
var client_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43353__$1,new cljs.core.Keyword(null,"client-info","client-info",1958982504));
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43353__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
shadow.remote.runtime.shared.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"hello","hello",-245025397),new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info], null));

return shadow.remote.runtime.shared.trigger_BANG_(runtime,new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125));
});
shadow.remote.runtime.shared.ping = (function shadow$remote$runtime$shared$ping(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"pong","pong",-172484958)], null));
});
shadow.remote.runtime.shared.get_client_id = (function shadow$remote$runtime$shared$get_client_id(p__43363){
var map__43366 = p__43363;
var map__43366__$1 = cljs.core.__destructure_map(map__43366);
var runtime = map__43366__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43366__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var or__4253__auto__ = new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("runtime has no assigned runtime-id",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null));
}
});
shadow.remote.runtime.shared.request_supported_ops = (function shadow$remote$runtime$shared$request_supported_ops(p__43374,msg){
var map__43375 = p__43374;
var map__43375__$1 = cljs.core.__destructure_map(map__43375);
var runtime = map__43375__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43375__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"supported-ops","supported-ops",337914702),new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.disj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.set(cljs.core.keys(new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref)))),new cljs.core.Keyword(null,"welcome","welcome",-578152123),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),new cljs.core.Keyword(null,"tool-disconnect","tool-disconnect",189103996)], 0))], null));
});
shadow.remote.runtime.shared.unknown_relay_op = (function shadow$remote$runtime$shared$unknown_relay_op(msg){
return console.warn("unknown-relay-op",msg);
});
shadow.remote.runtime.shared.unknown_op = (function shadow$remote$runtime$shared$unknown_op(msg){
return console.warn("unknown-op",msg);
});
shadow.remote.runtime.shared.add_extension_STAR_ = (function shadow$remote$runtime$shared$add_extension_STAR_(p__43389,key,p__43390){
var map__43392 = p__43389;
var map__43392__$1 = cljs.core.__destructure_map(map__43392);
var state = map__43392__$1;
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43392__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
var map__43393 = p__43390;
var map__43393__$1 = cljs.core.__destructure_map(map__43393);
var spec = map__43393__$1;
var ops = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43393__$1,new cljs.core.Keyword(null,"ops","ops",1237330063));
if(cljs.core.contains_QMARK_(extensions,key)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("extension already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"spec","spec",347520401),spec], null));
} else {
}

return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null)))){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("op already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"op","op",-1882987955),op_kw], null));
} else {
}

return cljs.core.assoc_in(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null),op_handler);
}),cljs.core.assoc_in(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null),spec),ops);
});
shadow.remote.runtime.shared.add_extension = (function shadow$remote$runtime$shared$add_extension(p__43400,key,spec){
var map__43401 = p__43400;
var map__43401__$1 = cljs.core.__destructure_map(map__43401);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43401__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,shadow.remote.runtime.shared.add_extension_STAR_,key,spec);
});
shadow.remote.runtime.shared.add_defaults = (function shadow$remote$runtime$shared$add_defaults(runtime){
return shadow.remote.runtime.shared.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.shared","defaults","shadow.remote.runtime.shared/defaults",-1821257543),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"welcome","welcome",-578152123),(function (p1__43404_SHARP_){
return shadow.remote.runtime.shared.welcome(runtime,p1__43404_SHARP_);
}),new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),(function (p1__43405_SHARP_){
return shadow.remote.runtime.shared.unknown_relay_op(p1__43405_SHARP_);
}),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),(function (p1__43406_SHARP_){
return shadow.remote.runtime.shared.unknown_op(p1__43406_SHARP_);
}),new cljs.core.Keyword(null,"ping","ping",-1670114784),(function (p1__43407_SHARP_){
return shadow.remote.runtime.shared.ping(runtime,p1__43407_SHARP_);
}),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),(function (p1__43408_SHARP_){
return shadow.remote.runtime.shared.request_supported_ops(runtime,p1__43408_SHARP_);
})], null)], null));
});
shadow.remote.runtime.shared.del_extension_STAR_ = (function shadow$remote$runtime$shared$del_extension_STAR_(state,key){
var ext = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null));
if(cljs.core.not(ext)){
return state;
} else {
return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(state__$1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063)], null),cljs.core.dissoc,op_kw);
}),cljs.core.update.cljs$core$IFn$_invoke$arity$4(state,new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.dissoc,key),new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(ext));
}
});
shadow.remote.runtime.shared.del_extension = (function shadow$remote$runtime$shared$del_extension(p__43453,key){
var map__43455 = p__43453;
var map__43455__$1 = cljs.core.__destructure_map(map__43455);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43455__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(state_ref,shadow.remote.runtime.shared.del_extension_STAR_,key);
});
shadow.remote.runtime.shared.unhandled_call_result = (function shadow$remote$runtime$shared$unhandled_call_result(call_config,msg){
return console.warn("unhandled call result",msg,call_config);
});
shadow.remote.runtime.shared.unhandled_client_not_found = (function shadow$remote$runtime$shared$unhandled_client_not_found(p__43463,msg){
var map__43466 = p__43463;
var map__43466__$1 = cljs.core.__destructure_map(map__43466);
var runtime = map__43466__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43466__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic(runtime,new cljs.core.Keyword(null,"on-client-not-found","on-client-not-found",-642452849),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0));
});
shadow.remote.runtime.shared.reply_unknown_op = (function shadow$remote$runtime$shared$reply_unknown_op(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg], null));
});
shadow.remote.runtime.shared.process = (function shadow$remote$runtime$shared$process(p__43473,p__43474){
var map__43476 = p__43473;
var map__43476__$1 = cljs.core.__destructure_map(map__43476);
var runtime = map__43476__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43476__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__43477 = p__43474;
var map__43477__$1 = cljs.core.__destructure_map(map__43477);
var msg = map__43477__$1;
var op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43477__$1,new cljs.core.Keyword(null,"op","op",-1882987955));
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43477__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var state = cljs.core.deref(state_ref);
var op_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op], null));
if(cljs.core.truth_(call_id)){
var cfg = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null));
var call_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cfg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"handlers","handlers",79528781),op], null));
if(cljs.core.truth_(call_handler)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([call_id], 0));

return (call_handler.cljs$core$IFn$_invoke$arity$1 ? call_handler.cljs$core$IFn$_invoke$arity$1(msg) : call_handler.call(null,msg));
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
return shadow.remote.runtime.shared.unhandled_call_result(cfg,msg);

}
}
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-not-found","client-not-found",-1754042614),op)){
return shadow.remote.runtime.shared.unhandled_client_not_found(runtime,msg);
} else {
return shadow.remote.runtime.shared.reply_unknown_op(runtime,msg);

}
}
}
});
shadow.remote.runtime.shared.run_on_idle = (function shadow$remote$runtime$shared$run_on_idle(state_ref){
var seq__43491 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__43493 = null;
var count__43494 = (0);
var i__43495 = (0);
while(true){
if((i__43495 < count__43494)){
var map__43508 = chunk__43493.cljs$core$IIndexed$_nth$arity$2(null,i__43495);
var map__43508__$1 = cljs.core.__destructure_map(map__43508);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43508__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__43626 = seq__43491;
var G__43627 = chunk__43493;
var G__43628 = count__43494;
var G__43629 = (i__43495 + (1));
seq__43491 = G__43626;
chunk__43493 = G__43627;
count__43494 = G__43628;
i__43495 = G__43629;
continue;
} else {
var G__43631 = seq__43491;
var G__43632 = chunk__43493;
var G__43633 = count__43494;
var G__43634 = (i__43495 + (1));
seq__43491 = G__43631;
chunk__43493 = G__43632;
count__43494 = G__43633;
i__43495 = G__43634;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__43491);
if(temp__5804__auto__){
var seq__43491__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__43491__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__43491__$1);
var G__43635 = cljs.core.chunk_rest(seq__43491__$1);
var G__43636 = c__4679__auto__;
var G__43637 = cljs.core.count(c__4679__auto__);
var G__43638 = (0);
seq__43491 = G__43635;
chunk__43493 = G__43636;
count__43494 = G__43637;
i__43495 = G__43638;
continue;
} else {
var map__43520 = cljs.core.first(seq__43491__$1);
var map__43520__$1 = cljs.core.__destructure_map(map__43520);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43520__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__43642 = cljs.core.next(seq__43491__$1);
var G__43644 = null;
var G__43645 = (0);
var G__43646 = (0);
seq__43491 = G__43642;
chunk__43493 = G__43644;
count__43494 = G__43645;
i__43495 = G__43646;
continue;
} else {
var G__43647 = cljs.core.next(seq__43491__$1);
var G__43648 = null;
var G__43649 = (0);
var G__43650 = (0);
seq__43491 = G__43647;
chunk__43493 = G__43648;
count__43494 = G__43649;
i__43495 = G__43650;
continue;
}
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=shadow.remote.runtime.shared.js.map
