goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_45685 = (function (this$){
var x__4550__auto__ = (((this$ == null))?null:this$);
var m__4551__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4551__auto__.call(null,this$));
} else {
var m__4549__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4549__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_45685(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_45686 = (function (this$){
var x__4550__auto__ = (((this$ == null))?null:this$);
var m__4551__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4550__auto__)]);
if((!((m__4551__auto__ == null)))){
return (m__4551__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4551__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4551__auto__.call(null,this$));
} else {
var m__4549__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4549__auto__ == null)))){
return (m__4549__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4549__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4549__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_45686(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__44351 = coll;
var G__44352 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__44351,G__44352) : shadow.dom.lazy_native_coll_seq.call(null,G__44351,G__44352));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4253__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4491__auto__,writer__4492__auto__,opt__4493__auto__){
return cljs.core._write(writer__4492__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__44391 = arguments.length;
switch (G__44391) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__44429 = arguments.length;
switch (G__44429) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__44442 = arguments.length;
switch (G__44442) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__44455 = arguments.length;
switch (G__44455) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__44468 = arguments.length;
switch (G__44468) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__44486 = arguments.length;
switch (G__44486) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4253__auto__ = (!((typeof document !== 'undefined')));
if(or__4253__auto__){
return or__4253__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e44507){if((e44507 instanceof Object)){
var e = e44507;
return console.log("didnt support attachEvent",el,e);
} else {
throw e44507;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4253__auto__ = (!((typeof document !== 'undefined')));
if(or__4253__auto__){
return or__4253__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__44534 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__44535 = null;
var count__44536 = (0);
var i__44537 = (0);
while(true){
if((i__44537 < count__44536)){
var el = chunk__44535.cljs$core$IIndexed$_nth$arity$2(null,i__44537);
var handler_45710__$1 = ((function (seq__44534,chunk__44535,count__44536,i__44537,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__44534,chunk__44535,count__44536,i__44537,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_45710__$1);


var G__45711 = seq__44534;
var G__45712 = chunk__44535;
var G__45713 = count__44536;
var G__45714 = (i__44537 + (1));
seq__44534 = G__45711;
chunk__44535 = G__45712;
count__44536 = G__45713;
i__44537 = G__45714;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__44534);
if(temp__5804__auto__){
var seq__44534__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__44534__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__44534__$1);
var G__45715 = cljs.core.chunk_rest(seq__44534__$1);
var G__45716 = c__4679__auto__;
var G__45717 = cljs.core.count(c__4679__auto__);
var G__45718 = (0);
seq__44534 = G__45715;
chunk__44535 = G__45716;
count__44536 = G__45717;
i__44537 = G__45718;
continue;
} else {
var el = cljs.core.first(seq__44534__$1);
var handler_45719__$1 = ((function (seq__44534,chunk__44535,count__44536,i__44537,el,seq__44534__$1,temp__5804__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__44534,chunk__44535,count__44536,i__44537,el,seq__44534__$1,temp__5804__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_45719__$1);


var G__45720 = cljs.core.next(seq__44534__$1);
var G__45721 = null;
var G__45722 = (0);
var G__45723 = (0);
seq__44534 = G__45720;
chunk__44535 = G__45721;
count__44536 = G__45722;
i__44537 = G__45723;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__44590 = arguments.length;
switch (G__44590) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__44605 = cljs.core.seq(events);
var chunk__44606 = null;
var count__44607 = (0);
var i__44608 = (0);
while(true){
if((i__44608 < count__44607)){
var vec__44625 = chunk__44606.cljs$core$IIndexed$_nth$arity$2(null,i__44608);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44625,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44625,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__45725 = seq__44605;
var G__45726 = chunk__44606;
var G__45727 = count__44607;
var G__45728 = (i__44608 + (1));
seq__44605 = G__45725;
chunk__44606 = G__45726;
count__44607 = G__45727;
i__44608 = G__45728;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__44605);
if(temp__5804__auto__){
var seq__44605__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__44605__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__44605__$1);
var G__45729 = cljs.core.chunk_rest(seq__44605__$1);
var G__45730 = c__4679__auto__;
var G__45731 = cljs.core.count(c__4679__auto__);
var G__45732 = (0);
seq__44605 = G__45729;
chunk__44606 = G__45730;
count__44607 = G__45731;
i__44608 = G__45732;
continue;
} else {
var vec__44634 = cljs.core.first(seq__44605__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44634,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44634,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__45733 = cljs.core.next(seq__44605__$1);
var G__45734 = null;
var G__45735 = (0);
var G__45736 = (0);
seq__44605 = G__45733;
chunk__44606 = G__45734;
count__44607 = G__45735;
i__44608 = G__45736;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__44640 = cljs.core.seq(styles);
var chunk__44641 = null;
var count__44642 = (0);
var i__44643 = (0);
while(true){
if((i__44643 < count__44642)){
var vec__44662 = chunk__44641.cljs$core$IIndexed$_nth$arity$2(null,i__44643);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44662,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44662,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__45737 = seq__44640;
var G__45738 = chunk__44641;
var G__45739 = count__44642;
var G__45740 = (i__44643 + (1));
seq__44640 = G__45737;
chunk__44641 = G__45738;
count__44642 = G__45739;
i__44643 = G__45740;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__44640);
if(temp__5804__auto__){
var seq__44640__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__44640__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__44640__$1);
var G__45741 = cljs.core.chunk_rest(seq__44640__$1);
var G__45742 = c__4679__auto__;
var G__45743 = cljs.core.count(c__4679__auto__);
var G__45744 = (0);
seq__44640 = G__45741;
chunk__44641 = G__45742;
count__44642 = G__45743;
i__44643 = G__45744;
continue;
} else {
var vec__44677 = cljs.core.first(seq__44640__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44677,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44677,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__45745 = cljs.core.next(seq__44640__$1);
var G__45746 = null;
var G__45747 = (0);
var G__45748 = (0);
seq__44640 = G__45745;
chunk__44641 = G__45746;
count__44642 = G__45747;
i__44643 = G__45748;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__44685_45749 = key;
var G__44685_45750__$1 = (((G__44685_45749 instanceof cljs.core.Keyword))?G__44685_45749.fqn:null);
switch (G__44685_45750__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_45753 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4253__auto__ = goog.string.startsWith(ks_45753,"data-");
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return goog.string.startsWith(ks_45753,"aria-");
}
})())){
el.setAttribute(ks_45753,value);
} else {
(el[ks_45753] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__44713){
var map__44714 = p__44713;
var map__44714__$1 = cljs.core.__destructure_map(map__44714);
var props = map__44714__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__44714__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__44716 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44716,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44716,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44716,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__44720 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__44720,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__44720;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__44730 = arguments.length;
switch (G__44730) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__44746){
var vec__44748 = p__44746;
var seq__44749 = cljs.core.seq(vec__44748);
var first__44750 = cljs.core.first(seq__44749);
var seq__44749__$1 = cljs.core.next(seq__44749);
var nn = first__44750;
var first__44750__$1 = cljs.core.first(seq__44749__$1);
var seq__44749__$2 = cljs.core.next(seq__44749__$1);
var np = first__44750__$1;
var nc = seq__44749__$2;
var node = vec__44748;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__44759 = nn;
var G__44760 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__44759,G__44760) : create_fn.call(null,G__44759,G__44760));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__44762 = nn;
var G__44763 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__44762,G__44763) : create_fn.call(null,G__44762,G__44763));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__44770 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44770,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__44770,(1),null);
var seq__44775_45761 = cljs.core.seq(node_children);
var chunk__44776_45762 = null;
var count__44777_45763 = (0);
var i__44778_45764 = (0);
while(true){
if((i__44778_45764 < count__44777_45763)){
var child_struct_45765 = chunk__44776_45762.cljs$core$IIndexed$_nth$arity$2(null,i__44778_45764);
var children_45767 = shadow.dom.dom_node(child_struct_45765);
if(cljs.core.seq_QMARK_(children_45767)){
var seq__44821_45768 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_45767));
var chunk__44823_45769 = null;
var count__44824_45770 = (0);
var i__44825_45771 = (0);
while(true){
if((i__44825_45771 < count__44824_45770)){
var child_45772 = chunk__44823_45769.cljs$core$IIndexed$_nth$arity$2(null,i__44825_45771);
if(cljs.core.truth_(child_45772)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_45772);


var G__45773 = seq__44821_45768;
var G__45774 = chunk__44823_45769;
var G__45775 = count__44824_45770;
var G__45776 = (i__44825_45771 + (1));
seq__44821_45768 = G__45773;
chunk__44823_45769 = G__45774;
count__44824_45770 = G__45775;
i__44825_45771 = G__45776;
continue;
} else {
var G__45777 = seq__44821_45768;
var G__45778 = chunk__44823_45769;
var G__45779 = count__44824_45770;
var G__45780 = (i__44825_45771 + (1));
seq__44821_45768 = G__45777;
chunk__44823_45769 = G__45778;
count__44824_45770 = G__45779;
i__44825_45771 = G__45780;
continue;
}
} else {
var temp__5804__auto___45781 = cljs.core.seq(seq__44821_45768);
if(temp__5804__auto___45781){
var seq__44821_45782__$1 = temp__5804__auto___45781;
if(cljs.core.chunked_seq_QMARK_(seq__44821_45782__$1)){
var c__4679__auto___45783 = cljs.core.chunk_first(seq__44821_45782__$1);
var G__45784 = cljs.core.chunk_rest(seq__44821_45782__$1);
var G__45785 = c__4679__auto___45783;
var G__45786 = cljs.core.count(c__4679__auto___45783);
var G__45787 = (0);
seq__44821_45768 = G__45784;
chunk__44823_45769 = G__45785;
count__44824_45770 = G__45786;
i__44825_45771 = G__45787;
continue;
} else {
var child_45788 = cljs.core.first(seq__44821_45782__$1);
if(cljs.core.truth_(child_45788)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_45788);


var G__45789 = cljs.core.next(seq__44821_45782__$1);
var G__45790 = null;
var G__45791 = (0);
var G__45792 = (0);
seq__44821_45768 = G__45789;
chunk__44823_45769 = G__45790;
count__44824_45770 = G__45791;
i__44825_45771 = G__45792;
continue;
} else {
var G__45793 = cljs.core.next(seq__44821_45782__$1);
var G__45794 = null;
var G__45795 = (0);
var G__45796 = (0);
seq__44821_45768 = G__45793;
chunk__44823_45769 = G__45794;
count__44824_45770 = G__45795;
i__44825_45771 = G__45796;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_45767);
}


var G__45797 = seq__44775_45761;
var G__45798 = chunk__44776_45762;
var G__45799 = count__44777_45763;
var G__45800 = (i__44778_45764 + (1));
seq__44775_45761 = G__45797;
chunk__44776_45762 = G__45798;
count__44777_45763 = G__45799;
i__44778_45764 = G__45800;
continue;
} else {
var temp__5804__auto___45801 = cljs.core.seq(seq__44775_45761);
if(temp__5804__auto___45801){
var seq__44775_45802__$1 = temp__5804__auto___45801;
if(cljs.core.chunked_seq_QMARK_(seq__44775_45802__$1)){
var c__4679__auto___45803 = cljs.core.chunk_first(seq__44775_45802__$1);
var G__45804 = cljs.core.chunk_rest(seq__44775_45802__$1);
var G__45805 = c__4679__auto___45803;
var G__45806 = cljs.core.count(c__4679__auto___45803);
var G__45807 = (0);
seq__44775_45761 = G__45804;
chunk__44776_45762 = G__45805;
count__44777_45763 = G__45806;
i__44778_45764 = G__45807;
continue;
} else {
var child_struct_45809 = cljs.core.first(seq__44775_45802__$1);
var children_45810 = shadow.dom.dom_node(child_struct_45809);
if(cljs.core.seq_QMARK_(children_45810)){
var seq__44835_45812 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_45810));
var chunk__44837_45813 = null;
var count__44838_45814 = (0);
var i__44839_45815 = (0);
while(true){
if((i__44839_45815 < count__44838_45814)){
var child_45816 = chunk__44837_45813.cljs$core$IIndexed$_nth$arity$2(null,i__44839_45815);
if(cljs.core.truth_(child_45816)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_45816);


var G__45817 = seq__44835_45812;
var G__45818 = chunk__44837_45813;
var G__45819 = count__44838_45814;
var G__45820 = (i__44839_45815 + (1));
seq__44835_45812 = G__45817;
chunk__44837_45813 = G__45818;
count__44838_45814 = G__45819;
i__44839_45815 = G__45820;
continue;
} else {
var G__45821 = seq__44835_45812;
var G__45822 = chunk__44837_45813;
var G__45823 = count__44838_45814;
var G__45824 = (i__44839_45815 + (1));
seq__44835_45812 = G__45821;
chunk__44837_45813 = G__45822;
count__44838_45814 = G__45823;
i__44839_45815 = G__45824;
continue;
}
} else {
var temp__5804__auto___45825__$1 = cljs.core.seq(seq__44835_45812);
if(temp__5804__auto___45825__$1){
var seq__44835_45830__$1 = temp__5804__auto___45825__$1;
if(cljs.core.chunked_seq_QMARK_(seq__44835_45830__$1)){
var c__4679__auto___45831 = cljs.core.chunk_first(seq__44835_45830__$1);
var G__45832 = cljs.core.chunk_rest(seq__44835_45830__$1);
var G__45833 = c__4679__auto___45831;
var G__45834 = cljs.core.count(c__4679__auto___45831);
var G__45835 = (0);
seq__44835_45812 = G__45832;
chunk__44837_45813 = G__45833;
count__44838_45814 = G__45834;
i__44839_45815 = G__45835;
continue;
} else {
var child_45836 = cljs.core.first(seq__44835_45830__$1);
if(cljs.core.truth_(child_45836)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_45836);


var G__45837 = cljs.core.next(seq__44835_45830__$1);
var G__45838 = null;
var G__45839 = (0);
var G__45840 = (0);
seq__44835_45812 = G__45837;
chunk__44837_45813 = G__45838;
count__44838_45814 = G__45839;
i__44839_45815 = G__45840;
continue;
} else {
var G__45841 = cljs.core.next(seq__44835_45830__$1);
var G__45842 = null;
var G__45843 = (0);
var G__45844 = (0);
seq__44835_45812 = G__45841;
chunk__44837_45813 = G__45842;
count__44838_45814 = G__45843;
i__44839_45815 = G__45844;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_45810);
}


var G__45845 = cljs.core.next(seq__44775_45802__$1);
var G__45846 = null;
var G__45847 = (0);
var G__45848 = (0);
seq__44775_45761 = G__45845;
chunk__44776_45762 = G__45846;
count__44777_45763 = G__45847;
i__44778_45764 = G__45848;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__44867 = cljs.core.seq(node);
var chunk__44868 = null;
var count__44869 = (0);
var i__44870 = (0);
while(true){
if((i__44870 < count__44869)){
var n = chunk__44868.cljs$core$IIndexed$_nth$arity$2(null,i__44870);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__45853 = seq__44867;
var G__45854 = chunk__44868;
var G__45855 = count__44869;
var G__45856 = (i__44870 + (1));
seq__44867 = G__45853;
chunk__44868 = G__45854;
count__44869 = G__45855;
i__44870 = G__45856;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__44867);
if(temp__5804__auto__){
var seq__44867__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__44867__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__44867__$1);
var G__45857 = cljs.core.chunk_rest(seq__44867__$1);
var G__45858 = c__4679__auto__;
var G__45859 = cljs.core.count(c__4679__auto__);
var G__45860 = (0);
seq__44867 = G__45857;
chunk__44868 = G__45858;
count__44869 = G__45859;
i__44870 = G__45860;
continue;
} else {
var n = cljs.core.first(seq__44867__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__45861 = cljs.core.next(seq__44867__$1);
var G__45862 = null;
var G__45863 = (0);
var G__45864 = (0);
seq__44867 = G__45861;
chunk__44868 = G__45862;
count__44869 = G__45863;
i__44870 = G__45864;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__44907 = arguments.length;
switch (G__44907) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__44923 = arguments.length;
switch (G__44923) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__44940 = arguments.length;
switch (G__44940) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4253__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4253__auto__)){
return or__4253__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4870__auto__ = [];
var len__4864__auto___45870 = arguments.length;
var i__4865__auto___45871 = (0);
while(true){
if((i__4865__auto___45871 < len__4864__auto___45870)){
args__4870__auto__.push((arguments[i__4865__auto___45871]));

var G__45872 = (i__4865__auto___45871 + (1));
i__4865__auto___45871 = G__45872;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((0) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4871__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__44999_45876 = cljs.core.seq(nodes);
var chunk__45000_45877 = null;
var count__45001_45878 = (0);
var i__45002_45879 = (0);
while(true){
if((i__45002_45879 < count__45001_45878)){
var node_45880 = chunk__45000_45877.cljs$core$IIndexed$_nth$arity$2(null,i__45002_45879);
fragment.appendChild(shadow.dom._to_dom(node_45880));


var G__45881 = seq__44999_45876;
var G__45882 = chunk__45000_45877;
var G__45883 = count__45001_45878;
var G__45884 = (i__45002_45879 + (1));
seq__44999_45876 = G__45881;
chunk__45000_45877 = G__45882;
count__45001_45878 = G__45883;
i__45002_45879 = G__45884;
continue;
} else {
var temp__5804__auto___45885 = cljs.core.seq(seq__44999_45876);
if(temp__5804__auto___45885){
var seq__44999_45886__$1 = temp__5804__auto___45885;
if(cljs.core.chunked_seq_QMARK_(seq__44999_45886__$1)){
var c__4679__auto___45887 = cljs.core.chunk_first(seq__44999_45886__$1);
var G__45888 = cljs.core.chunk_rest(seq__44999_45886__$1);
var G__45889 = c__4679__auto___45887;
var G__45890 = cljs.core.count(c__4679__auto___45887);
var G__45891 = (0);
seq__44999_45876 = G__45888;
chunk__45000_45877 = G__45889;
count__45001_45878 = G__45890;
i__45002_45879 = G__45891;
continue;
} else {
var node_45892 = cljs.core.first(seq__44999_45886__$1);
fragment.appendChild(shadow.dom._to_dom(node_45892));


var G__45893 = cljs.core.next(seq__44999_45886__$1);
var G__45894 = null;
var G__45895 = (0);
var G__45896 = (0);
seq__44999_45876 = G__45893;
chunk__45000_45877 = G__45894;
count__45001_45878 = G__45895;
i__45002_45879 = G__45896;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq44988){
var self__4852__auto__ = this;
return self__4852__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq44988));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__45031_45897 = cljs.core.seq(scripts);
var chunk__45032_45898 = null;
var count__45033_45899 = (0);
var i__45034_45900 = (0);
while(true){
if((i__45034_45900 < count__45033_45899)){
var vec__45078_45901 = chunk__45032_45898.cljs$core$IIndexed$_nth$arity$2(null,i__45034_45900);
var script_tag_45902 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45078_45901,(0),null);
var script_body_45903 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45078_45901,(1),null);
eval(script_body_45903);


var G__45904 = seq__45031_45897;
var G__45905 = chunk__45032_45898;
var G__45906 = count__45033_45899;
var G__45907 = (i__45034_45900 + (1));
seq__45031_45897 = G__45904;
chunk__45032_45898 = G__45905;
count__45033_45899 = G__45906;
i__45034_45900 = G__45907;
continue;
} else {
var temp__5804__auto___45908 = cljs.core.seq(seq__45031_45897);
if(temp__5804__auto___45908){
var seq__45031_45909__$1 = temp__5804__auto___45908;
if(cljs.core.chunked_seq_QMARK_(seq__45031_45909__$1)){
var c__4679__auto___45910 = cljs.core.chunk_first(seq__45031_45909__$1);
var G__45911 = cljs.core.chunk_rest(seq__45031_45909__$1);
var G__45912 = c__4679__auto___45910;
var G__45913 = cljs.core.count(c__4679__auto___45910);
var G__45914 = (0);
seq__45031_45897 = G__45911;
chunk__45032_45898 = G__45912;
count__45033_45899 = G__45913;
i__45034_45900 = G__45914;
continue;
} else {
var vec__45090_45915 = cljs.core.first(seq__45031_45909__$1);
var script_tag_45916 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45090_45915,(0),null);
var script_body_45917 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45090_45915,(1),null);
eval(script_body_45917);


var G__45918 = cljs.core.next(seq__45031_45909__$1);
var G__45919 = null;
var G__45920 = (0);
var G__45921 = (0);
seq__45031_45897 = G__45918;
chunk__45032_45898 = G__45919;
count__45033_45899 = G__45920;
i__45034_45900 = G__45921;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__45103){
var vec__45104 = p__45103;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45104,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45104,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__45152 = arguments.length;
switch (G__45152) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__45206 = cljs.core.seq(style_keys);
var chunk__45207 = null;
var count__45208 = (0);
var i__45209 = (0);
while(true){
if((i__45209 < count__45208)){
var it = chunk__45207.cljs$core$IIndexed$_nth$arity$2(null,i__45209);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__45926 = seq__45206;
var G__45927 = chunk__45207;
var G__45928 = count__45208;
var G__45929 = (i__45209 + (1));
seq__45206 = G__45926;
chunk__45207 = G__45927;
count__45208 = G__45928;
i__45209 = G__45929;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__45206);
if(temp__5804__auto__){
var seq__45206__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__45206__$1)){
var c__4679__auto__ = cljs.core.chunk_first(seq__45206__$1);
var G__45930 = cljs.core.chunk_rest(seq__45206__$1);
var G__45931 = c__4679__auto__;
var G__45932 = cljs.core.count(c__4679__auto__);
var G__45933 = (0);
seq__45206 = G__45930;
chunk__45207 = G__45931;
count__45208 = G__45932;
i__45209 = G__45933;
continue;
} else {
var it = cljs.core.first(seq__45206__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__45934 = cljs.core.next(seq__45206__$1);
var G__45935 = null;
var G__45936 = (0);
var G__45937 = (0);
seq__45206 = G__45934;
chunk__45207 = G__45935;
count__45208 = G__45936;
i__45209 = G__45937;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4502__auto__,k__4503__auto__){
var self__ = this;
var this__4502__auto____$1 = this;
return this__4502__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4503__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4504__auto__,k45229,else__4505__auto__){
var self__ = this;
var this__4504__auto____$1 = this;
var G__45240 = k45229;
var G__45240__$1 = (((G__45240 instanceof cljs.core.Keyword))?G__45240.fqn:null);
switch (G__45240__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k45229,else__4505__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4522__auto__,f__4523__auto__,init__4524__auto__){
var self__ = this;
var this__4522__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4525__auto__,p__45254){
var vec__45255 = p__45254;
var k__4526__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45255,(0),null);
var v__4527__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45255,(1),null);
return (f__4523__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4523__auto__.cljs$core$IFn$_invoke$arity$3(ret__4525__auto__,k__4526__auto__,v__4527__auto__) : f__4523__auto__.call(null,ret__4525__auto__,k__4526__auto__,v__4527__auto__));
}),init__4524__auto__,this__4522__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4517__auto__,writer__4518__auto__,opts__4519__auto__){
var self__ = this;
var this__4517__auto____$1 = this;
var pr_pair__4520__auto__ = (function (keyval__4521__auto__){
return cljs.core.pr_sequential_writer(writer__4518__auto__,cljs.core.pr_writer,""," ","",opts__4519__auto__,keyval__4521__auto__);
});
return cljs.core.pr_sequential_writer(writer__4518__auto__,pr_pair__4520__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4519__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__45228){
var self__ = this;
var G__45228__$1 = this;
return (new cljs.core.RecordIter((0),G__45228__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4500__auto__){
var self__ = this;
var this__4500__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4497__auto__){
var self__ = this;
var this__4497__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4506__auto__){
var self__ = this;
var this__4506__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4498__auto__){
var self__ = this;
var this__4498__auto____$1 = this;
var h__4360__auto__ = self__.__hash;
if((!((h__4360__auto__ == null)))){
return h__4360__auto__;
} else {
var h__4360__auto____$1 = (function (coll__4499__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4499__auto__));
})(this__4498__auto____$1);
(self__.__hash = h__4360__auto____$1);

return h__4360__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this45230,other45231){
var self__ = this;
var this45230__$1 = this;
return (((!((other45231 == null)))) && ((((this45230__$1.constructor === other45231.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this45230__$1.x,other45231.x)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this45230__$1.y,other45231.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this45230__$1.__extmap,other45231.__extmap)))))))));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4512__auto__,k__4513__auto__){
var self__ = this;
var this__4512__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4513__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4512__auto____$1),self__.__meta),k__4513__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4513__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__4509__auto__,k45229){
var self__ = this;
var this__4509__auto____$1 = this;
var G__45278 = k45229;
var G__45278__$1 = (((G__45278 instanceof cljs.core.Keyword))?G__45278.fqn:null);
switch (G__45278__$1) {
case "x":
case "y":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k45229);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4510__auto__,k__4511__auto__,G__45228){
var self__ = this;
var this__4510__auto____$1 = this;
var pred__45283 = cljs.core.keyword_identical_QMARK_;
var expr__45284 = k__4511__auto__;
if(cljs.core.truth_((pred__45283.cljs$core$IFn$_invoke$arity$2 ? pred__45283.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__45284) : pred__45283.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__45284)))){
return (new shadow.dom.Coordinate(G__45228,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__45283.cljs$core$IFn$_invoke$arity$2 ? pred__45283.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__45284) : pred__45283.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__45284)))){
return (new shadow.dom.Coordinate(self__.x,G__45228,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4511__auto__,G__45228),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4515__auto__){
var self__ = this;
var this__4515__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4501__auto__,G__45228){
var self__ = this;
var this__4501__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__45228,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4507__auto__,entry__4508__auto__){
var self__ = this;
var this__4507__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4508__auto__)){
return this__4507__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4508__auto__,(0)),cljs.core._nth(entry__4508__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4507__auto____$1,entry__4508__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4546__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4546__auto__,writer__4547__auto__){
return cljs.core._write(writer__4547__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__45233){
var extmap__4542__auto__ = (function (){var G__45310 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__45233,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__45233)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__45310);
} else {
return G__45310;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__45233),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__45233),null,cljs.core.not_empty(extmap__4542__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4502__auto__,k__4503__auto__){
var self__ = this;
var this__4502__auto____$1 = this;
return this__4502__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4503__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4504__auto__,k45327,else__4505__auto__){
var self__ = this;
var this__4504__auto____$1 = this;
var G__45340 = k45327;
var G__45340__$1 = (((G__45340 instanceof cljs.core.Keyword))?G__45340.fqn:null);
switch (G__45340__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k45327,else__4505__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4522__auto__,f__4523__auto__,init__4524__auto__){
var self__ = this;
var this__4522__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4525__auto__,p__45347){
var vec__45349 = p__45347;
var k__4526__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45349,(0),null);
var v__4527__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45349,(1),null);
return (f__4523__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4523__auto__.cljs$core$IFn$_invoke$arity$3(ret__4525__auto__,k__4526__auto__,v__4527__auto__) : f__4523__auto__.call(null,ret__4525__auto__,k__4526__auto__,v__4527__auto__));
}),init__4524__auto__,this__4522__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4517__auto__,writer__4518__auto__,opts__4519__auto__){
var self__ = this;
var this__4517__auto____$1 = this;
var pr_pair__4520__auto__ = (function (keyval__4521__auto__){
return cljs.core.pr_sequential_writer(writer__4518__auto__,cljs.core.pr_writer,""," ","",opts__4519__auto__,keyval__4521__auto__);
});
return cljs.core.pr_sequential_writer(writer__4518__auto__,pr_pair__4520__auto__,"#shadow.dom.Size{",", ","}",opts__4519__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__45326){
var self__ = this;
var G__45326__$1 = this;
return (new cljs.core.RecordIter((0),G__45326__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4500__auto__){
var self__ = this;
var this__4500__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4497__auto__){
var self__ = this;
var this__4497__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4506__auto__){
var self__ = this;
var this__4506__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4498__auto__){
var self__ = this;
var this__4498__auto____$1 = this;
var h__4360__auto__ = self__.__hash;
if((!((h__4360__auto__ == null)))){
return h__4360__auto__;
} else {
var h__4360__auto____$1 = (function (coll__4499__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4499__auto__));
})(this__4498__auto____$1);
(self__.__hash = h__4360__auto____$1);

return h__4360__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this45328,other45329){
var self__ = this;
var this45328__$1 = this;
return (((!((other45329 == null)))) && ((((this45328__$1.constructor === other45329.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this45328__$1.w,other45329.w)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this45328__$1.h,other45329.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this45328__$1.__extmap,other45329.__extmap)))))))));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4512__auto__,k__4513__auto__){
var self__ = this;
var this__4512__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4513__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4512__auto____$1),self__.__meta),k__4513__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4513__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__4509__auto__,k45327){
var self__ = this;
var this__4509__auto____$1 = this;
var G__45381 = k45327;
var G__45381__$1 = (((G__45381 instanceof cljs.core.Keyword))?G__45381.fqn:null);
switch (G__45381__$1) {
case "w":
case "h":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k45327);

}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4510__auto__,k__4511__auto__,G__45326){
var self__ = this;
var this__4510__auto____$1 = this;
var pred__45386 = cljs.core.keyword_identical_QMARK_;
var expr__45387 = k__4511__auto__;
if(cljs.core.truth_((pred__45386.cljs$core$IFn$_invoke$arity$2 ? pred__45386.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__45387) : pred__45386.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__45387)))){
return (new shadow.dom.Size(G__45326,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__45386.cljs$core$IFn$_invoke$arity$2 ? pred__45386.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__45387) : pred__45386.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__45387)))){
return (new shadow.dom.Size(self__.w,G__45326,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4511__auto__,G__45326),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4515__auto__){
var self__ = this;
var this__4515__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4501__auto__,G__45326){
var self__ = this;
var this__4501__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__45326,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4507__auto__,entry__4508__auto__){
var self__ = this;
var this__4507__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4508__auto__)){
return this__4507__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4508__auto__,(0)),cljs.core._nth(entry__4508__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4507__auto____$1,entry__4508__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4546__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4546__auto__,writer__4547__auto__){
return cljs.core._write(writer__4547__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__45332){
var extmap__4542__auto__ = (function (){var G__45427 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__45332,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__45332)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__45427);
} else {
return G__45427;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__45332),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__45332),null,cljs.core.not_empty(extmap__4542__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4738__auto__ = opts;
var l__4739__auto__ = a__4738__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4739__auto__)){
var G__45973 = (i + (1));
var G__45974 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__45973;
ret = G__45974;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__45451){
var vec__45452 = p__45451;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45452,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45452,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__45459 = arguments.length;
switch (G__45459) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5802__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5802__auto__)){
var child = temp__5802__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__45979 = ps;
var G__45980 = (i + (1));
el__$1 = G__45979;
i = G__45980;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__45496 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45496,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45496,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45496,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__45500_45984 = cljs.core.seq(props);
var chunk__45501_45985 = null;
var count__45502_45986 = (0);
var i__45503_45987 = (0);
while(true){
if((i__45503_45987 < count__45502_45986)){
var vec__45519_45988 = chunk__45501_45985.cljs$core$IIndexed$_nth$arity$2(null,i__45503_45987);
var k_45989 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45519_45988,(0),null);
var v_45990 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45519_45988,(1),null);
el.setAttributeNS((function (){var temp__5804__auto__ = cljs.core.namespace(k_45989);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_45989),v_45990);


var G__45991 = seq__45500_45984;
var G__45992 = chunk__45501_45985;
var G__45993 = count__45502_45986;
var G__45994 = (i__45503_45987 + (1));
seq__45500_45984 = G__45991;
chunk__45501_45985 = G__45992;
count__45502_45986 = G__45993;
i__45503_45987 = G__45994;
continue;
} else {
var temp__5804__auto___45995 = cljs.core.seq(seq__45500_45984);
if(temp__5804__auto___45995){
var seq__45500_45996__$1 = temp__5804__auto___45995;
if(cljs.core.chunked_seq_QMARK_(seq__45500_45996__$1)){
var c__4679__auto___45997 = cljs.core.chunk_first(seq__45500_45996__$1);
var G__45999 = cljs.core.chunk_rest(seq__45500_45996__$1);
var G__46000 = c__4679__auto___45997;
var G__46001 = cljs.core.count(c__4679__auto___45997);
var G__46002 = (0);
seq__45500_45984 = G__45999;
chunk__45501_45985 = G__46000;
count__45502_45986 = G__46001;
i__45503_45987 = G__46002;
continue;
} else {
var vec__45523_46003 = cljs.core.first(seq__45500_45996__$1);
var k_46004 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45523_46003,(0),null);
var v_46005 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45523_46003,(1),null);
el.setAttributeNS((function (){var temp__5804__auto____$1 = cljs.core.namespace(k_46004);
if(cljs.core.truth_(temp__5804__auto____$1)){
var ns = temp__5804__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_46004),v_46005);


var G__46007 = cljs.core.next(seq__45500_45996__$1);
var G__46008 = null;
var G__46009 = (0);
var G__46010 = (0);
seq__45500_45984 = G__46007;
chunk__45501_45985 = G__46008;
count__45502_45986 = G__46009;
i__45503_45987 = G__46010;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__45527 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45527,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__45527,(1),null);
var seq__45530_46014 = cljs.core.seq(node_children);
var chunk__45532_46015 = null;
var count__45533_46016 = (0);
var i__45534_46017 = (0);
while(true){
if((i__45534_46017 < count__45533_46016)){
var child_struct_46018 = chunk__45532_46015.cljs$core$IIndexed$_nth$arity$2(null,i__45534_46017);
if((!((child_struct_46018 == null)))){
if(typeof child_struct_46018 === 'string'){
var text_46020 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_46020),child_struct_46018].join(''));
} else {
var children_46022 = shadow.dom.svg_node(child_struct_46018);
if(cljs.core.seq_QMARK_(children_46022)){
var seq__45588_46023 = cljs.core.seq(children_46022);
var chunk__45590_46024 = null;
var count__45591_46025 = (0);
var i__45592_46026 = (0);
while(true){
if((i__45592_46026 < count__45591_46025)){
var child_46027 = chunk__45590_46024.cljs$core$IIndexed$_nth$arity$2(null,i__45592_46026);
if(cljs.core.truth_(child_46027)){
node.appendChild(child_46027);


var G__46028 = seq__45588_46023;
var G__46029 = chunk__45590_46024;
var G__46030 = count__45591_46025;
var G__46031 = (i__45592_46026 + (1));
seq__45588_46023 = G__46028;
chunk__45590_46024 = G__46029;
count__45591_46025 = G__46030;
i__45592_46026 = G__46031;
continue;
} else {
var G__46032 = seq__45588_46023;
var G__46033 = chunk__45590_46024;
var G__46034 = count__45591_46025;
var G__46035 = (i__45592_46026 + (1));
seq__45588_46023 = G__46032;
chunk__45590_46024 = G__46033;
count__45591_46025 = G__46034;
i__45592_46026 = G__46035;
continue;
}
} else {
var temp__5804__auto___46036 = cljs.core.seq(seq__45588_46023);
if(temp__5804__auto___46036){
var seq__45588_46037__$1 = temp__5804__auto___46036;
if(cljs.core.chunked_seq_QMARK_(seq__45588_46037__$1)){
var c__4679__auto___46038 = cljs.core.chunk_first(seq__45588_46037__$1);
var G__46039 = cljs.core.chunk_rest(seq__45588_46037__$1);
var G__46040 = c__4679__auto___46038;
var G__46041 = cljs.core.count(c__4679__auto___46038);
var G__46042 = (0);
seq__45588_46023 = G__46039;
chunk__45590_46024 = G__46040;
count__45591_46025 = G__46041;
i__45592_46026 = G__46042;
continue;
} else {
var child_46043 = cljs.core.first(seq__45588_46037__$1);
if(cljs.core.truth_(child_46043)){
node.appendChild(child_46043);


var G__46044 = cljs.core.next(seq__45588_46037__$1);
var G__46045 = null;
var G__46046 = (0);
var G__46047 = (0);
seq__45588_46023 = G__46044;
chunk__45590_46024 = G__46045;
count__45591_46025 = G__46046;
i__45592_46026 = G__46047;
continue;
} else {
var G__46048 = cljs.core.next(seq__45588_46037__$1);
var G__46049 = null;
var G__46050 = (0);
var G__46051 = (0);
seq__45588_46023 = G__46048;
chunk__45590_46024 = G__46049;
count__45591_46025 = G__46050;
i__45592_46026 = G__46051;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_46022);
}
}


var G__46052 = seq__45530_46014;
var G__46053 = chunk__45532_46015;
var G__46054 = count__45533_46016;
var G__46055 = (i__45534_46017 + (1));
seq__45530_46014 = G__46052;
chunk__45532_46015 = G__46053;
count__45533_46016 = G__46054;
i__45534_46017 = G__46055;
continue;
} else {
var G__46056 = seq__45530_46014;
var G__46057 = chunk__45532_46015;
var G__46058 = count__45533_46016;
var G__46059 = (i__45534_46017 + (1));
seq__45530_46014 = G__46056;
chunk__45532_46015 = G__46057;
count__45533_46016 = G__46058;
i__45534_46017 = G__46059;
continue;
}
} else {
var temp__5804__auto___46060 = cljs.core.seq(seq__45530_46014);
if(temp__5804__auto___46060){
var seq__45530_46061__$1 = temp__5804__auto___46060;
if(cljs.core.chunked_seq_QMARK_(seq__45530_46061__$1)){
var c__4679__auto___46062 = cljs.core.chunk_first(seq__45530_46061__$1);
var G__46063 = cljs.core.chunk_rest(seq__45530_46061__$1);
var G__46064 = c__4679__auto___46062;
var G__46065 = cljs.core.count(c__4679__auto___46062);
var G__46066 = (0);
seq__45530_46014 = G__46063;
chunk__45532_46015 = G__46064;
count__45533_46016 = G__46065;
i__45534_46017 = G__46066;
continue;
} else {
var child_struct_46067 = cljs.core.first(seq__45530_46061__$1);
if((!((child_struct_46067 == null)))){
if(typeof child_struct_46067 === 'string'){
var text_46068 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_46068),child_struct_46067].join(''));
} else {
var children_46073 = shadow.dom.svg_node(child_struct_46067);
if(cljs.core.seq_QMARK_(children_46073)){
var seq__45608_46074 = cljs.core.seq(children_46073);
var chunk__45610_46075 = null;
var count__45611_46076 = (0);
var i__45612_46077 = (0);
while(true){
if((i__45612_46077 < count__45611_46076)){
var child_46081 = chunk__45610_46075.cljs$core$IIndexed$_nth$arity$2(null,i__45612_46077);
if(cljs.core.truth_(child_46081)){
node.appendChild(child_46081);


var G__46082 = seq__45608_46074;
var G__46083 = chunk__45610_46075;
var G__46084 = count__45611_46076;
var G__46085 = (i__45612_46077 + (1));
seq__45608_46074 = G__46082;
chunk__45610_46075 = G__46083;
count__45611_46076 = G__46084;
i__45612_46077 = G__46085;
continue;
} else {
var G__46086 = seq__45608_46074;
var G__46087 = chunk__45610_46075;
var G__46088 = count__45611_46076;
var G__46089 = (i__45612_46077 + (1));
seq__45608_46074 = G__46086;
chunk__45610_46075 = G__46087;
count__45611_46076 = G__46088;
i__45612_46077 = G__46089;
continue;
}
} else {
var temp__5804__auto___46090__$1 = cljs.core.seq(seq__45608_46074);
if(temp__5804__auto___46090__$1){
var seq__45608_46091__$1 = temp__5804__auto___46090__$1;
if(cljs.core.chunked_seq_QMARK_(seq__45608_46091__$1)){
var c__4679__auto___46092 = cljs.core.chunk_first(seq__45608_46091__$1);
var G__46093 = cljs.core.chunk_rest(seq__45608_46091__$1);
var G__46094 = c__4679__auto___46092;
var G__46095 = cljs.core.count(c__4679__auto___46092);
var G__46096 = (0);
seq__45608_46074 = G__46093;
chunk__45610_46075 = G__46094;
count__45611_46076 = G__46095;
i__45612_46077 = G__46096;
continue;
} else {
var child_46097 = cljs.core.first(seq__45608_46091__$1);
if(cljs.core.truth_(child_46097)){
node.appendChild(child_46097);


var G__46098 = cljs.core.next(seq__45608_46091__$1);
var G__46099 = null;
var G__46100 = (0);
var G__46101 = (0);
seq__45608_46074 = G__46098;
chunk__45610_46075 = G__46099;
count__45611_46076 = G__46100;
i__45612_46077 = G__46101;
continue;
} else {
var G__46102 = cljs.core.next(seq__45608_46091__$1);
var G__46103 = null;
var G__46104 = (0);
var G__46105 = (0);
seq__45608_46074 = G__46102;
chunk__45610_46075 = G__46103;
count__45611_46076 = G__46104;
i__45612_46077 = G__46105;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_46073);
}
}


var G__46106 = cljs.core.next(seq__45530_46061__$1);
var G__46107 = null;
var G__46108 = (0);
var G__46109 = (0);
seq__45530_46014 = G__46106;
chunk__45532_46015 = G__46107;
count__45533_46016 = G__46108;
i__45534_46017 = G__46109;
continue;
} else {
var G__46110 = cljs.core.next(seq__45530_46061__$1);
var G__46111 = null;
var G__46112 = (0);
var G__46113 = (0);
seq__45530_46014 = G__46110;
chunk__45532_46015 = G__46111;
count__45533_46016 = G__46112;
i__45534_46017 = G__46113;
continue;
}
}
} else {
}
}
break;
}

return node;
});
(shadow.dom.SVGElement["string"] = true);

(shadow.dom._to_svg["string"] = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

(shadow.dom.SVGElement["null"] = true);

(shadow.dom._to_svg["null"] = (function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4870__auto__ = [];
var len__4864__auto___46117 = arguments.length;
var i__4865__auto___46118 = (0);
while(true){
if((i__4865__auto___46118 < len__4864__auto___46117)){
args__4870__auto__.push((arguments[i__4865__auto___46118]));

var G__46119 = (i__4865__auto___46118 + (1));
i__4865__auto___46118 = G__46119;
continue;
} else {
}
break;
}

var argseq__4871__auto__ = ((((1) < args__4870__auto__.length))?(new cljs.core.IndexedSeq(args__4870__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4871__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq45620){
var G__45621 = cljs.core.first(seq45620);
var seq45620__$1 = cljs.core.next(seq45620);
var self__4851__auto__ = this;
return self__4851__auto__.cljs$core$IFn$_invoke$arity$variadic(G__45621,seq45620__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__45628 = arguments.length;
switch (G__45628) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__4251__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4251__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4251__auto__;
}
})())){
var c__41555__auto___46131 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__41556__auto__ = (function (){var switch__41222__auto__ = (function (state_45652){
var state_val_45653 = (state_45652[(1)]);
if((state_val_45653 === (1))){
var state_45652__$1 = state_45652;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_45652__$1,(2),once_or_cleanup);
} else {
if((state_val_45653 === (2))){
var inst_45649 = (state_45652[(2)]);
var inst_45650 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_45652__$1 = (function (){var statearr_45660 = state_45652;
(statearr_45660[(7)] = inst_45649);

return statearr_45660;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_45652__$1,inst_45650);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__41223__auto__ = null;
var shadow$dom$state_machine__41223__auto____0 = (function (){
var statearr_45662 = [null,null,null,null,null,null,null,null];
(statearr_45662[(0)] = shadow$dom$state_machine__41223__auto__);

(statearr_45662[(1)] = (1));

return statearr_45662;
});
var shadow$dom$state_machine__41223__auto____1 = (function (state_45652){
while(true){
var ret_value__41224__auto__ = (function (){try{while(true){
var result__41225__auto__ = switch__41222__auto__(state_45652);
if(cljs.core.keyword_identical_QMARK_(result__41225__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__41225__auto__;
}
break;
}
}catch (e45664){var ex__41226__auto__ = e45664;
var statearr_45665_46136 = state_45652;
(statearr_45665_46136[(2)] = ex__41226__auto__);


if(cljs.core.seq((state_45652[(4)]))){
var statearr_45666_46137 = state_45652;
(statearr_45666_46137[(1)] = cljs.core.first((state_45652[(4)])));

} else {
throw ex__41226__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__41224__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__46138 = state_45652;
state_45652 = G__46138;
continue;
} else {
return ret_value__41224__auto__;
}
break;
}
});
shadow$dom$state_machine__41223__auto__ = function(state_45652){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__41223__auto____0.call(this);
case 1:
return shadow$dom$state_machine__41223__auto____1.call(this,state_45652);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__41223__auto____0;
shadow$dom$state_machine__41223__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__41223__auto____1;
return shadow$dom$state_machine__41223__auto__;
})()
})();
var state__41557__auto__ = (function (){var statearr_45667 = f__41556__auto__();
(statearr_45667[(6)] = c__41555__auto___46131);

return statearr_45667;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__41557__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
