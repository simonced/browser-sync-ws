(defproject browser-sync-ws "0.1.0-SNAPSHOT"
  :description "Websocket server + client to sync different browsers' actions"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  ;; libs for the application
  :dependencies [[org.clojure/clojure "1.11.0"]
                 ; routing
                 [compojure "1.6.2"]
                 ; http inners and stuff
                 [ring/ring-defaults "0.3.3"]
                 ; http server + websocket stuff
                 [http-kit "2.5.3"]]
  :source-paths ["src/clj" "src/cljs"]
  ;; resource-paths is set here so lein uberjar will include the resources as well
  :resource-paths ["resources"]
  ;; :main defines the class to witch run the -main method with `lein run`
  :main browser-sync-ws.core
  ;; :aot is used for jar or uberjar
  :aot [browser-sync-ws.core]
  :repl-options {:init-ns browser-sync-ws.core}
  ;; dev only depedencies
  :profiles {:dev {:dependencies [[thheller/shadow-cljs "2.17.8"]]}}
  ;; alias to watch or compile the client done in cljs
  :aliases {"client-watch" ["run" "-m" "shadow.cljs.devtools.cli" "watch" "app"]
            "client-compile" ["run" "-m" "shadow.cljs.devtools.cli" "compile" "app"]})

