(ns browser-sync-ws.server
  (:require [org.httpkit.server :as srv]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer :all]
            [ring.middleware.resource :as rsc]
            ))

(defonce server (atom nil))

(defonce ws-clients (atom #{}))

(defn route-top [_req]
  ;; http answer
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    "<!DOCTYPE html><html><body><h1>hello HTTP<h1></body></html>"})

(defn route-mimic [req]
  (srv/as-channel req
                  {:on-open    (fn [ch]
                                 (println "open:" ch)
                                 (swap! ws-clients conj ch))
                   :on-close   (fn [ch _status]
                                 (println "closed:" ch)
                                 (swap! ws-clients disj ch))
                   :on-receive (fn [ch msg]
                                 (println "received:" msg "from:" ch)
                                 (dorun (map #(srv/send! % msg) (disj @ws-clients ch)))
                                 )
                   }))

(defroutes app-routes
           ;; basic top page (nothing special)
           (GET "/" [] route-top)
           ;; entry point to my ws sync api
           (GET "/mimic" [] #'route-mimic)
           (route/not-found "Error, page not found!"))

(defn stop-server []
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@server :timeout 100)
    (reset! server nil)
    (println "server stopped")))

(defn start-server
  "Start the server at a specified port."
  [port]
  ;; The #' is useful when you want to hot-reload code
  ;; You may want to take a look: https://github.com/clojure/tools.namespace
  ;; and https://http-kit.github.io/migration.html#reload
  (reset! server
          (srv/run-server
            (-> app-routes
                (rsc/wrap-resource ,,, "public")            ; service static files
                (wrap-defaults ,,, site-defaults)           ; getting get parameters etc...
                )
            {:port port}))
  (println "server running on port:" port))


(comment
  ;; start server
  (start-server 8080)

  ;; then stop it when finished
  (stop-server)
  )