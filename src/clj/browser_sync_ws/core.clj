(ns browser-sync-ws.core
  (:require [browser-sync-ws.server :as server])
  (:gen-class))

(defn -main
  "Start the server from command line."
  ([] (-main "8080"))

  ([port]
   (println (format "Add to your pages OR with tamper monkey (adapt to your IP):
  <script src=\"//aaa.bbb.ccc.ddd:%s/js/element-xpath.js\"></script>
  <script src=\"//aaa.bbb.ccc.ddd:%s/js/client.js\"></script>
  <script>window.browserSyncInit(\"ws://aaa.bbb.ccc.ddd:%s/mimic\");</script>" port port port))

   ;; start server
   (server/start-server (read-string port))

   ;; hook for stoping server
   (-> (Runtime/getRuntime)
       (.addShutdownHook
         (Thread. ^Runnable (fn []
                              (println "\nStopping server...")
                              (server/stop-server))))))
  )
