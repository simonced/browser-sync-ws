(ns browser-sync-ws.client
  (:require [cljs.reader :refer [read-string]]))

(defonce socket (atom nil))

(defn action-sender [e]
  "Capture user actions."
  (let [target (.-target e)
        type (.-type e)
        xpath (js/window.getElementXpath target)
        my-json {:xpath xpath :event type}]
    (when (.-isTrusted e)
      (case type
        "click" (.send @socket my-json)
        "keyup" (.send @socket (assoc my-json :value (.-value target)))
        _ nil))
    )
  )

;; socket listeners

(defn socket-open [_e]
  ;;  connect
  (println "connection opened")
  ;; capture events
  (let [doc js/document
        inputs (.querySelectorAll doc "input[type=text]")]
    ;; capture click events anywhere
    (.addEventListener doc "click" action-sender)
    ;; capture keyup events from inputs
    (doseq [x (range (.-length inputs))
            :let [input (aget inputs x)]]
      (.addEventListener input "keyup" action-sender))
    ))

(defn socket-close [e]
  (if (.wasClean e)
    (println "closed cleanly")
    (println "connection died")))

(defn socket-error [e]
  (js/console.error "Error:" (.-message e)))

(defn socket-message [e]
  "When message received."
  (let [data (.-data e)
        {xpath :xpath event :event val :value} (read-string data)
        target-node (-> xpath
                        (js/document.evaluate,,, js/document nil js/XPathResult.FIRST_ORDERED_NODE_TYPE nil)
                        .-singleNodeValue)]
    ;(println "received:" data)
    (case event
            "click" (.click target-node)
            "keyup" (set! target-node.value val)
            _ nil)

    )
  )

;; socket events

(defn init [socket-access-point]
  ;; connect to socket
  (let [new-socket (js/window.WebSocket. socket-access-point)]
    (reset! socket new-socket)
    ;; attach socket events
    (set! new-socket.onopen socket-open)
    (set! new-socket.close socket-close)
    (set! new-socket.onmessage socket-message)
    (set! new-socket.onerror socket-error)
    ))

(set! js/window.browserSyncInit init)

